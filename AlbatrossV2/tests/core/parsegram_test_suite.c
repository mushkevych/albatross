#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"
#include "parsegram_test_suite.h"
#include "../../onboard/protocol/parsegram.h"

#define TRUE 1
#define FALSE 0

/** 
 * test of parsegram creation and removal
 */
void testParsegramInits(void)
{
  parsegram_t parsegram;
  parsegram_init(&parsegram);
  CU_PASS("parsegram initialized");
  
  parsegram_clear(&parsegram);
  CU_PASS("parsegram cleared");

  parsegram_delete(&parsegram);
  CU_PASS("parsegram deleted");
}

/** 
 * test elements adds and removal
 */
void testParsegramAddRemove(void)
{
  uint8_t key = 1;
  parsegram_t parsegram;
  uint8_t *to = "one two three";
  uint8_t *from;
  
  parsegram_init(&parsegram);
  
  parsegram_put(&parsegram, key, to);
  from = (uint8_t *) parsegram_get(&parsegram, key);
  CU_ASSERT_STRING_EQUAL(to, from);
  printf("\n first put OK");

  parsegram_remove(&parsegram, key);
  printf("\n remove OK");
  from = (uint8_t *) parsegram_get(&parsegram, key);
  printf("\n get OK");
  CU_ASSERT_PTR_NULL(from);

  parsegram_clear(&parsegram);
  parsegram_delete(&parsegram);
}

/** 
 * test elements overwrite
 */
void testParsegramOverwrite(void)
{
  parsegram_t parsegram;
  uint8_t key = 1;
  uint8_t *array1 = "one two three";
  uint8_t *array2 = "sun mercury venus";
  uint8_t *from;
  
  parsegram_init(&parsegram);
  
  parsegram_put(&parsegram, key, array1);
  parsegram_put(&parsegram, key, array2);
  
  from = (uint8_t *) parsegram_get(&parsegram, key);
  
  CU_ASSERT_STRING_EQUAL(array2, from);
  
  parsegram_clear(&parsegram);
  parsegram_delete(&parsegram);
}

/**
 * test type addon 
 */
void testParsegramTypes(void)
{
  parsegram_t parsegram;
  uint8_t key1 = 1;
  uint8_t key2 = 2;
  uint8_t key3 = 3;
  uint8_t key4 = 4;
  uint8_t key5 = 5;
  float value1 = 9876.000111222;
  float value1Exp;
  uint32_t value2 = 123456;
  uint32_t value2Exp;
  uint16_t value3 = 512;
  uint16_t value3Exp;
  uint8_t value4 = 255;
  uint8_t value4Exp;
  uint8_t *value5 = "sun mercury venus";
  uint8_t *value5Exp;
  
  parsegram_init(&parsegram);
  
  parsegram_put_float(&parsegram, key1, value1);
  parsegram_put_integer(&parsegram, key2, value2);
  parsegram_put_short(&parsegram, key3, value3);
  parsegram_put_byte(&parsegram, key4, value4);
  parsegram_put(&parsegram, key5, value5);
  
  value1Exp = parsegram_get_as_float(&parsegram, key1);
  value2Exp = parsegram_get_as_integer(&parsegram, key2);
  value3Exp = parsegram_get_as_short(&parsegram, key3);
  value4Exp = parsegram_get_as_byte(&parsegram, key4);
  value5Exp = parsegram_get(&parsegram, key5);
  
  CU_ASSERT_DOUBLE_EQUAL(value1, value1Exp, 0.00001);
  printf("\nvalue 1: %e value 1 expected: %e", value1, value1Exp);
  CU_ASSERT_EQUAL(value2, value2Exp);
  printf("\nvalue 2: %i value 2 expected: %i", value2, value2Exp);
  CU_ASSERT_EQUAL(value3, value3Exp);
  printf("\nvalue 3: %hu value 3 expected: %hu", value3, value3Exp);
  CU_ASSERT_EQUAL(value4, value4Exp);
  printf("\nvalue 4: %hd value 4 expected: %hd", value4, value4Exp);
  CU_ASSERT_STRING_EQUAL(value5, value5Exp);
  printf("\nvalue 5: %s value 5 expected: %s \n", value5, value5Exp);
  
  parsegram_clear(&parsegram);
  parsegram_delete(&parsegram);
}

int containKey(uint8_t* keys, int len, uint8_t key)
{
  int contains = FALSE;
  int i; 
  for (i = 0; i < len; i++)
  {
    if (keys[i] == key) 
    {
      contains = TRUE;
      printf("\nkey found! : %hd ", key);
      break;
    } 
  }

  return contains;
}

void testParsegramKeys()
{
  parsegram_t parsegram;
  uint8_t key1 = 1;
  uint8_t key2 = 2;
  uint8_t key3 = 3;
  uint8_t key4 = 4;
  uint8_t key5 = 5;
  uint8_t key6 = 0x5e;
  uint8_t *value = "any";
  
  uint8_t *keys;
  uint8_t size;

  parsegram_init(&parsegram);
  
  parsegram_put(&parsegram, key1, value);
  parsegram_put(&parsegram, key2, value);
  parsegram_put(&parsegram, key3, value);
  parsegram_put(&parsegram, key4, value);
  parsegram_put(&parsegram, key5, value);
  parsegram_put(&parsegram, key6, value);

  keys = parsegram_get_keys(&parsegram);
  size = parsegram_get_size(&parsegram);
  int isOK1 = containKey(keys, size, key1);
  int isOK2 = containKey(keys, 6, key2);
  int isOK3 = containKey(keys, 6, key3);
  int isOK4 = containKey(keys, 6, key4);
  int isOK5 = containKey(keys, 6, key5);
  int isOK6 = containKey(keys, 6, key6);
  
  if (isOK1 == TRUE 
      && isOK2 == TRUE
      && isOK3 == TRUE
      && isOK4 == TRUE
      && isOK5 == TRUE
      && isOK6 == TRUE)
  {
    CU_PASS("all keys are in place");
  } 
  else 
  {
    CU_FAIL("key is missing");
  }
}


/**
 * add_datagram_test_suite - registers test suite for further execution in testing framework
 */
int add_parsegram_test_suite()
{
   CU_pSuite pSuiteParsegram = NULL;

   /* add a suite to the registry */
   pSuiteParsegram = CU_add_suite("Suite_Parsegram", NULL, NULL);
   if (NULL == pSuiteParsegram) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if ( NULL == CU_add_test(pSuiteParsegram, "test of parsegram inits \n", testParsegramInits)
        || NULL == CU_add_test(pSuiteParsegram, "test elements adds and removal \n", testParsegramAddRemove)
        || NULL == CU_add_test(pSuiteParsegram, "test elements overwrite \n", testParsegramOverwrite)
        || NULL == CU_add_test(pSuiteParsegram, "test parsegram types \n", testParsegramTypes)
        || NULL == CU_add_test(pSuiteParsegram, "test parsegram keys \n", testParsegramKeys) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   return CU_get_error();
}


