#ifndef DATAGRAM_TEST_SUITE
#define DATAGRAM_TEST_SUITE

#include "CUnit/Basic.h"

#ifdef __cplusplus
extern "C" {
#endif

/* 
 * test of datagram_print().
 */
void testDatagramPrint(void);

/* 
 * test of datagram_print().
 */
void testPayloadPrint(void);

/* 
 * test of datagram_get_sender() and datagram_set_sender().
 */
void testSenderSetterGetter(void);

/* 
 * test of datagram_get_destination() and datagram_set_destination().
 */
void testDestinationSetterGetter(void);

/* 
 * test of datagram_get_command() and datagram_set_command().
 */
void testCommandSetterGetter(void);

/* 
 * test of datagram_get_chsum() and datagram_set_chsum().
 */
void testChsumSetterGetter(void);

/* 
 * test of datagram_get_hchsum() and datagram_set_hchsum().
 */
void testHchsumSetterGetter(void);

/* 
 * test of datagram_get_payload_size() and datagram_set_payload_size().
 */
void testPayloadSetterGetter(void);

/**
 * add_datagram_test_suite - registers test suite for further execution in testing framework
 */
int add_datagram_test_suite();

#ifdef __cplusplus
}
#endif

#endif //DATAGRAM_TEST_SUITE

