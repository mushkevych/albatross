#ifndef DATAGRAM_MARSHALLING_TEST_SUITE
#define DATAGRAM_MARSHALLING_TEST_SUITE

#include "CUnit/Basic.h"
#include "../../onboard/protocol/datagram.h"

#define ADDRESS_STATE 0
#define SIZE_STATE 1
#define DATA_STATE 2


#ifdef __cplusplus
extern "C" {
#endif

void unmarshall_payload(datagrizzle_t *d);

void testDatagramMarshalling(void);

/**
 * add_marshalling_test_suite - registers test suite for further execution in testing framework
 */
int add_datagram_marshalling_test_suite();

#ifdef __cplusplus
}
#endif

#endif //DATAGRAM_MARSHALLING_TEST_SUITE

