#include <stdio.h>
#include <string.h>
#include "datagram_marshalling_test_suite.h"
#include "../../onboard/protocol/datagram.h"

float datagram_buffer_to_float(uint8_t *f)
{
	uint32_t asFloat;
	float aFloat;
	
	asFloat = 0;	
	asFloat |= (0xFF000000 & (f[0] << 24));
	asFloat |= (0x00FF0000 & (f[1] << 16));
	asFloat |= (0x000FF00 & (f[2] << 8));
	asFloat |= (0x000000FF & f[3]);

	aFloat = *((float *)&asFloat);
	return aFloat;
}

uint8_t datagram_buffer_to_byte(uint8_t *b)
{
	return b[0];
}

uint16_t datagram_buffer_to_short(uint8_t *s)
{
	uint16_t asShort;
	asShort = 0;
	asShort |= (0xFF00 & (s[0] << 8));
	asShort |= (0x00FF & s[1]);
	return asShort;
}

uint32_t datagram_buffer_to_int(uint8_t *i)
{
	uint32_t asInt;
	asInt = 0;	
	asInt |= (0xFF000000 & (i[0] << 24));
	asInt |= (0x00FF0000 & (i[1] << 16));
	asInt |= (0x000FF00 & (i[2] << 8));
	asInt |= (0x000000FF & i[3]);
	return asInt;
}

/* 
 * test of datagram_print().
 */
void testDatagramMarshalling(void)
{
  datagrizzle_t datagram;
  datagrizzle_t resDatagram;
  
  uint16_t fAddress;
  uint16_t iAddress;
  uint16_t bAddress;
  uint16_t shAddress;
  uint16_t sAddress;

  float fValue;
  uint32_t iValue;
  uint8_t bValue;
  uint16_t shValue;
  char *sValue;
  
  fAddress = 9876;
  fValue = 123.456;
  iAddress = 8765;
  iValue = 123456;
  bAddress = 7654;
  bValue = 255;
  shAddress = 6543;
  shValue = 512;
  sAddress = 5432;
  sValue = "string value of the datagram";
  datagram_init(&datagram, DATAGRAM_MAX_PAYLOAD_SIZE);
  datagram_init(&resDatagram, DATAGRAM_MAX_PAYLOAD_SIZE);
  CU_ASSERT_PTR_NOT_EQUAL(&datagram, &resDatagram);

  // preparing datagram for sending
  datagram_add_float(&datagram, fAddress, fValue);
  datagram_add_int(&datagram, iAddress, iValue);
  datagram_add_byte(&datagram, bAddress, bValue);
  datagram_add_short(&datagram, shAddress, shValue);
  datagram_add_string(&datagram, sAddress, sValue);
  CU_PASS("datagram filled in");
  datagram_print(&datagram);
  
  datagram_pack(&datagram);
  CU_PASS("datagram packed");
  
  //instead of sending - merge header and data into one buffer and unmarshall it
  
  // unmarshall the buffer
  unmarshall_payload(&datagram);
  
  datagram_clear(&datagram);
  datagram_clear(&resDatagram);
}

void unmarshall_payload(datagrizzle_t *d)
{
	uint8_t state,tupleSize,i,endTuple,endPayload = 0;
	uint16_t addy,index = 0;
	
	//Lock the mutexs to prevent the receiving thread overwriting
	pthread_mutex_lock(&d->mutexProcessing);
	
  // payload start-end indeces
	i = 0;
	endPayload = datagram_get_payload_size(d);
	
	//Start by looking for an address
	printf("Unpacking Payload");
	state = ADDRESS_STATE;
	while(i < endPayload)
	{
		if(state == ADDRESS_STATE)
		{
			//Address sent [AddyHigh][AddyLo]
			addy = 0;
			addy |= (0xFF00 & (d->payload.data[i] << 8));
			addy |= d->payload.data[i+1];
			printf("\nAddress: %i ", addy);

			state = SIZE_STATE;
			i+=2; //Skip over the 2nd address byte
		}
		else if(state == SIZE_STATE)
		{
			tupleSize = d->payload.data[i];
			printf("Size: %i ", tupleSize);

			i++;
			state = DATA_STATE;
		}
		else if (state == DATA_STATE)
		{
			//This tuples data ends at
			endTuple = i + tupleSize;
			uint8_t variable[tupleSize];
			
			//Copy the data into the local variable buffer
			for(index = 0 ; index < tupleSize; i++, index++)
			{
				variable[index] = d->payload.data[i];
			}
			
			printf("Raw Data: %s ", variable);
			printf("\n data as short: %hu ", datagram_buffer_to_short(variable));
  		printf("\n data as float: %f ", datagram_buffer_to_float(variable));
  		printf("\n data as int: %i ", datagram_buffer_to_int(variable));
  		printf("\n data as byte: %hd ", datagram_buffer_to_byte(variable));  					
  		
			state = ADDRESS_STATE;
		} else {
  		printf("ERROR: Unknown state %i", state);
		}
	}
	//printf("\n");
	
	//Unlock the mutexs to prevent the receiving thread overwriting
	pthread_mutex_unlock(&d->mutexProcessing);
}


/**
 * add_marshalling_test_suite - registers test suite for further execution in testing framework
 */
int add_datagram_marshalling_test_suite()
{
   CU_pSuite pSuiteMarshalling = NULL;

   /* add a suite to the registry */
   pSuiteMarshalling = CU_add_suite("Suite_Marshalling", NULL, NULL);
   if (NULL == pSuiteMarshalling) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if ( NULL == CU_add_test(pSuiteMarshalling, "test of datagram marshalling \n", testDatagramMarshalling) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   return CU_get_error();
}

