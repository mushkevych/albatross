#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"
#include "datagram_test_suite.h"
#include "../../onboard/protocol/datagram.h"

/* 
 * test of datagram_print().
 */
void testDatagramPrint(void)
{
  datagrizzle_t datagram;
  datagram_print(&datagram);
  CU_PASS("datagram printed");
}

/* 
 * test of datagram_print().
 */
void testPayloadPrint(void)
{
  datagrizzle_t datagram;
  payload_print(&datagram.payload);
  CU_PASS("payload printed");
}

/* 
 * test of datagram_get_destination() and datagram_set_destination().
 */
void testDestinationSetterGetter(void)
{
  datagrizzle_t datagram;
  uint16_t dest = 999;
  uint16_t res;
  
  datagram_set_destination(&datagram, dest);
  res = datagram_get_destination(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}

/* 
 * test of datagram_get_sender() and datagram_set_sender().
 */
void testSenderSetterGetter(void)
{
  datagrizzle_t datagram;
  uint16_t dest = 1111;
  uint16_t res;
  
  datagram_set_sender(&datagram, dest);
  res = datagram_get_sender(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}

/* 
 * test of datagram_get_command() and datagram_set_command().
 */
void testCommandSetterGetter(void)
{
  datagrizzle_t datagram;
  uint16_t dest = 1111;
  uint16_t res;
  
  datagram_set_command(&datagram, dest);
  res = datagram_get_command(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}

/* 
 * test of datagram_get_chsum() and datagram_set_chsum().
 */
void testChsumSetterGetter(void)
{ 
  datagrizzle_t datagram;
  uint16_t dest = 1111;
  uint16_t res;
  
  datagram_set_chsum(&datagram, dest);
  res = datagram_get_chsum(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}

/* 
 * test of datagram_get_hchsum() and datagram_set_hchsum().
 */
void testHchsumSetterGetter(void)
{ 
  datagrizzle_t datagram;
  uint8_t dest = 255; //255 is ceil for the uint8_t
  uint8_t res;
  
  datagram_set_hchsum(&datagram, dest);
  res = datagram_get_hchsum(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}

/* 
 * test of datagram_get_payload_size() and datagram_set_payload_size().
 */
void testPayloadSetterGetter(void)
{ 
  datagrizzle_t datagram;
  uint8_t dest = 255; //255 is ceil for the uint8_t
  uint8_t res;
  
  datagram_set_payload_size(&datagram, dest);
  res = datagram_get_payload_size(&datagram);
  
  CU_ASSERT_EQUAL(dest, res);
}


/**
 * add_datagram_test_suite - registers test suite for further execution in testing framework
 */
int add_datagram_test_suite()
{

  CU_pSuite pDatagramSuite = NULL;

  /* add a suite to the registry */
  pDatagramSuite = CU_add_suite("Datagram Suite", NULL, NULL);
  if (NULL == pDatagramSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* add the tests to the suite */
  if ( NULL == CU_add_test(pDatagramSuite, "test of datagram_print", testDatagramPrint) 
      || NULL == CU_add_test(pDatagramSuite, "test of payload_print", testPayloadPrint) 
      || NULL == CU_add_test(pDatagramSuite, "test of destination_set_get", testDestinationSetterGetter)
      || NULL == CU_add_test(pDatagramSuite, "test of sender_set_get", testSenderSetterGetter)
      || NULL == CU_add_test(pDatagramSuite, "test of command_set_get", testCommandSetterGetter)
      || NULL == CU_add_test(pDatagramSuite, "test of chsum_set_get", testChsumSetterGetter)
      || NULL == CU_add_test(pDatagramSuite, "test of hchsum_set_get", testHchsumSetterGetter)
      || NULL == CU_add_test(pDatagramSuite, "test of payload_set_get", testPayloadSetterGetter) )
  {
    CU_cleanup_registry();
    return CU_get_error();
  }

  return CU_get_error();
}


