#ifndef PARSEGRAM_TEST_SUITE
#define PARSEGRAM_TEST_SUITE

#include "CUnit/Basic.h"

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * test of parsegram creation and removal
 */
void testParsegramInits(void);

/** 
 * test elements adds and removal
 */
void testParsegramAddRemove(void);

/** 
 * test elements overwrite
 */
void testParsegramOverwrite(void);

/**
 * test type addon 
 */
void testParsegramTypes(void);

/**
 * add_datagram_test_suite - registers test suite for further execution in testing framework
 */
int add_parsegram_test_suite();

#ifdef __cplusplus
}
#endif

#endif //PARSEGRAM_TEST_SUITE

