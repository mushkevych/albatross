#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "udp_test_suite.h"
#include "../../onboard/protocol/albatross_std.h"
#include "../../onboard/protocol/udp_multicast.h"
#include "../../onboard/protocol/datagram.h"
#include "../../onboard/protocol/parsegram.h"
#include "../../onboard/protocol/protocol.h"

//  for the receive test - START
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
//  for the receive test - END

#define DATAGRAM_PRINT 1
#define BUFFER_PRINT 0

void test_udp_receive()
{
	int cmd;
	char *from;
	//Allocate the required memory
	from = (char *)malloc(100 * sizeof(char)); //From IP String
	datagrizzle_t dgz;
	parsegram_t parsegram;

	//Threading
	pthread_t rcv;
  uint16_t dest;
//	dest = 0xFFFF;
  dest = TEST_DESTINATION;

	printf("Receiving From: 0x%X\n",dest);
	
	//Initiation
	datagram_clear(&dgz);
	datagram_init(&dgz, DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_recieve_frequency(&dgz, 200);
	parsegram_init(&parsegram);
	
	//Now attach the thread and start it
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *)&dgz);
	printf("Reciever Thread Started\n");

	while(TRUE)
	{
		if(datagram_recieved_and_begin_processing(&dgz) == TRUE)
		{
			printf("Received bytes from %s\n",dgz.from);
			cmd = datagram_process(&dgz, dest);
			if(cmd != -1)
			{
				printf("Valid Datagram\n");
				datagram_print(&dgz);

				datagram_process_payload(&parsegram, &dgz, 0, 0xFFFF);
				printf("Sent Byte = 0x%X\n", parsegram_get_as_byte(&parsegram, TEST_BYTE_ADDY));
				printf("Sent Short = 0x%X\n", parsegram_get_as_short(&parsegram, TEST_SHORT_ADDY));
				printf("Sent Int = 0x%X\n", parsegram_get_as_integer(&parsegram, TEST_INT_ADDY));
				printf("Sent Float = %f\n", parsegram_get_as_float(&parsegram, TEST_FLOAT_ADDY));
				printf("Sent String = %s\n",parsegram_get(&parsegram, TEST_STRING_ADDY));
			}
			else
			{
				printf("Invalid Datagram\n");
//				payload_print(&dgz.payload);
			}
		  
		  datagram_end_processing(&dgz);
		}
		datagram_pause_recieve(&dgz);
	}

	printf("--DONE--\n");
}

void test_udp_send()
{
	datagrizzle_t snd;
	uint16_t dest;

	dest = TEST_DESTINATION;
	printf("Destination: 0x%X\n",dest);

	datagram_clear(&snd);
	datagram_init(&snd, DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_sender(&snd, TEST_SENDER);
	datagram_set_destination(&snd, dest);
	datagram_set_command(&snd, TEST_COMMAND);
	printf("Added Destination and Command\n");
	datagram_add_byte(&snd, TEST_BYTE_ADDY,TEST_BYTE);
	printf("Added Byte\n");
	datagram_add_short(&snd, TEST_SHORT_ADDY,TEST_SHORT);
	printf("Added Short\n");
	datagram_add_int(&snd, TEST_INT_ADDY,TEST_INT);
	printf("Added Int\n");
	datagram_add_float(&snd, TEST_FLOAT_ADDY,TEST_FLOAT);
	printf("Added Float\n");
	datagram_add_string(&snd, TEST_STRING_ADDY,TEST_STRING);
	printf("Added String\n");
	datagram_pack(&snd);
	printf("Packed\n");
	datagram_send(&snd);
#if DATAGRAM_PRINT
	datagram_print(&snd);
#endif
#if BUFFER_PRINT
	payload_print(&snd.payload);
#endif
	printf("--DONE--\n");
}


void test_multicast_receive()
{
	int sock, n, on = 1;
  socklen_t fromlen = 1;
	unsigned char ttl = UDP_MULTICAST_TTL;
	char *data = "test data alpha beta gama";
	printf("\ntest data: %s \n", data);
			
	struct sockaddr_in from;
	struct ip_mreq mreq;
	
	// Create the socket.
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) 
	{
		printf("\nERR_SOCK_CREATE\n");
		return;
	}

/*	// Allow the port/address to be reused
	#if defined(SO_REUSEPORT)
		setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(on));
	#elif defined(SO_REUSEADDR)
		setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	#else
		#error This platform does not support SO_REUSEPORT or SO_REUSEADDR.
	#endif
*/
	// Initialize address
	fromlen = sizeof(from);
	memset(&from, '\0', fromlen);
	from.sin_family = AF_INET; //Internet domain sockets
	from.sin_port = htons(UDP_DEFAULT_PORT);
//	from.sin_addr.s_addr = htonl(INADDR_ANY);


	// Multicast address setup
	mreq.imr_multiaddr.s_addr = inet_addr(UDP_DEFAULT_IP);
//  inet_aton(UDP_DEFAULT_IP, &mreq.imr_multiaddr);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY); // INADDR_ANY is don't care - let the kernel decide
	if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
	{
		printf("\nERR_JOIN_MULTICAST\n");
		return;
	}

	// Bind to the address/port
	n = bind(sock, (struct sockaddr *)&from, fromlen);
	if (n < 0) 
	{
		printf("\nERR_BIND_FAILED\n");
		return;
	}
	
	// Set socket options (make multicast, set TTL)
	setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &on, sizeof(on));
	setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));

	// Receive
	n = recvfrom(sock, data, BUF_SIZE, 0, (struct sockaddr *)&from, &fromlen);
	if (n < 0) 
	{
		printf("\nERR_RECVFROM_FAILED\n");
		return;
	}
	
	// Decode from address
	printf("From Address = %s\n", inet_ntoa(from.sin_addr));

	// Drop multicast group membership
	if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
	{
		printf("\nERR_DROP_MULTICAST\n");
		return;
	}

	close(sock);
}

/**
 * add_upd_test_suite - registers test suite for further execution in testing framework
 */
int add_udp_test_suite()
{
   CU_pSuite pSuiteUDP = NULL;

   /* add a suite to the registry */
   pSuiteUDP = CU_add_suite("Suite_UDP", NULL, NULL);
   if (NULL == pSuiteUDP) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
//   if ( NULL == CU_add_test(pSuiteUDP, "test sending II \n", test_multicast_receive) )
   if ( NULL == CU_add_test(pSuiteUDP, "test sending \n", test_udp_send)
        || NULL == CU_add_test(pSuiteUDP, "test receiving \n", test_udp_receive) 
        || NULL == CU_add_test(pSuiteUDP, "test sending II \n", test_udp_send) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   return CU_get_error();

}
