#ifndef UDP_TEST_SUITE
#define UDP_TEST_SUITE

#include "CUnit/Basic.h"

#define TEST_BYTE_ADDY 0x00
#define TEST_SHORT_ADDY 0x10
#define TEST_INT_ADDY 0x20
#define TEST_FLOAT_ADDY 0x30
#define TEST_STRING_ADDY 0x40

#define TEST_BYTE 0x53
#define TEST_SHORT 0xABCD
#define TEST_INT 0x12345678
#define TEST_FLOAT 17.24
#define TEST_STRING "Hello World"

#define TEST_COMMAND 0x41
#define TEST_DESTINATION 0x02
#define TEST_SENDER 0x01

#ifdef __cplusplus
extern "C" {
#endif

void test_udp_send();
void test_udp_receive();
void test_multicast_receive();

/**
 * add_upd_test_suite - registers test suite for further execution in testing framework
 */
int add_udp_test_suite();

#ifdef __cplusplus
}
#endif

#endif //UDP_TEST_SUITE

