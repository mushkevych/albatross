'''
Python module presents simple Autopilot 
Autopilot listens for GPS/IMU data and orders simple rudder/ailerons/elevator turns 

@date: Apr 24, 2010
@author: Bohdan Mushkevych
'''

import datagram
import parsegram
#import threading
import time
import logging
from math import atan 
from math import sqrt 
from math import degrees
from math import sin 
from waypoint import Waypoint
from route import Route
from servo import Servo
from aviation_formulary import calculate_course_and_distance, calculate_next_position

class Autopilot : #(threading.Thread):
	
	# turn angles extremes   
	MAXIMUM_VERTICAL_ANGLE = 10
	MAXIMUM_HORIZONTAL_ANGLE = 10
	MAXIMUM_ROLL_ANGLE = 8
	MINIMUM_VIRAGE_ANGLE = 10 # handle turn within this range with rudder only. 
	STEP_CHANGE_ANGLE = 0.05 # value of increment 
	
	PATROL_ALTITUDE = 600 # default altitude for the patrol mode (meters)
	PATROL_RADIUS = 50    # default radius of the turn around the patrol point
	PATROL_POINT = None   # point of the patrol
	PATROL_POINT_ID = 999 # id of the patroling point
	PATROL_CALCULATED_ID = 888 # id of the next point while patroling 


	def __init__(self):
#		threading.Thread.__init__(self)

		# autopilot controls initialization
		self.thread_is_running = True
		self.is_in_control = False	  # if True - autopilot is flying the UAV. if False - autopilot is off
		self.servo_rudder_angle = 0
		self.servo_elevator_angle = 0
		self.servo_ailerons_angle = 0
		
		# datagram and inner objects initialization
		self.datagram = datagram.Datagram()
		self.datagram.set_start_addr(0) # Make the buffer large enough to hold any possible input
		self.datagram.set_end_addr(0xFFFF)
		self.route = Route()
		self.servo = Servo()
		
		# create logger and set it up
		self.logger = logging.getLogger("autopilot_demon")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)

	def run(self):
		''' method runs in a separate thread and listens for data from communication chanel 
		as soon as data arrives, it is reviewed and in case it is proper to change UAV direction
		a command is send to servo engines to perform course adjustments
		'''
		self.datagram.start_threaded_recv()
		
		while self.thread_is_running:
			if self.datagram.received_and_begin_processing():
				try:
					cmd = self.datagram.process(datagram.AUTOPILOT_D_BITMASK) # Listen for datagrams for Autopilot
					parsegram = self.datagram.getParsegram()   # get parsed data
				except datagram.IpcException:
					#self.logger.info("Invalid datagram received.")
					continue

				if cmd == datagram.LWC_DATA:
					# autopilot steers the UAV only if it has control
					if self.is_in_control:
						self.steer(parsegram)
				elif cmd == datagram.LWC_EXIT_MANUAL_MODE:
					# take over control of the UAV and fly the route
					self.is_in_control = True
					self.reset_the_controls()
				elif cmd == datagram.LWC_ENTER_MANUAL_MODE:
					# switch autopilot off, and pass control to commands from the ground
					self.is_in_control = False
				elif cmd == datagram.LWC_ADD_WAYPOINT:
					waypoint = self.parse_waypoint(parsegram)
					self.route.add(waypoint)
				elif cmd == datagram.LWC_REMOVE_WAYPOINT:
					waypoint = self.parse_waypoint(parsegram)
					self.route.remove(waypoint)
				elif cmd == datagram.LWC_AUTOPILOT_EXIT:
					# shut down the autopilot thread
					self.is_in_control = False
					self.thread_is_running = False
					self.datagram.stop_threaded_recv()
					self.logger.info("***** ***** Autopilot shut down ***** *****")
				elif cmd == datagram.LWC_REQUEST and parsegram.get_byte(datagram.AUTOPILOT_D_REQUEST_ROUTE) == True:
					self.reply_route(self.datagram.get_sender())
				else:
					self.logger.info("Ignoring incoming datagram with command = %d" % cmd)
					
			self.datagram.end_processing()
			time.sleep(0.01) # 100 Hz
	 
	def parse_waypoint(self, parsegram):
		''' method is called when we expect Waypoint in the transmission
		method goes thru the parsegram and forms Waypoint base on arrived data'''
		longitude = parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE)
		latitude = parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE)
		altitude = parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE)
		lwc = parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_LWC)
		_id = parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_ID)
		waypoint = Waypoint(_id=_id, latitude=latitude, longitude=longitude, altitude=altitude, lwc=lwc)
		self.logger.debug("waypoint received. id: %d" % waypoint._id)
		
		return waypoint
		
	def steer(self, parsegram):
		''' Method makes a decision on whether to change the course or not 
		servo module is ordered to update course if needed
		in case UAV reaches the Waypoint, a LWC is fired to onboard computer
		'''
		uav_longitude = parsegram.get_float(datagram.STATE_D_LONGITUDE)
		uav_latitude = parsegram.get_float(datagram.STATE_D_LATITUDE)
		uav_altitude = parsegram.get_float(datagram.STATE_D_ALTITUDE)
		uav_heading = parsegram.get_float(datagram.STATE_D_HEADING)
		uav_airspeed = parsegram.get_float(datagram.STATE_D_SPEED_LOCAL_X)
		uav_pitch = parsegram.get_float(datagram.STATE_D_PITCH_ANGLE)
		uav_roll = parsegram.get_float(datagram.STATE_D_ROLL_ANGLE)
		
		# waypoint must always be a valid spatial point 
		waypoint = self.route.get_active()
		if waypoint is None:
			if self.PATROL_POINT is None: self.PATROL_POINT = Waypoint(self.PATROL_POINT_ID, uav_latitude, uav_longitude, self.PATROL_ALTITUDE, None)
			waypoint = self.next_point_while_patrol()
		else: self.PATROL_POINT = waypoint
		
		course, distance = calculate_course_and_distance(uav_latitude, uav_longitude, waypoint.latitude, waypoint.longitude)

		altitude_delta = uav_altitude - waypoint.altitude   
		angle_horizontal, angle_vertical, distance = self.calculate_vector (360 - course, distance, altitude_delta, uav_heading)

		if distance <= 9.999:
			# distance is less than 10 meters
			if waypoint.lwc != 0:
				# fire LWC command
				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.AUTOPILOT_D_BITMASK)
				d.set_command(waypoint.lwc)
				d.pack_and_send()

			self.route.remove(waypoint)
		else:
			# alter plane's course  
			self.calculate_servo_angles(angle_horizontal, angle_vertical, uav_pitch, uav_roll)
			self.servo.alter_course(self.servo_rudder_angle, self.servo_elevator_angle, self.servo_ailerons_angle)
			director_waypoint_lat, director_waypoint_lon = calculate_next_position(uav_latitude, uav_longitude, course, distance)
			self.announce_intentions(360 - course, angle_vertical, self.servo_ailerons_angle * self.MAXIMUM_ROLL_ANGLE, uav_airspeed, director_waypoint_lat, director_waypoint_lon, waypoint.altitude)

	def calculate_vector (self, course, distance, altitude_delta, uav_heading):
		''' Method calculates vector in which UAV should fly to reach next waypoint
		  @param course [degree] - azimuth between current position and next waypoint
		  @param distance [meter] - distance between current position and next waypoint
		  @param altitude_delta [meter] - difference in altitudes between current position and next waypoint
		  @param uav_heading [degree] - azimuth of current UAV direction 
		  
		  @return:
		  tuple(angle_horizontal, angle_vertical, distance)
		  angle_horizontal [degrees] - degrees to turn the rudder
		  angle_vertical [degrees] - degrees to ascent/descent the elevator
		  distance [meters] - distance between current position and next waypoint in 3D space 
		'''
		tangent = altitude_delta / distance
		angle_vertical = degrees(atan(tangent))
		variant = None
		
		if 0 <= course < 90 and 0 <= uav_heading < 90:
			angle_horizontal = course - uav_heading
			variant = 11
		elif 0 <= course < 90 and 90 <= uav_heading < 180:
			angle_horizontal = course - uav_heading 
			variant = 14
		elif 0 <= course < 90 and 180 <= uav_heading < 270:
			angle_horizontal = course + (360 - uav_heading)
			if angle_horizontal > 180 : angle_horizontal = angle_horizontal - 360  
			variant = 13
		elif 0 <= course < 90 and 270 <= uav_heading <= 360:
			angle_horizontal = course + (360 - uav_heading)
			if angle_horizontal > 180 : angle_horizontal = angle_horizontal - 360 
			variant = 12

		elif 90 <= course < 180 and 0 <= uav_heading < 90:
			angle_horizontal = course - uav_heading
			variant = 41 
		elif 90 <= course < 180 and 90 <= uav_heading < 180:
			angle_horizontal = course - uav_heading 
			variant = 44
		elif 90 <= course < 180 and 180 <= uav_heading < 270:
			angle_horizontal = course - uav_heading 
			variant = 43
		elif 90 <= course < 180 and 270 <= uav_heading <= 360:
			angle_horizontal = course + (360 - uav_heading)
			if angle_horizontal > 180 : angle_horizontal = angle_horizontal - 360
			variant = 42

		elif 180 <= course < 270 and 0 <= uav_heading < 90:
			angle_horizontal = -uav_heading - (360 - course)
			if angle_horizontal < -180 : angle_horizontal = angle_horizontal + 360
			variant = 31 
		elif 180 <= course < 270 and 90 <= uav_heading < 180:
			angle_horizontal = course - uav_heading 
			variant = 34
		elif 180 <= course < 270 and 180 <= uav_heading < 270:
			angle_horizontal = course - uav_heading
			variant = 33
		elif 180 <= course < 270 and 270 <= uav_heading <= 360:
			angle_horizontal = course - uav_heading
			variant = 32

		elif 270 <= course <= 360 and 0 <= uav_heading < 90:
			angle_horizontal = -uav_heading -(360 - course)
			if angle_horizontal < -180 : angle_horizontal = angle_horizontal + 360
			variant = 21 
		elif 270 <= course <= 360 and 90 <= uav_heading < 180:
			angle_horizontal = -uav_heading -(360 - course) 
			if angle_horizontal < -180 : angle_horizontal = angle_horizontal + 360
			variant = 24
		elif 270 <= course <= 360 and 180 <= uav_heading < 270:
			angle_horizontal = course - uav_heading 
			variant = 23
		elif 270 <= course <= 360 and 270 <= uav_heading <= 360:
			angle_horizontal = course - uav_heading 
			variant = 22

		distance = sqrt(pow(distance, 2) + pow(altitude_delta, 2))
		
		print "*Steer* Course: %f \t Heading: %f \t Result: %f \t Variant: %d" % (course, uav_heading, angle_horizontal, variant)
#		print "*Vector* Horizontal: %f \t Vertical: %f \t Distance: %f" % (angle_horizontal, angle_vertical, distance)
		return (angle_horizontal, angle_vertical, distance)
		
	def calculate_servo_angles (self, angle_horizontal, angle_vertical, uav_pitch, uav_roll):
		''' Method verifies if current pitch/roll/yaw of the UAV is not crossing the hardware/safety constraints 
		  @param angle_horizontal [degrees] - angle, by which Autopilot wants to turn the rudder
		  @param angle_vertical [degrees] - angle, by which Autopilot wants to ascent/descent the elevator
		  @param uav_pitch [degrees] - angle of current UAV pitch
		  @param uav_roll [degrees] - angle of current UAV roll 
		  
		  @return:
		  tuple(angle_horizontal, angle_vertical, angle_roll)
		  angle_horizontal [-1:1] - value from -1 to +1 that is safe to turn the rudder
		  angle_vertical [-1:1] - value from -1 to +1 that is safe to ascent/descent the elevator
		  angle_roll [-1:1] - value from -1 to +1 to turn the ailerons
		'''
		
		# *********** ELEVATOR ***********
		self.servo_elevator_angle = self.calculate_elevator_angle(uav_pitch, angle_vertical)
		if self.servo_elevator_angle < -1:
			# there is no sense to decrease bank beyond "-1" as it is maximum value for the servo-engines
			self.servo_elevator_angle = -1
		if self.servo_elevator_angle > +1:
			# there is no sense to increase bank beyond "+1" as it is maximum value for the servo-engines
			self.servo_elevator_angle = +1

		# *********** AILERONS *********** 
		self.servo_ailerons_angle = self.calculate_ailerons_angle(uav_roll, angle_horizontal)
		if self.servo_ailerons_angle < -1:
			# there is no sense to decrease bank beyond "-1" as it is maximum value for the servo-engines
			self.servo_ailerons_angle = -1
		if self.servo_ailerons_angle > +1:
			# there is no sense to increase bank beyond "+1" as it is maximum value for the servo-engines
			self.servo_ailerons_angle = +1
		
		# *********** RUDDER *********** course
		self.servo_rudder_angle = self.calculate_ruder_angle(angle_horizontal)
		if self.servo_rudder_angle < -1:
			# there is no sense to decrease bank beyond "-1" as it is maximum value for the servo-engines
			self.servo_rudder_angle = -1
		if self.servo_rudder_angle > +1:
			# there is no sense to increase bank beyond "+1" as it is maximum value for the servo-engines
			self.servo_rudder_angle = +1
			
		return (self.servo_rudder_angle, self.servo_elevator_angle, self.servo_ailerons_angle)

	def calculate_ruder_angle(self, angle_horizontal):
#		if abs(angle_horizontal) >= Autopilot.MINIMUM_VIRAGE_ANGLE:
#			# angles larger than Autopilot.MINIMUM_VIRAGE_ANGLE should be handled by ailerons
#			return 0
#		
		# update rudder by number of STEP degrees 
		if angle_horizontal >= 0:
			# aeroplane shall turn RIGHT	
			if angle_horizontal > self.servo_rudder_angle * Autopilot.MAXIMUM_HORIZONTAL_ANGLE:
				self.servo_rudder_angle = self.servo_rudder_angle + Autopilot.STEP_CHANGE_ANGLE
			else:
				self.servo_rudder_angle = self.servo_rudder_angle - Autopilot.STEP_CHANGE_ANGLE
		else:
			# aeroplane shall turn LEFT
			if angle_horizontal < self.servo_rudder_angle * Autopilot.MAXIMUM_HORIZONTAL_ANGLE:
				self.servo_rudder_angle = self.servo_rudder_angle - Autopilot.STEP_CHANGE_ANGLE
			else:
				self.servo_rudder_angle = self.servo_rudder_angle + Autopilot.STEP_CHANGE_ANGLE
		return self.servo_rudder_angle

	def calculate_ailerons_angle(self, uav_roll, angle_horizontal):
#		if abs(angle_horizontal) < Autopilot.MINIMUM_VIRAGE_ANGLE:
			# angles larger than Autopilot.MINIMUM_VIRAGE_ANGLE should be handled by ailerons
			self.servo_ailerons_angle = self.maintain_equilibrium(uav_roll)
			return self.servo_ailerons_angle
		
#		# update ailerons by number of STEP degrees 
#		if angle_horizontal >= 0:
#			# aeroplane shall turn RIGHT	
#			if uav_roll < Autopilot.MAXIMUM_ROLL_ANGLE:
#				self.servo_ailerons_angle = self.servo_ailerons_angle + Autopilot.STEP_CHANGE_ANGLE / 2
#			else:
#				self.servo_ailerons_angle = self.servo_ailerons_angle - 2 * Autopilot.STEP_CHANGE_ANGLE
#		else:
#			# aeroplane shall turn LEFT
#			if uav_roll > -Autopilot.MAXIMUM_ROLL_ANGLE:
#				self.servo_ailerons_angle = self.servo_ailerons_angle - Autopilot.STEP_CHANGE_ANGLE / 2
#			else:
#				self.servo_ailerons_angle = self.servo_ailerons_angle + 2 * Autopilot.STEP_CHANGE_ANGLE
#		return self.servo_ailerons_angle
	
	def maintain_equilibrium(self, uav_roll):
		''' keep the plane's wings in horizontally'''
		denominator = max(abs(uav_roll), Autopilot.MAXIMUM_ROLL_ANGLE)
		self.servo_ailerons_angle = (-1 * uav_roll / 2) / denominator;
		return self.servo_ailerons_angle
	
	def calculate_elevator_angle(self, uav_pitch, angle_vertical):
		if angle_vertical < 0:
			# Autopilot wants aeroplane to climb UP
			if uav_pitch <= Autopilot.MAXIMUM_VERTICAL_ANGLE :
				# current pitch is within allowed range
				if angle_vertical <= self.servo_elevator_angle * Autopilot.MAXIMUM_VERTICAL_ANGLE or uav_pitch < -angle_vertical:
					self.servo_elevator_angle = self.servo_elevator_angle - Autopilot.STEP_CHANGE_ANGLE / 2
				else:
					self.servo_elevator_angle = self.servo_elevator_angle + Autopilot.STEP_CHANGE_ANGLE
			else:
				# current pitch is dangerous and needs immediate correction
				self.servo_elevator_angle = self.servo_elevator_angle + Autopilot.STEP_CHANGE_ANGLE
		else:
			# aeroplane is climbing DOWN
			if uav_pitch >= -Autopilot.MAXIMUM_VERTICAL_ANGLE :
				# current pitch is within allowed range
				if angle_vertical >= self.servo_elevator_angle * Autopilot.MAXIMUM_VERTICAL_ANGLE or uav_pitch > -angle_vertical:
					self.servo_elevator_angle = self.servo_elevator_angle + Autopilot.STEP_CHANGE_ANGLE / 2
				else:
					self.servo_elevator_angle = self.servo_elevator_angle - Autopilot.STEP_CHANGE_ANGLE
			else:
				# current pitch is dangerous and needs immediate correction
				self.servo_elevator_angle = self.servo_elevator_angle - Autopilot.STEP_CHANGE_ANGLE
		
		return self.servo_elevator_angle
			
	def reset_the_controls(self):
		''' method is used to reset controls (rudder, elevator, ailerons) to their neutral positions'''
		self.servo_rudder_angle = 0
		self.servo_elevator_angle = 0
		self.servo_ailerons_angle = 0
		self.servo.alter_course(self.servo_rudder_angle, self.servo_elevator_angle, self.servo_ailerons_angle)
		self.announce_intentions(self.servo_rudder_angle, self.servo_elevator_angle, self.servo_ailerons_angle, 0, 0, 0, 0)
		
	def next_point_while_patrol(self, uav_heading):
		''' method is used to calculate next point for the autopilot, while the aeroplane is is patrol mode
		principle of work is following: as we fix the PATROL_POINT, autopilot is cruising circles around it with the radius PATROL_RADIUS
		Since the autopilot has no capability to make other than simple steering, we give him points that are on the circle of patroling and
		which are on the route of the aeroplane 
		such waypoint will have latitude of PATROL_POINT_ALTITUDE and shall be located 10 degrees further than current UAV heading
		
		@return: Waypoint with calculated lat, long, alt'''
		
		uav_heading += 10
		if uav_heading > 360: uav_heading -= 360  
		latitude_2, longitude_2 = calculate_next_position(self.PATROL_POINT.latitude, self.PATROL_POINT.longitude, uav_heading, self.PATROL_RADIUS)
		
		return Waypoint(self.PATROL_CALCULATED_ID, latitude_2, longitude_2, self.PATROL_ALTITUDE, None)
		

	def reply_route(self, requester):
		''' Method is called when Route information was requested. 
		In response, series of datagrams are fired to provide information about the route 
		@param requester: defines who is the requester of the route '''
		
		i = 0 
		for waypoint in self.route:
			i = i + 1
			d = datagram.Datagram()
			d.set_destination(requester) 
			d.set_sender(datagram.AUTOPILOT_D_BITMASK)
			d.set_command(datagram.LWC_RESPONSE)
			d.add_byte(datagram.AUTOPILOT_D_RESPONSE_LINKS, len(self.route))
			d.add_byte(datagram.AUTOPILOT_D_RESPONSE_ELEMENT, i)
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE, waypoint.longitude)
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE, waypoint.latitude)
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE, waypoint.altitude)
			d.add_int(datagram.AUTOPILOT_D_WAYPOINT_LWC, waypoint.lwc)
			d.add_int(datagram.AUTOPILOT_D_WAYPOINT_ID, waypoint._id)
			d.pack_and_send()
			self.logger.debug("waypoint replied: element %d from %d with personal id %d" % (i, len(self.route), waypoint._id))
			time.sleep(0.02) # 50 Hz

	def announce_intentions(self, course, angle_vertical, angle_roll, uav_airspeed, director_waypoint_lat, director_waypoint_lon, director_waypoint_alt):
		''' Method prepares and sends Datagram to update Groundstation (OpenGC in particular) about Autopilot intentions (roll, pitch, vertical speed, etc).
		  @param course [degree] - azimuth between current position and next waypoint
		  @param angle_vertical [degrees] - defines angle of the rudder's turn 
		  @param angle_roll [degrees] - defines angle of aileron's turn 
		  @param uav_airspeed [meters per seconds] - defines current front-tail speed of the uav, as measured in local aircraft coordinates
		  @param director_waypoint_lat[degrees]: latitude of the waypoint, that Autopilot consider to be the next target 
		  @param director_waypoint_lon[degrees]: longitude of the waypoint, that Autopilot consider to be the next target 
		  '''
		waypoint = self.route.get_active()
		vertical_speed = sin (angle_vertical) * uav_airspeed
		
		d = datagram.Datagram()
		d.set_destination(datagram.COMM_D_BITMASK | datagram.GROUNDSTATION_D_BITMASK) 
		d.set_sender(datagram.AUTOPILOT_D_BITMASK)
		d.set_command(datagram.LWC_DATA)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_ROLL_ANGLE, angle_roll)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_PITCH_ANGLE, angle_vertical)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_HEADING_ANGLE, course)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_ALTITUDE, waypoint.altitude)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_SPEED_X, uav_airspeed)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_SPEED_Z, vertical_speed)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_WAYPOINT_LATITUDE, director_waypoint_lat)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_WAYPOINT_LONGITUDE, director_waypoint_lon)
		d.add_float(datagram.AUTOPILOT_D_DIRECTOR_WAYPOINT_ALTITUDE, director_waypoint_alt)
		d.pack_and_send()
		
#-----------------------------------------------------------------------------
# Main Function
if __name__ == '__main__':
	a = Autopilot()
	a.run()

