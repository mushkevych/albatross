'''
this Python module presents aviation formulary, kindly described by Ed Williams at:
http://williams.best.vwh.net/avform.htm

Formulary contains formulas and calculations, providing position, distance and heading 
for aircraft in the air 

@date: Apr 24, 2010
@author: Bohdan Mushkevych
'''

from math import *

# http://www.pcigeomatics.com/cgi-bin/pcihlp/PROJ%7CEARTH+MODELS%7CELLIPSOIDS
# ELLIPSE Descriptor			  Semi-Major Axis (A) (metres)	  Semi-Minor Axis (B) (metres)
# 12	  WGS			   1984  6378137.000000					6356752.314245
# 15	  Krassovsky		1940  6378245.000000					6356863.018800
	
ELLIPSE_A = 6378137.0			   # Semi-major axis of ellipsoid in meters
#ELLIPSE_A = 6366710.0				# Semi-major axis of ellipsoid in meters
ELLIPSE_B = 6356752.314245		  # Semi-minor axis of ellipsoid in meters
ECCENTRICITY = 0.00673949674233346  # 2nd ECCENTRICITY squared  Geocent_ep2 = (Geocent_a2 - Geocent_b2) / Geocent_b2
EPSOLON = 0.0000008				 # a small number ~ machine precision
D_to_R = 0.01745329251994330		# Pi/180 - degrees to radians		 
R_to_D = 57.29577951308232088	   # 180/Pi - radians to degrees 



def calculate_course_and_distance (latitude_1, longitude_1, latitude_2, longitude_2):
	'''Function calculates: 
	a. The distance between two points with coordinates {lat1,lon1} and {lat2,lon2}
	b. initial course, tc1 (at point 1), from point 1 to point 2 
	
	@param latitude_1: latitude of the point 1. In degrees
	@param longitude_1: longitude of the point 1. In degrees
	@param latitude_2: latitude of the point 2. In degrees
	@param longitude_2: longitude of the point 2. In degrees
	
	@return: tuple of (angle_degrees, distance_meters) 
	where distance is returned in meters 
	and course in degrees
	
	~ 10 centimeters precision when:
	~ abs(lon1-lon2) <= 0.0000014
	~ abs(lat1-lat2) <= 0.0000008 '''

	if abs(latitude_1 - latitude_2) <= 0.0000014 and abs(longitude_1 - longitude_2) <= 0.0000008: 
		return (0, 0)
	
	# We obtain the initial course, tc1, (at point 1) from point 1 to point 2 by the following. 
	if cos(latitude_1) < EPSOLON:   
		# The formula fails if the initial point is a pole. We can special case this with:
		if latitude_1 > 0:
			#  starting from N pole
			tc1 = pi		
		else:
			#  starting from S pole
			tc1 = 2*pi	  
	else:
		# transform measurements system: from SI to radians
		latitude_1 *= D_to_R
		longitude_1 *= D_to_R
		latitude_2 *= D_to_R
		longitude_2 *= D_to_R
		
		sin_lat_1 = sin(latitude_1)
		sin_lat_2 = sin(latitude_2)
		cos_lat_1 = cos(latitude_1)
		cos_lat_2 = cos(latitude_2)
	
		distance_radians = acos(sin_lat_1*sin_lat_2 + cos_lat_1*cos_lat_2*cos(longitude_1-longitude_2))
		distance_meters = ELLIPSE_A * distance_radians

		# For starting points other than the poles:
		calculations = (sin_lat_2 - sin_lat_1*cos(distance_radians)) / (sin(distance_radians)*cos_lat_1)
		# Because of several sin/cos computation, the fluff may exceed acceptable levels
		calculations = max(-1, min(calculations, 1)) 
		tc1 = acos(calculations)
		if sin(longitude_2 - longitude_1) >= 0:
			tc1 = 2*pi - tc1	

	tc1 *= R_to_D
	return (tc1, distance_meters)


def calculate_next_position(latitude_1, longitude_1, angle_degrees, distance_meters):
	''' Function takes latitude_1 and longitude_1 as current position, and calculates new latitude_2, longitude_2 
	with assumption that aircraft will follow course tc over distance
	
	@param angle_degrees is specified in degrees
	@param distance_meters is specified in meters
	
	@return: tuple of calculated (latitude_2, longitude_2). In degrees
	'''  

	# transform measurements system: from SI to radians
	latitude_1 *= D_to_R
	longitude_1 *= D_to_R
	angle_radians = D_to_R * angle_degrees 
	distance_radians = distance_meters / ELLIPSE_A

	sin_lat_1 = sin(latitude_1)
	cos_lat_1 = cos(latitude_1)
	sin_distance_rad = sin(distance_radians)
	cos_distance_rad = cos(distance_radians)

	latitude_2 = asin(sin_lat_1*cos_distance_rad + cos_lat_1*sin_distance_rad*cos(angle_radians))
	dlon = atan2(sin(angle_radians)*sin_distance_rad*cos_lat_1, cos_distance_rad - sin_lat_1*sin(latitude_2))
	longitude_2 = (longitude_1 - dlon + pi) % (2 * pi) - pi
	
	latitude_2 *= R_to_D
	longitude_2 *= R_to_D

	return (latitude_2, longitude_2)
