'''
Unit test to verify Route transmission to and back from "autopilot"

Created on May 3, 2010
@author: Bohdan Mushkevych
'''
import unittest
import threading
import datagram, parsegram
import time
import logging
from waypoint import Waypoint
from route import Route
from autopilot import Autopilot

class TestRouteTransmission(unittest.TestCase):
		'''
		Unit tests covers full working cycle of autopilot:
		from setting the course to tracking the decisions of the autopilot in visualizer
				
		route is set between JFK and LAX (position in degrees): 
		point 1 (LAX): lat1 = 33 + 57/60 = 33.95;     lon1 = 118 + 24/60 = 118.4
		point 2 (JFK): lat2 = 40 + 38/60 = 40.633333; lon2 = 73 + 47/60 = 73.783333
		'''
		
		def setUp(self):
				# create loger and set it up
				self.logger = logging.getLogger("TestRouteTransmission")
				self.logger.setLevel(logging.DEBUG)
				ch = logging.StreamHandler()
				ch.setLevel(logging.DEBUG)
				formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
				ch.setFormatter(formatter)
				self.logger.addHandler(ch)
				self.logger.info("***** ***** SET UP ***** *****")
				
				# all these threadings...
				self.pilot_thread = Autopilot()
				self.pilot_thread.start()

				# start own response listener
				self.datagram = datagram.Datagram()
				self.parsegram = parsegram.Parsegram()
				self.datagram.set_start_addr(0) # Make the buffer large enough to hold any possible input
				self.datagram.set_end_addr(0xFFFF)
				self.datagram.start_threaded_recv()
				
				self.rx_thread = threading.Thread(target=self._receiverThreadRun)
				self.rx_thread.setDaemon(False)
				self.thread_is_running = True
				self.rx_thread.start()
				
				time.sleep(2) # allow all threads to start up

		
		def tearDown(self):
				self.logger.info("***** ***** TEAR DOWN ***** *****")
				# shut down own response listeners
				self.thread_is_running = False
				self.datagram.stop_threaded_recv()

				# shut down the autopilot
				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.TEST_D_BITMASK)
				d.set_command(datagram.LWC_AUTOPILOT_EXIT)  # shuts down the autopilot
				d.pack_and_send()
				
				self.pilot_thread.join(5) # wait for 5 sec for autopilot thread to shut down


		@staticmethod 
		def set_route():
				''' method is used to set route	'''
				wp_1 = Waypoint (_id=1, latitude=33.95, longitude=118.4, altitude=1000, lwc=0)
				wp_2 = Waypoint (_id=2, latitude=35.20, longitude=110.0, altitude=1100, lwc=0)
				wp_3 = Waypoint (_id=3, latitude=37.30, longitude=105.5, altitude=1200, lwc=0)
				wp_4 = Waypoint (_id=4, latitude=39.40, longitude=100.0, altitude=1300, lwc=0)
				wp_5 = Waypoint (_id=5, latitude=41.50, longitude=95.0, altitude=800, lwc=128)
				wp_6 = Waypoint (_id=6, latitude=43.60, longitude=90.5, altitude=700, lwc=255)
				wp_7 = Waypoint (_id=7, latitude=42.50, longitude=85.0, altitude=600, lwc=0)
				wp_8 = Waypoint (_id=8, latitude=41.40, longitude=80.5, altitude=500, lwc=0)
				wp_9 = Waypoint (_id=9, latitude=40.633333, longitude=73.783333, altitude=800, lwc=0)

				route = Route()
				route.add(wp_1)
				route.add(wp_2)
				route.add(wp_3)
				route.add(wp_4)
				route.add(wp_5)
				route.add(wp_6)
				route.add(wp_7)
				route.add(wp_8)
				route.add(wp_9)

				return route
				
		@staticmethod 
		def transmit_route(route):
				''' method transmits route to onboard autopilot '''

				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.TEST_D_BITMASK)
				d.set_command(datagram.LWC_ADD_WAYPOINT)  # adds a waypoint to the route

				for wp in route:
						d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE, wp.longitude)
						d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE, wp.latitude)
						d.add_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE, wp.altitude)
						d.add_int(datagram.AUTOPILOT_D_WAYPOINT_ID, wp._id)
						d.add_int(datagram.AUTOPILOT_D_WAYPOINT_LWC, wp.lwc)
						d.pack_and_send()
						time.sleep(0.02) # 50 Hz


		def test_route(self):
				''' transmits route to the onboard autopilot, and afterwards - requests Autopilot to reply received Route '''

				route = TestRouteTransmission.set_route()
				TestRouteTransmission.transmit_route(route)
			
				# request autopilot to provide its route
				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.TEST_D_BITMASK)
				d.set_command(datagram.LWC_REQUEST)         # requests a Route
				d.add_byte(datagram.AUTOPILOT_D_REQUEST_ROUTE, True)
				d.pack_and_send()
				time.sleep(0.02) # 50 Hz

				self.rx_thread.join(60) 

		def _receiverThreadRun(self):
				''' Method listens for Route to be transfered from Autopilot and checks if it is the same one as was send to Autopilot'''
				received_route = Route()
				start_time = time.time()
						
				while self.thread_is_running:
						if self.datagram.received_and_begin_processing():
								try:
										cmd = self.datagram.process(0xFFFF)             # Listen for everything
										self.parsegram = self.datagram.getParsegram()   # get parsed data
								except datagram.IpcException:
										self.logger.info("Invalid datagram received.")
		
								if cmd == datagram.LWC_RESPONSE:
										# autopilot sends response
										length = self.parsegram.get_byte(datagram.AUTOPILOT_D_RESPONSE_LINKS)
										element = self.parsegram.get_byte(datagram.AUTOPILOT_D_RESPONSE_ELEMENT)
										received_route.add(self.parse_waypoint())
										self.logger.info("Response has arrived. Element %d of %d; current Route length %d" % (element, length, len(received_route)))
							
										if len(received_route) == length:
												self.assertEqual(received_route, TestRouteTransmission.set_route(), 'Route transmitted from Autopilot is different')
		
								else:
										self.logger.info("Ignoring incoming datagram with command = %d" % cmd)
						self.datagram.end_processing()
						time.sleep(0.01) # 100 Hz
						
						if time.time() - start_time > 60:
								self.fail('waiting period of 1 min has expired: Test Failed')


		def parse_waypoint(self):
				''' method is called when we expect Waypoint in the transmission
				method goes thru the parsegram and forms Waypoint base on arrived data'''
				longitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE)
				latitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE)
				altitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE)
				lwc = self.parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_LWC)
				_id = self.parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_ID)
				waypoint = Waypoint(_id=_id, latitude=latitude, longitude=longitude, altitude=altitude, lwc=lwc)
				
				print waypoint
				return waypoint

    
if __name__ == "__main__":
		#import sys;sys.argv = ['', 'Test.testName']
		
		unittest.main()

