'''
Unit test to verify "autopilot" and its "vizualizer"

Created on May 3, 2010
@author: Bohdan Mushkevych
'''
import random
import unittest
from autopilot import Autopilot

class TestAutopilotCourseHeading(unittest.TestCase):
	'''
	Unit tests covers testing of vertical angle only (heading+course):
	'''
	CONSTANT_DOES_NOT_MATTER = 100
	
	def setUp(self):
		self.autopilot = Autopilot()

	def test_heading(self):
#		for course in range(88, 92):
#			for heading in range (88, 93):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#			
#		for course in range(88, 92):
#			for heading in range (178, 183):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#				
#		for course in range(88, 92):
#			for heading in range (268, 273):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#
#		for course in range(88, 92):
#			for heading in range (358, 361):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)

#		for course in range(178, 182):
#			for heading in range (88, 93):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#			
#		for course in range(178, 182):
#			for heading in range (178, 183):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#				
#		for course in range(178, 182):
#			for heading in range (268, 273):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#
#		for course in range(178, 182):
#			for heading in range (358, 361):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)

		for course in range(268, 272):
			for heading in range (88, 93):
				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
			
		for course in range(268, 272):
			for heading in range (178, 183):
				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
				
		for course in range(268, 272):
			for heading in range (268, 273):
				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)

		for course in range(268, 272):
			for heading in range (358, 361):
				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)

#		for course in range(358, 361):
#			for heading in range (88, 93):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#			
#		for course in range(358, 361):
#			for heading in range (178, 183):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#				
#		for course in range(358, 361):
#			for heading in range (268, 273):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)
#
#		for course in range(358, 361):
#			for heading in range (358, 361):
#				self.autopilot.calculate_vector(course, self.CONSTANT_DOES_NOT_MATTER, self.CONSTANT_DOES_NOT_MATTER, heading)



if __name__ == "__main__":
		#import sys;sys.argv = ['', 'Test.testName']
		
		unittest.main()
