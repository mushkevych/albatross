/*
	Daemon to listen to all communication of UDP port and write it to log files

	date: September, 2010
	author: Bohdan Mushkevych
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "logd.h"
#include "../protocol/albatross_std.h"
#include "../protocol/udp_multicast.h"
#include "../protocol/datagram.h"
#include "../protocol/protocol.h"

//Datagram for recieving
datagrizzle_t DgRecv;
//Parsegram
parsegram_t parsegram;

uint8_t running;
FILE *messages, *packets;

//Set up the default packet log frequencies
//They define number of packets to receive from the daemons before saving to packet.log
uint8_t logging_rate = DEFAULT_LOGGING_RATE;

int main(int argc, char *argv[])
{
	pthread_t rcv;
	//Main loop sleep timer
	struct timespec timer;

	if (argc != 4)
	{
		display_usage();
		exit(1);
	}

	//Open The Files
	packets = gfopen(argv[1], argv[3]);
	messages = gfopen(argv[2], argv[3]);

	//Initiation
	datagram_clear(&DgRecv);
	datagram_init(&DgRecv, DATAGRAM_MAX_PAYLOAD_SIZE);
	parsegram_init(&parsegram);

	//Initialise the datagram callbacks (logd recieves from all)
	datagram_set_receive_bitmask(&DgRecv, 0xFFFF);
	datagram_set_callback(&DgRecv, datagram_callback, NULL);

	//Now attach the thread and start it
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *) &DgRecv);
	datagram_send_log_message("LogD Started", LOGGING_D_BITMASK, 0);

	//Initialise the main loop
	timer.tv_sec = 0;
	timer.tv_nsec = 10000000;
	running = TRUE;
	while (running)
	{
		nanosleep(&timer, NULL);
	}

	//Close the open log files
	fflush(messages);
	fclose(messages);
	fflush(packets);
	fclose(packets);

	return 0;
}

void display_usage(void)
{
	fprintf(stdout,
			"Usage: ./logd /path/to/packet.log /path/to/message.log readWriteMode\n"
			"Example: ./logd ../logs/packet.log ../logs/message.log w+\n");
}

FILE *gfopen(char *filename, char *mode)
{
	FILE *fp;
	if ((fp = fopen(filename, mode)) == NULL)
	{
		fprintf(stderr, "LogD: Cannot open %s - Exiting\n", filename);
		exit(1);
	}
	return fp;
}

/** Returns milliseconds since the epoch (1 Jan 1970).
 * Remember: under GCC, long long is always 64-bits
 */
long long time_milliseconds()
{
	static struct timeval tv;
	static struct timezone tz; // not used but need to provide it anyway

	if (gettimeofday(&tv, &tz) != 0)
	{
		// Misleading - it is actually seconds since the Epoch
		printf("Warning - gettimeofday failed.");
	}

	return (tv.tv_usec / 1000) + ((long long) tv.tv_sec * 1000L);
}

void datagram_callback(int cmd, void *user_data)
{
	int16_t header_len;
	int16_t payload_len;
	static uint8_t counter = 0;
	static long long nowTime;

	if (cmd != -1)
	{

		//printf("Message from daemon #%d to daemon #%d\n",datagram_get_sender(&DgRecv),datagram_get_destination(&DgRecv));
		//First check its not a kill command
		if (cmd == LWC_LOGD_EXIT)
		{
			//Prints in the format time,from,message
			fprintf(messages, "%ld,%X,%s\n", time(NULL), LOGGING_D_BITMASK, "LogD Stopped");
			fprintf(stdout, "Message (%d): %s\n", LOGGING_D_BITMASK, "LogD Stopped");
			fflush(stdout);
			fflush(messages);

			running = FALSE;
		}
		//If Its addressed to the logging daemon its a message and not for the packet.log
		else if (cmd == LWC_LOGMSG)
		{
			datagram_process_payload(&parsegram, &DgRecv, LOGGING_D_ADDR_START, LOGGING_D_ADDR_END);

			//Prints in the format time,from,message
			fprintf(messages, "%ld,%X,%s\n", time(NULL), datagram_get_sender(&DgRecv), parsegram_get(&parsegram, LOGGING_D_MESSAGE));
			fprintf(stdout, "Message (%d): %s\n", datagram_get_sender(&DgRecv), parsegram_get(&parsegram, LOGGING_D_MESSAGE));
		}
		//changing the logging rate
		else if ((datagram_get_destination(&DgRecv) & LOGGING_D_BITMASK) && (cmd == LWC_CHANGE_LOGGING_RATE))
		{
				datagram_process_payload(&parsegram, &DgRecv, LOGGING_D_ADDR_START, LOGGING_D_ADDR_END);
				if (parsegram_get(&parsegram, LOGGING_D_LOGGING_RATE) != NULL)
				{
					logging_rate = parsegram_get_as_byte(&parsegram, LOGGING_D_LOGGING_RATE);
				}

				//Prints in the format time,from,message
				fprintf(messages, "%ld,%X,%s%d\n", time(NULL), LOGGING_D_BITMASK, "Logging Rate Changed to: ", logging_rate);
				fprintf(stdout, "Message (%d): %s%d\n", LOGGING_D_BITMASK, "Logging Rate Changed to: ", logging_rate);
		}
		//If its a DATA packet
		else if (cmd == LWC_DATA)
		{
			counter++;
			if (counter == logging_rate)
			{
				fflush(stdout);
				fflush(messages);
				fflush(packets);
				counter = 0;
			}

			//Format: time, size_of_header, header, size of data, data
			header_len = DATAGRAM_HEADER_LEN * sizeof(uint8_t);
			payload_len = datagram_get_payload_size(&DgRecv);

			nowTime = time_milliseconds();
			fwrite(&nowTime, sizeof(nowTime), 1, packets);
			fwrite(&header_len, sizeof(header_len), 1, packets);
			fwrite(DgRecv.payload.header, header_len, 1, packets);
			fwrite(&payload_len, sizeof(payload_len), 1, packets);
			fwrite(DgRecv.payload.data, payload_len, 1, packets);
		}
	}
}
