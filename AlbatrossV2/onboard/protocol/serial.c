/* serial.c */
#include <stdio.h>
#include <termios.h>
#include <strings.h>
// Returns 1 for sucess, 0 for failure.
int serial_configure (FILE *serial, speed_t baud_rate, char hwFlowControl)
{
	struct termios serial_attr;

	if (tcgetattr (fileno(serial), &serial_attr) == -1)
	{
		perror ("serial_configure: Cannot get serial port attributes");
		return 0;
	}

	// Set input and output baud rates. 
	cfsetispeed (&serial_attr, baud_rate);
	cfsetospeed (&serial_attr, baud_rate);

	//Stupid termios raw data mode
	cfmakeraw(&serial_attr);

	//Configure Hardware Flow Control
	if(hwFlowControl)
	{
		//Enable HW (RTS/CTS) Flow control
		serial_attr.c_cflag |= CRTSCTS;
		//Disable SW (XON/XOFF) Flow control
    	serial_attr.c_iflag &= ~(IXON | IXOFF | IXANY);	
	}

	// Allow characters to be read individually.
	serial_attr.c_cc[VMIN] = 1;

	if (tcsetattr (fileno (serial), TCSANOW, &serial_attr) == -1)
	{
		perror ("serial_configure: Cannot set serial port attributes");
		return 0;
	}    
	return 1;
}
