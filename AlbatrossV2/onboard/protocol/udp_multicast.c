/*-------Interprocess Communications: UDP Multicasting-----------------------*
 *  Last Modified: 17 May 2005.
 *  
 *  File: udp_multicast.c
 *  Authors: Hugo Vincent.
 *  Description: Provides send and receive funtions for communication by UDP
 *  			 multicast datagrams. 
 *	References:
 *		http://www.opensource.apple.com/darwinsource/10.3/mDNSResponder-58/mDNSPosix/mDNSPosix.c
 *		http://www.python.org/moin/UdpCommunication
 *		http://sourceforge.net/projects/pyzeroconf
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include "udp_multicast.h"

int multicast_send(char *addr, int port, char *data, int len)
{
	int sock, length, n;
	unsigned char ttl = UDP_MULTICAST_TTL;
	struct sockaddr_in server;
	struct hostent *hp;
	
	// Create the socket.
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) // Socket not created right
		return ERR_SOCK_CREATE;

	// Address Lookup
	hp = gethostbyname(addr);
	if (hp == 0)  // Unknown host
		return ERR_UNKNOWN_HOST;
	
	// Initialize address
	server.sin_family = AF_INET;
	bcopy((char *)hp->h_addr,
			(char *)&server.sin_addr,
			hp->h_length);
	server.sin_port = htons(port);
	length = sizeof(server);

	// Set socket options (make multicast, set TTL)
	setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));

	// Transmit
	n = sendto(sock, data, len, 0, (struct sockaddr *)&server, length);	
	close(sock);
	// Check if it was sent
	if (n < 0) // Sendto failed
		return ERR_SENDTO_FAILED; 
	
	return n;
}

int multicast_recv(char *addr, int port, char *data, char *fromaddr)
{
	int sock, n, on = 1;
  socklen_t fromlen = 1;
	unsigned char ttl = UDP_MULTICAST_TTL;
	struct sockaddr_in from;
	struct ip_mreq mreq;
	
	// Create the socket.
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) 
		return ERR_SOCK_CREATE;

	// Allow the port/address to be reused
	#if defined(SO_REUSEPORT)
		setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(on));
	#elif defined(SO_REUSEADDR)
		setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	#else
		#error This platform does not support SO_REUSEPORT or SO_REUSEADDR.
	#endif
	
	// Initialize address
	fromlen = sizeof(from);
	bzero(&from, fromlen);
	from.sin_family = AF_INET;
	from.sin_addr.s_addr = htonl(INADDR_ANY);
	from.sin_port = htons(port);
	
	// Multicast address setup
	mreq.imr_multiaddr.s_addr = inet_addr(addr);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY); // INADDR_ANY is don't care - let the kernel decide
	if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
		return ERR_JOIN_MULTICAST; 

	// Bind to the address/port
	n = bind(sock, (struct sockaddr *)&from, fromlen);
	if (n < 0) 
		return ERR_BIND_FAILED;
	
	// Set socket options (make multicast, set TTL)
	setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &on, sizeof(on));
	setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));

	// Receive
	n = recvfrom(sock, data, BUF_SIZE, 0, (struct sockaddr *)&from, &fromlen);
	if (n < 0) 
		return ERR_RECVFROM_FAILED;
	
	// Decode from address
	fromaddr = inet_ntoa(from.sin_addr);

	// Drop multicast group membership
	if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
		return ERR_DROP_MULTICAST;

	close(sock);
	return n;
}



