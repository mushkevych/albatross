/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef PieDial_H
#define PieDial_H

#include "MarkedDial.h"
#include "CircleEvaluator.h"

namespace OpenGC
{

class PieDial : public MarkedDial
{
	public:
		PieDial();
		PieDial(XMLNode gaugeNode);

		virtual ~PieDial() {};

		/** Overloaded render function */
		void Render();

		/** Set the gauge label */
		void SetLabel(string label) {m_Label = label; }

		/** Set the thresholds which if the value exceeds, the color will change */
		void SetColourRanges(double minYellow, double minRed) {m_MinYellow = minYellow; m_MinRed = minRed; }

	protected:
		/** Used from Render() to draw the dials arc.
		 * Separate so it can be overloaded to draw the arc differently, 
		 * e.g. for the Tachometer. Drawing is done in a coordinate system
		 * with the origin at the center of the dial. */
		virtual void RenderArc(CircleEvaluator *circ);

		/** Used from Render() to draw the dials ticks.
		 * See RenderArc() for details. */
		virtual void RenderTicks(CircleEvaluator *circ);

		string m_Label;
		double m_MinYellow, m_MinRed;

		// Drawing options
		static const double R = 16.0;
		static const double minDegrees = 90.0;
		static const double maxDegrees = 300.0;
};

} // end namespace OpenGC

#endif
