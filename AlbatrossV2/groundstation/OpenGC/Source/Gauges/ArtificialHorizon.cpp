/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/


#include <stdio.h>

#include "CircleEvaluator.h"
#include "ArtificialHorizon.h"

namespace OpenGC
{

ArtificialHorizon::ArtificialHorizon() : Gauge()
{
	m_PhysicalSize.x = 94.0;
	m_PhysicalSize.y = 98.0;
}

ArtificialHorizon::ArtificialHorizon(XMLNode gaugeNode) : Gauge(gaugeNode)
{
	m_PhysicalSize.x = 94.0;
	m_PhysicalSize.y = 98.0;
}

void ArtificialHorizon::Render()
{
	// Call base class to setup viewport and projection
	Gauge::Render();

	// For drawing circles
	CircleEvaluator *aCircle = globals->m_CircleEvaluator;

	// First, store the "root" position of the gauge component
	glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glPushMatrix();

	double Roll = globals->m_DataSource->GetAirframe()->GetRoll();
	double Pitch = globals->m_DataSource->GetAirframe()->GetPitch();

	// Move to the center of the window
	glTranslated(47,49,0);
	// Rotate based on the bank
	glRotated(Roll, 0, 0, 1);

	// Translate in the direction of the rotation based
	// on the pitch. On the 777, a pitch of 1 degree = 2 mm
	glTranslated(0, Pitch * -2.0, 0);

	//-------------------Gauge Background------------------
	// It's drawn oversize to allow for pitch and bank

	// The "ground" rectangle
	// Remember, the coordinate system is now centered in the gauge component

	glBegin(GL_POLYGON);

#define GROUND_COLOUR1()	glColor3ub(195,82,0)
#define GROUND_COLOUR2()	glColor3ub(253,88,0)
	GROUND_COLOUR1();
	glVertex2f(-75,-75);

	GROUND_COLOUR2();
	glVertex2f(-75,0);

	GROUND_COLOUR2();
	glVertex2f(75,0);

	GROUND_COLOUR1();
	glVertex2f(75,-75);

	GROUND_COLOUR1();
	glVertex2f(-75,-75);

	glEnd();

	// The "sky" rectangle
	// Remember, the coordinate system is now centered in the gauge component

	glBegin(GL_POLYGON);

#define SKY_COLOUR1()	glColor3ub(68,195,255)
#define SKY_COLOUR2()	glColor3ub(30,71,247)
	SKY_COLOUR1();
	glVertex2f(-75,0);

	SKY_COLOUR2();
	glVertex2f(-75,75);

	SKY_COLOUR2();
	glVertex2f(75,75);

	SKY_COLOUR1();
	glVertex2f(75,0);

	SKY_COLOUR1();
	glVertex2f(-75,0);

	glEnd();

	//------------Draw the pitch markings--------------

	// Draw in white
	glColor3ub(255,255,255);
	// Specify line width
	glLineWidth(1.0);

	static const float vertices2[] = {
		-100,0,   100,0,   // The dividing line between sky and ground
		 -5,5,      5,5,   // +2.5 degrees
		-10,10,    10,10,  // +5.0 degrees
		 -5,15,     5,15,  // +7.5 degrees
		-20,20,    20,20,  // +10.0 degrees
		 -5,25,     5,25,  // +12.5 degrees
		-10,30,    10,30,  // +15.0 degrees
		 -5,35,      5,35, // +17.5 degrees
		-20,40,    20,40,  // +20.0 degrees
		 -5,-5,     5,-5,  // -2.5 degrees
		-10,-10,   10,-10, // -5.0 degrees
		 -5,-15,    5,-15, // -7.5 degrees
		-20,-20,   20,-20, // -10.0 degrees
		 -5,-25,    5,-25, // -12.5 degrees
		-10,-30,   10,-30, // -15.0 degrees
		 -5,-35,    5,-35, // -17.5 degrees
		-20,-40,   20,-40  // -20.0 degrees
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices2);
	glDrawArrays(GL_LINES, 0, 34);

	globals->m_FontManager->SetSize(m_Font,4.0, 4.0);

	globals->m_FontManager->Print(-27.5,18.0,"10",m_Font);
	globals->m_FontManager->Print(21.0,18.0,"10",m_Font);

	globals->m_FontManager->Print(-27.5,-22.0,"10",m_Font);
	globals->m_FontManager->Print(21.0,-22.0,"10",m_Font);

	globals->m_FontManager->Print(-27.5,38.0,"20",m_Font);
	globals->m_FontManager->Print(21.0,38.0,"20",m_Font);

	globals->m_FontManager->Print(-27.5,-42.0,"20",m_Font);
	globals->m_FontManager->Print(21.0,-42.0,"20",m_Font);

#if 0 // FIXME this should work in gradient mode too
	//-----The background behind the bank angle markings-------
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();

	// Draw in the sky color
	glColor3ub(0,153,204);

	aCircle->SetOrigin(47,49);
	aCircle->SetRadius(46);
	aCircle->SetDegreesPerPoint(5);
	aCircle->SetArcStartEnd(300.0,360.0);

	aCircle->ResetVertices();
	aCircle->AddVertex(0,98);
	aCircle->AddVertex(0,72);
	aCircle->Evaluate();
	aCircle->AddVertex(47,98);
	aCircle->Render(GL_TRIANGLE_FAN);

	aCircle->SetArcStartEnd(0.0,60.0);
	aCircle->ResetVertices();
	aCircle->AddVertex(94,98);
	aCircle->AddVertex(47,98);
	aCircle->Evaluate();
	aCircle->AddVertex(94,72);
	aCircle->Render(GL_TRIANGLE_FAN);
#endif

	//----------------The bank angle markings----------------

	// Left side bank markings
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();

	// Draw in white
	glColor3ub(255,255,255);

	// Move to the center of the window
	glTranslated(47,49,0);

	// Draw the center detent
	static const float vertices3[] = {
		0.0f,46.0f, -2.3f,49.0f,
		2.3f,49.0f,	0.0f,46.0f
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices3);
	glDrawArrays(GL_POLYGON, 0, 4);

	glRotated(10.0,0,0,1);
	static const float vertices4[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices4);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(10.0,0,0,1);
	static const float vertices5[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices5);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(10.0,0,0,1);
	static const float vertices6[] = {
		0,46, 	0,53
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices6);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(15.0,0,0,1);
	static const float vertices7[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices7);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(15.0,0,0,1);
	static const float vertices8[] = {
		0,46, 	0,51
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices8);
	glDrawArrays(GL_LINES, 0, 2);

	// Right side bank markings
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();
	// Move to the center of the window
	glTranslated(47,49,0);

	glRotated(-10.0,0,0,1);
	static const float vertices9[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices9);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(-10.0,0,0,1);
	static const float vertices10[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices10);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(-10.0,0,0,1);
	static const float vertices11[] = {
		0,46, 	0,53
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices11);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(-15.0,0,0,1);
	static const float vertices12[] = {
		0,46, 	0,49
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices12);
	glDrawArrays(GL_LINES, 0, 2);

	glRotated(-15.0,0,0,1);
	static const float vertices13[] = {
		0,46, 	0,51
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices13);
	glDrawArrays(GL_LINES, 0, 2);

	//----------------End Draw Bank Markings----------------


	//----------------Bank Indicator----------------
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();
	// Move to the center of the window
	glTranslated(47,49,0);
	// Rotate based on the bank
	glRotated(Roll, 0, 0, 1);

	// Draw in white
	glColor3ub(255,255,255);
	// Specify line width
	glLineWidth(2.0);

	// the bottom rectangle
	static const float vertices14[] = {
		-4.5,39.5, 	4.5,39.5,  4.5,41.5,  -4.5,41.5
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices14);
	glDrawArrays(GL_LINE_LOOP, 0, 4);


	// the top triangle
	static const float vertices15[] = {
		-4.5, 41.5,  0, 46,   4.5, 41.5
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices15);
	glDrawArrays(GL_LINE_STRIP, 0, 3);

	//----------------Flight Director----------------
	if (globals->m_DataSource->GetAirframe()->GetDirector_Active() == 1)
	{
		// Reset the modelview matrix
		glPopMatrix();
		glPushMatrix();

		// Get the data
		double directorPitch = globals->m_DataSource->GetAirframe()->GetDirector_Pitch();
		double directorRoll = globals->m_DataSource->GetAirframe()->GetDirector_Roll();

		// Move to the center of the window, move up/down for pitch, and rotate for roll
		glTranslated(47,49 + directorPitch * 2.0, 0);
		glRotated(directorRoll, 0.0, 0.0, 1.0);

		// Draw the wings symbol
		glColor3ub(255,0,255);
		glLineWidth(3.0);

		static const float vertices16[] = {
			-20.0,0.0,  -8.0,0.0,  -4.0,-4.0,  0.0,0.0,  4.0,-4.0,  8.0,0.0,  20.0,0.0
		};
		glVertexPointer(2, GL_FLOAT, 0, &vertices16);
		glDrawArrays(GL_LINE_STRIP, 0, 7);
	}

	//----------------Attitude Indicator----------------
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();
	// Move to the center of the window
	glTranslated(47,49,0);

	// The center axis indicator
	// Black background
	glColor3ub(0,0,0);
	static const float vertices17[] = {
		1.25,1.25,  1.25,-1.25,  -1.25,-1.25,  -1.25,1.25,  1.25,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices17);
	glDrawArrays(GL_POLYGON, 0, 5);

	// White lines
	glColor3ub(255,255,255);
	glLineWidth(2.0);
	static const float vertices18[] = {
		1.25,1.25,  1.25,-1.25,  -1.25,-1.25,  -1.25,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices18);
	glDrawArrays(GL_LINE_LOOP, 0, 4);

	// The left part
	// Black background
	glColor3ub(0,0,0);
	static const float vertices19[] = {
		-39,1.25,  -19,1.25,  -19,-1.25,  -39,-1.25,  -39,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices19);
	glDrawArrays(GL_POLYGON, 0, 5);

	static const float vertices20[] = {
		-19,1.25,  -19,-5.75,  -22,-5.75,  -22,1.25,  -19,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices20);
	glDrawArrays(GL_POLYGON, 0, 5);

	// White lines
	glColor3ub(255,255,255);
	glLineWidth(2.0);
	static const float vertices21[] = {
		-39,1.25,  -19,1.25,  -19,-5.75,  -22,-5.75,  -22,-1.25,  -39,-1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices21);
	glDrawArrays(GL_LINE_LOOP, 0, 6);

	// The right part
	// Black background
	glColor3ub(0,0,0);
	static const float vertices22[] = {
		39,1.25,  19,1.25,  19,-1.25,  39,-1.25,  39,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices22);
	glDrawArrays(GL_POLYGON, 0, 5);

	static const float vertices23[] = {
		19,1.25,  19,-5.75,  22,-5.75,  22,1.25,  19,1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices23);
	glDrawArrays(GL_POLYGON, 0, 5);


	// White lines
	glColor3ub(255,255,255);
	glLineWidth(2.0);

	static const float vertices24[] = {
		39,1.25,  19,1.25,  19,-5.75,  22,-5.75,  22,-1.25,  39,-1.25
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices24);
	glDrawArrays(GL_LINE_LOOP, 0, 6);

	//--------------End draw attitude indicator------------
	// Reset the modelview matrix
	glPopMatrix();
	glPushMatrix();

	aCircle->SetRadius(3.77);
	//-------------------Rounded corners------------------
	// The corners of the artificial horizon are rounded off by
	// drawing over them in black. The overlays are essentially the
	// remainder of a circle subtracted from a square, and are formed
	// by fanning out triangles from a point just off each corner
	// to an arc descrbing the curved portion of the art. horiz.

	// Note we draw each rounded corner as a line strip too, for antialiasing.

	glColor3ub(0,0,0);
	glLineWidth(1.0);

	// Lower left
	aCircle->SetOrigin(3.77,3.77);
	aCircle->SetArcStartEnd(180,270);
	aCircle->SetDegreesPerPoint(15);
	aCircle->ResetVertices();
	aCircle->AddVertex(-1.0,-1.0);
	aCircle->Evaluate();
	aCircle->Render(GL_TRIANGLE_FAN);
	aCircle->Render(GL_LINE_STRIP);

	// Upper left
	aCircle->SetOrigin(3.77,94.23);
	aCircle->SetArcStartEnd(270,360);
	aCircle->SetDegreesPerPoint(15);
	aCircle->ResetVertices();
	aCircle->AddVertex(-1.0,99.0);
	aCircle->Evaluate();
	aCircle->Render(GL_TRIANGLE_FAN);
	aCircle->Render(GL_LINE_STRIP);

	// Upper right
	aCircle->SetOrigin(90.23,94.23);
	aCircle->SetArcStartEnd(0,90);
	aCircle->SetDegreesPerPoint(15);
	aCircle->ResetVertices();
	aCircle->AddVertex(95.0,99.0);
	aCircle->Evaluate();
	aCircle->Render(GL_TRIANGLE_FAN);
	aCircle->Render(GL_LINE_STRIP);

	//Lower right
	aCircle->SetOrigin(90.23,3.77);
	aCircle->SetArcStartEnd(90,180);
	aCircle->SetDegreesPerPoint(15);
	aCircle->ResetVertices();
	aCircle->AddVertex(95.0,-1);
	aCircle->Evaluate();
	aCircle->Render(GL_TRIANGLE_FAN);
	aCircle->Render(GL_LINE_STRIP);

	// Finally, restore the modelview matrix to what we received
	glPopMatrix();
	glDisableClientState(GL_VERTEX_ARRAY);
}

} // end namespace OpenGC
