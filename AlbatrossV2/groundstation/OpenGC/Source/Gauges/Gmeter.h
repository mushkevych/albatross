/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef Gmeter_H
#define Gmeter_H

#include "MarkedDial.h"

namespace OpenGC
{

class Gmeter : public MarkedDial
{
	public:
		Gmeter();
		Gmeter(XMLNode gaugeNode);

		virtual ~Gmeter() {};

		/** Overloaded render function */
		void Render();

		/** Set the gauge label */
		void SetLabel(string label) {m_Label = label; }

		/** Set the thresholds which if the value exceeds, the color will change */
		void SetColourRanges(double minYellow, double minRed) {m_MinYellow = minYellow; m_MinRed = minRed; }

		/** Mouse event handler for reset button */
		void OnMouseDown(int button, double physicalX, double physicalY);

	protected:
		/** Used from Render() to draw the markers on the gauge display */
		void RenderMarker(double markerValue);

		string m_Label;
		double m_MinYellow, m_MinRed;

		// Drawing options
		static const double R = 18.0;
		static const double startDegree = 270.0;
		static const double rangeDegree = 160.0;

	private:
		double m_maxValue, m_minValue;

};

}

#endif
