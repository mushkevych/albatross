/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef MarkedDial_H
#define MarkedDial_H

#include "Gauge.h"

namespace OpenGC
{

class MarkedDial : public Gauge
{
	public:
		MarkedDial();
		MarkedDial(XMLNode gaugeNode);

		virtual ~MarkedDial() {};

		/** Overloaded render function */
		void Render();

		/** Set a name of data source from airframe data container */
		void SetDataSource(string dataSource) {m_DataSource = dataSource; }

		/** Set the range of values displayed on this gauge */
		void SetMinMax(double min, double max) {m_Min = min; m_Max = max; }

		/** Set the spacing between ticks on this gauge display */
		void SetTickSpacing(double spacing) {m_TickSpacing = spacing; }

		/** Set the tick divisor on this gauge display */
		void SetTickDivisor(double divisor) {m_TickDivisor = divisor; }

	protected:
		string m_DataSource;
		double m_Min, m_Max, m_TickSpacing, m_TickDivisor;
};

}

#endif
