/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/


#include <math.h>

#include "Constants.h"
#include "PieDial.h"

namespace OpenGC
{

PieDial::PieDial() : MarkedDial()
{
	m_Label = "";

	m_PhysicalSize.x = 42.0;
	m_PhysicalSize.y = 79.0;

	m_MinYellow = 0.0;
	m_MinRed = 0.0;
}

PieDial::PieDial(XMLNode gaugeNode) : MarkedDial(gaugeNode)
{
	m_PhysicalSize.x = 42.0;
	m_PhysicalSize.y = 79.0;

	// Set the gauge label
	if (gaugeNode.HasChild("Text")) {
		SetLabel(gaugeNode.GetChild("Text").GetText());
	}
	else
	{
		SetLabel("");
	}

	// Set the yellow and red limits
	if (gaugeNode.HasChild("Limit")) {
		double minYellow = 0.0, minRed = 0.0; // temp variables

		XMLNode::NodeList nodeList = gaugeNode.GetChildList("Limit");
		XMLNode::NodeList::iterator iter;
		for (iter = nodeList.begin(); iter != nodeList.end(); ++iter)
		{
			string color = (*iter).GetProperty("color");

			if (color == "yellow")
			{
				minYellow = (*iter).GetTextAsDouble();
			}
			else if (color == "red")
			{
				minRed = (*iter).GetTextAsDouble();
			}
		}

		SetColourRanges(minYellow, minRed);
	}
	else
	{
		SetColourRanges(0.0, 0.0);
	}
}

void PieDial::Render()
{
	Gauge::Render();

	double value = globals->m_DataSource->GetAirframe()->GetAirframeData_D(m_DataSource);

	// Draw the engine dial labels
	glColor3ub(255, 255, 255);
	globals->m_FontManager->SetSize(m_Font, 4, 4);
	globals->m_FontManager->Print(16, 35, m_Label.c_str(), m_Font);

	if (value < m_Min)
		value = m_Min;
	if (value > m_Max)
		value = m_Max;

	char buf[10];
	//GLUquadric *qobj;

	glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glPushMatrix();

	glTranslatef(20, 20, 0);

	// gauge
	if (value < m_MinYellow)
		glColor3ub(51, 51, 76); // blue-grey
	else if (value < m_MinRed)
		glColor3ub(247, 231, 8); // yellow
	else
		glColor3ub(255, 20, 20); // red

	// Fill of the dial from 0 to the needle
	CircleEvaluator *aCircle = globals->m_CircleEvaluator;
	aCircle->SetDegreesPerPoint(10.0);
	aCircle->SetRadius(R);
	aCircle->SetOrigin(0.0, 0.0);
	aCircle->SetArcStartEnd(minDegrees, value / m_Max * (maxDegrees - minDegrees) + minDegrees);

	aCircle->ResetVertices();
	aCircle->AddVertex(0,0);
	aCircle->Evaluate();
	aCircle->Render(GL_TRIANGLE_FAN);

	// White line that is this needle of the dial
	double degree = minDegrees + ((maxDegrees - minDegrees) * (value / (m_Max-m_Min)));
	double radians = degree * DEG_TO_RAD;
	glColor3ub(255, 255, 255);
	glLineWidth(2.0);
	const float vertices[] = {0, 0, R * sin(radians), R * cos(radians)};
	glVertexPointer(2, GL_FLOAT, 0, &vertices);
	glDrawArrays(GL_LINE_STRIP, 0, 2);

	RenderTicks(aCircle);
	RenderArc(aCircle);
	glTranslatef(-20, -20, 0);

	// white rectangle containing the text
	glColor3ub(255, 255, 255);
	glLineWidth(1.0);
	static const float vertices2[] = {42,20,   20,20,   20,30,   42,30};
	glVertexPointer(2, GL_FLOAT, 0, &vertices2);
	glDrawArrays(GL_LINE_STRIP, 0, 4);

	// text
	globals->m_FontManager->SetSize(m_Font, 5, 5);
	glColor3ub(255, 255, 255);
	sprintf(buf, "%.0f", value);
	globals->m_FontManager->Print( 21.9, 22.7, buf, m_Font);

	glPopMatrix();
	glDisableClientState(GL_VERTEX_ARRAY);
}

void PieDial::RenderTicks(CircleEvaluator *circ)
{
	// yellow stripe
	double percentagey = m_MinYellow / (m_Max - m_Min) ;
	double degreeyellow = minDegrees + ((maxDegrees - minDegrees) * percentagey);
	glColor3ub(247, 231, 8);
	double radians = degreeyellow * DEG_TO_RAD;
	const float vertices[] = {
		R * sin(radians), R * cos(radians),
		(R + 4) * sin(radians), (R + 4) * cos(radians)
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices);
	glDrawArrays(GL_LINE_STRIP, 0, 2);

	// red stripe
	double percentager = m_MinRed / (m_Max - m_Min) ;
	double degreered =  minDegrees + ((maxDegrees - minDegrees) * percentager);
	radians = degreered * DEG_TO_RAD;
	glColor3ub(255, 0, 0);
	const float vertices2[] = {
		R * sin(radians), R * cos(radians),
		(R + 4) * sin(radians), (R + 5) * cos(radians)
	};
	glVertexPointer(2, GL_FLOAT, 0, &vertices2);
	glDrawArrays(GL_LINE_STRIP, 0, 2);
}

void PieDial::RenderArc(CircleEvaluator *circ)
{
	// white partial circle 
	glColor3ub(255, 255, 255);
	circ->SetArcStartEnd(minDegrees, maxDegrees);
	// FIXME enable mitering
	glLineWidth(3.0);
	circ->ResetVertices();
	circ->Evaluate();
	circ->Render(GL_LINE_STRIP);
}

} // end namespace OpenGC
