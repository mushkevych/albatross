/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <math.h>

#include "Gauge.h"
#include "Gmeter.h"
#include "CircleEvaluator.h"
#include "Constants.h"

namespace OpenGC
{

Gmeter::Gmeter() : MarkedDial()
{
	m_Label = "";

	m_PhysicalSize.x = 44.0;
	m_PhysicalSize.y = 50.0;

	m_minValue = 0.0;
	m_maxValue = 0.0;

	m_MinYellow = 0.0;
	m_MinRed = 0.0;

}

Gmeter::Gmeter(XMLNode gaugeNode) : MarkedDial(gaugeNode)
{
	m_minValue = 0.0;
	m_maxValue = 0.0;

	m_PhysicalSize.x = 44.0;
	m_PhysicalSize.y = 50.0;

	// Set the gauge label
	if (gaugeNode.HasChild("Text")) {
		SetLabel(gaugeNode.GetChild("Text").GetText());
	}
	else
	{
		SetLabel("");
	}

	// Set the yellow, red and absolute limits
	if (gaugeNode.HasChild("Limit")) {
		double maxAbsolute = 0.0, minYellow = 0.0, minRed = 0.0; // temp variables

		XMLNode::NodeList nodeList = gaugeNode.GetChildList("Limit");
		XMLNode::NodeList::iterator iter;
		for (iter = nodeList.begin(); iter != nodeList.end(); ++iter)
		{
			string color = (*iter).GetProperty("color");

			if (color == "yellow")
			{
				minYellow = (*iter).GetTextAsDouble();
			}
			else if (color == "red")
			{
				minRed = (*iter).GetTextAsDouble();
			}
			else if (color == "")
			{
				maxAbsolute = (*iter).GetTextAsDouble();
			}
		}

		SetMinMax(-1 * fabs(maxAbsolute), fabs(maxAbsolute));
		SetColourRanges(fabs(minYellow), fabs(minRed));
	}
	else
	{
		SetMinMax(-8.0, 8.0);
		SetColourRanges(2.0, 4.0);
	}
}

void Gmeter::Render()
{
	Gauge::Render();

	char buf[10];
	double value = globals->m_DataSource->GetAirframe()->GetAirframeData_D(m_DataSource);

	if (value > m_Max) {
		value = m_Max;
	} else if (value < m_Min) {
		value = m_Min;
	}

	// draw the gauge label
	glColor3ub(255, 255, 255);
	globals->m_FontManager->SetSize(m_Font, 4, 4);
	globals->m_FontManager->Print(10, 40, m_Label.c_str(), m_Font);

	// draw the "reset" button label
	glColor3ub(255, 0, 0);
	globals->m_FontManager->SetSize(m_Font, 3.0, 3.0);
	globals->m_FontManager->Print(34, 4, "R", m_Font);

	glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glPushMatrix();

	glLineWidth(2.0);
	glEnable(GL_LINE_SMOOTH);

	glTranslatef(18, 18, 0);

	// draw gauge circle outside
	glColor3ub(255, 255, 255);
	CircleEvaluator *aCircle = globals->m_CircleEvaluator;
	aCircle->SetDegreesPerPoint(10.0);
	aCircle->SetRadius(R);
	aCircle->SetOrigin(0.0, 0.0);
	aCircle->SetArcStartEnd(startDegree - rangeDegree, startDegree + rangeDegree);

	aCircle->ResetVertices();
	aCircle->Evaluate();
	aCircle->Render(GL_LINE_STRIP);

	// draw gauge "reset" button
	glColor3ub(255, 255, 255);
	aCircle->SetDegreesPerPoint(10.0);
	aCircle->SetRadius(5);
	aCircle->SetOrigin(16.0, -12.0);
	aCircle->SetArcStartEnd(8, 242);

	aCircle->ResetVertices();
	aCircle->Evaluate();
	aCircle->Render(GL_LINE_STRIP);

	// draw gauge arrow
	double percentage = value / m_Max ;
	double degree = startDegree + ((rangeDegree - 15.0) * percentage);
	double radians = degree * DEG_TO_RAD;

	glColor3ub(255, 255, 255);
	const float vertices[] = {0,0,  R * sin(radians),R * cos(radians)};
	glVertexPointer(2, GL_FLOAT, 0, &vertices);
	glDrawArrays(GL_LINE_STRIP, 0, 2);

	// draw gauge max positive arrow
	if (value > m_maxValue) {
		m_maxValue = value;
	}

	percentage = m_maxValue / m_Max ;
	degree = startDegree + ((rangeDegree - 15.0) * percentage);
	double maxRadians = degree * DEG_TO_RAD;

	// draw gauge min negative arrow
	if (value < m_minValue) {
		m_minValue = value;
	}

	percentage = m_minValue / m_Max ;
	degree = startDegree + ((rangeDegree - 15.0) * percentage);
	double minRadians = degree * DEG_TO_RAD;

	glColor3ub(255, 0, 0);
	const float arrVertices[] = {R * sin(minRadians), R * cos(minRadians),  0, 0,
								 R * sin(maxRadians), R * cos(maxRadians)};
	glVertexPointer(2, GL_FLOAT, 0, &arrVertices);
	glDrawArrays(GL_LINE_STRIP, 0, 3);

	// draw gauge rectangle containing the current value
	glLineWidth(1.0);
	glColor3ub(120, 120, 120);

	const float rectVertices[] = {44.0,-5.0,  13.0, -5.0, 13.0, 5.0, 44.0, 5.0};
	glVertexPointer(2, GL_FLOAT, 0, &rectVertices);
	glDrawArrays(GL_LINE_STRIP, 0, 4);

	// round current value
	value = round(10 * value) / 10;
	if (value == 0.0) value = fabs(value);

	// draw current value in gauge display
	if (fabs(value) < m_MinYellow)
	{
		glColor3ub(255, 255, 255);
	}
	else if (fabs(value) < m_MinRed)
	{
		glColor3ub(247, 231, 8);
	}
	else
	{
		glColor3ub(255, 20, 20);
	}

	sprintf(buf, "%.1f", value);
	globals->m_FontManager->SetSize(m_Font, 4.0, 3.5);
	globals->m_FontManager->Print(15.0, -1.5, buf, m_Font);

	// render gauge markers
	for(double xs = m_Min; xs <= m_Max; xs += m_TickSpacing)
	{
		if (fabs(xs) <= m_MinYellow)
		{
			glColor3ub(255, 255, 255);
		}
		else if (fabs(xs) <= m_MinRed)
		{
			glColor3ub(247, 231, 8);
		}
		else
		{
			glColor3ub(255, 20, 20);
		}

		RenderMarker(xs);
	}

	glPopMatrix();
	glDisableClientState(GL_VERTEX_ARRAY);
}

void Gmeter::OnMouseDown(int button, double physicalX, double physicalY)
{
	// calculate radiuses of "reset" button and gauge panel
	double buttonRadius = sqrt((physicalX - 34) * (physicalX - 34) + (physicalY - 5) * (physicalY - 5));
	double gaugeRadius = sqrt((physicalX - 18) * (physicalX - 18) + (physicalY - 18) * (physicalY - 18));

	if (gaugeRadius > 18.0 && buttonRadius <= 5.0) {
		m_minValue = 0.0;
		m_maxValue = 0.0;
	}
}

void Gmeter::RenderMarker(double markerValue)
{
	char buf[10];

	double percentagev = markerValue / m_Max ;
	double radians =  (startDegree + ((rangeDegree - 15.0) * percentagev)) * DEG_TO_RAD;

	if ((fabs(markerValue) < 1.0) && (fabs(markerValue) > 0.01))
		sprintf(buf, "%0.1f", markerValue);
	else
		sprintf(buf, "%.0f", markerValue / m_TickDivisor);

	if (markerValue >= 0)
		glTranslatef(-1.5, -2.0, 0);
	else
		glTranslatef(-3.2, -1.0, 0);

	globals->m_FontManager->Print((R - 4.5) * sin(radians), (R - 4.5) * cos(radians), buf, m_Font);

	if (markerValue >= 0)
		glTranslatef(1.5, 2.0, 0);
	else
		glTranslatef(3.2, 1.0, 0);

	const float vertices[] = { R * sin(radians), R * cos(radians),
							  (R - 2) * sin(radians), (R - 2) * cos(radians)};
	glVertexPointer(2, GL_FLOAT, 0, &vertices);
	glDrawArrays(GL_LINE_STRIP, 0, 2);
}

} // end namespace OpenGC
