/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <time.h>
#include <sys/time.h>

#include "FPSIndicator.h"

namespace OpenGC
{

FPSIndicator::FPSIndicator() : Gauge()
{
	m_PhysicalSize.x = 25.0;
	m_PhysicalSize.y = 10.0;
}

FPSIndicator::FPSIndicator(XMLNode gaugeNode) : Gauge(gaugeNode)
{
	m_PhysicalSize.x = 25.0;
	m_PhysicalSize.y = 10.0;
}

void FPSIndicator::Render()
{
	// Call base class to setup viewport and projection
	Gauge::Render();

	// Calculate estimated frames per second
	static double rate, last;
	static int count;
	static int updateRate = (int)(1.0 / globals->m_PrefManager->GetPrefD("AppUpdateRate"));

	double now;
	struct timeval tv;
	struct timezone tz;

	if (++count > updateRate)
	{ // recalculate roughly once per second
		gettimeofday(&tv, &tz);
		now = (double)tv.tv_usec/1e6 + (double)tv.tv_sec;
		rate = (double)updateRate / (now - last);
		last = now;
		count = 0;
	}

	// Draw frames per second
	char buffer[10];
	sprintf(buffer, "FPS  %.1f", rate);
	globals->m_FontManager->SetSize(m_Font, 4, 4);
	globals->m_FontManager->Print(0, 0, buffer, m_Font);
}

} // end namespace OpenGC
