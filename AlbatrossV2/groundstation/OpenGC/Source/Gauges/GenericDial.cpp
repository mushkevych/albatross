/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <math.h>

#include "CircleEvaluator.h"
#include "Constants.h"
#include "GenericDial.h"

namespace OpenGC
{

GenericDial::GenericDial() : Gauge()
{
	m_PhysicalSize.x = 30.0;
	m_PhysicalSize.y = 30.0;

	m_Min = 0.0,
	m_Max = 0.0;

	m_TickSpacing = 1.0,
	m_TickDivisor = 1.0;

	m_DataSource = "UNKNOWN SOURCE";
}

GenericDial::GenericDial(XMLNode gaugeNode) : Gauge(gaugeNode)
{
	m_PhysicalSize.x = 30.0;
	m_PhysicalSize.y = 30.0;

	// Set the data source
	if (gaugeNode.HasChild("Data")) {
		SetDataSource(gaugeNode.GetChild("Data").GetText());
	}
	else
	{
		SetDataSource("UNKNOWN SOURCE");
	}

	// Set the min and max
	if (gaugeNode.HasChild("Range")) {
		double min, max; // temp variables

		gaugeNode.GetChild("Range").GetTextAsCoord(min, max);
		SetMinMax(min, max);
	}
	else
	{
		SetMinMax(0.0, 0.0);
	}

	// Set the tick spacing
	if (gaugeNode.HasChild("TickSpacing")) {
		SetTickSpacing(gaugeNode.GetChild("TickSpacing").GetTextAsDouble());
	}
	else
	{
		SetTickSpacing(1.0);
	}

	// Set the tick divisor
	if (gaugeNode.HasChild("TickDivisor")) {
		SetTickDivisor(gaugeNode.GetChild("TickDivisor").GetTextAsDouble());
	}
	else
	{
		SetTickDivisor(1.0);
	}
}

void GenericDial::Render()
{
	Gauge::Render();

	double value = globals->m_DataSource->GetAirframe()->GetAirframeData_D(m_DataSource);

	if (value < m_Min)
		value = m_Min;
	else if (value > m_Max)
		value = m_Max;

	double R = 11.0;
	double minDegrees = 220.0;
	double maxDegrees = 100.0;

	double maxDegreesUse360 = maxDegrees;
	if (maxDegrees < minDegrees)
		maxDegreesUse360 += 360;

	double negativeoffset = 0;
	if (m_Min < 0)
		negativeoffset = m_Min*-1.0;

	double xcircle, ycircle, radians;
	char buf[10];
	//GLUquadric *qobj;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glLineWidth(2.0);
	glEnable(GL_LINE_SMOOTH);

	glTranslatef(18, 18, 0);

	// gauge
	glColor3ub(51, 51, 76);
	CircleEvaluator aCircle;
	aCircle.SetDegreesPerPoint(10.0);
	aCircle.SetRadius(R);
	aCircle.SetOrigin(0.0, 0.0);
	aCircle.SetArcStartEnd(minDegrees, value / m_Max * (maxDegreesUse360 - minDegrees) + minDegrees);

	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0,0);
	aCircle.Evaluate();
	glEnd();

	/*qobj = gluNewQuadric();
	  gluPartialDisk(qobj, 0, R+1, 50, 1, minDegrees, value / m_Max * (maxDegreesUse360-minDegrees));
	  gluDeleteQuadric(qobj);*/

	double percentage = value / (m_Max-m_Min) ;
	double degree = minDegrees + ((maxDegreesUse360 - minDegrees) * percentage);
	glBegin(GL_LINE_STRIP);
	glColor3ub(255, 255, 255);
	glVertex2f(0, 0);
	radians = degree * DEG_TO_RAD;
	xcircle = (R) * sin(radians);
	ycircle = (R) * cos(radians);
	glVertex2f(xcircle, ycircle);
	glEnd();

	//circle outside
	glColor3ub(255, 255, 255);
	aCircle.SetArcStartEnd(minDegrees, maxDegrees);

	// FIXME enable mitering
	glBegin(GL_LINE_STRIP);
	aCircle.Evaluate();
	glEnd();

	/*qobj = gluNewQuadric();
	  gluPartialDisk(qobj, R, R+1, 50, 1, minDegrees, maxDegreesUse360-minDegrees);
	  gluDeleteQuadric(qobj);*/

	// unit markers
	globals->m_FontManager->SetSize(m_Font, 4.0, 3.5);
	double percentagev, degreev;
	for(double xs = m_Min; xs <= m_Max; xs += m_TickSpacing)
	{
		percentagev = (xs+negativeoffset) / (m_Max-m_Min) ;
		degreev =  minDegrees+ ((maxDegreesUse360- minDegrees)*percentagev);
		glBegin(GL_LINE_STRIP);
		glColor3ub(255, 255, 255);
		radians=degreev * DEG_TO_RAD;
		xcircle = (R) * sin(radians);
		ycircle = (R) * cos(radians);
		glVertex2f(xcircle, ycircle);
		xcircle = (R - 2) * sin(radians);
		ycircle = (R - 2) * cos(radians);
		glVertex2f(xcircle, ycircle);
		glEnd();
		xcircle = (R-4.5) * sin(radians);
		ycircle = (R-4.5) * cos(radians);
		if ((fabs(xs) < 1.0) && (fabs(xs) > 0.01))
			sprintf(buf, "%0.1f",xs);
		else
			sprintf(buf, "%.0f",xs / m_TickDivisor);
		glTranslatef(-1.5, -2, 0);
		globals->m_FontManager->Print(xcircle , ycircle, buf, m_Font);			
		glTranslatef(1.5, 2, 0);
	}


	glPopMatrix();

}

} // end namespace OpenGC
