/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <stdio.h>

#include "AirframeDataContainer.h"

namespace OpenGC
{

AirframeDataContainer::AirframeDataContainer()
{
	this->InitializeData();
}

void AirframeDataContainer::InitializeData()
{
		m_Roll = 0;
		m_Pitch = 0;
		m_True_Heading = 0;
		m_Track_Heading = 0;

		m_Latitude = 0;
		m_Longitude = 0;

		m_Accel_Body_Fwd = 0;
		m_Accel_Body_Right = 0;
		m_Accel_Body_Down = 0;

		m_Airspeed_KT = 0;
		m_Ground_Speed_MS = 0;
		m_Vertical_Speed_FPM = 0;

		m_Altitude_AGL_Meters = 0;
		m_Altitude_MSL_Meters = 0;
		m_Barometric_Pressure = 1010.0;

		m_Engine_RPM = 0.0;
		m_Engine_CHT = 0.0;
		m_Engine_EGT = 0.0;
		m_Engine_Mixture = 0.0;
		m_Voltage_Alternator = 0.0;
		m_Voltage_Battery = 0.0;

		m_Director_Active = false;
		m_Director_Pitch = 0.0;
		m_Director_Roll = 0.0;
		m_Director_Heading = 0.0;
		m_Director_Altitude = 0.0;
		m_Director_Airspeed = 0.0;
		m_Director_Vertical_Speed = 0.0;

		m_Got_Data = false;
		m_Status_Active = false;
		m_GPS_Mode = 0;
		m_GPS_Sats = 0;
		m_Internal_Temp = 0.0;
		m_External_Temp = 0.0;

		m_Wind_Speed = 0.0;
		m_Wind_Direction = 0.0;

		m_Status_Text1 = 0; // Strings set to null pointers
		m_Status_Text2 = 0;
		m_Status_Text3 = 0;
		m_Status_Text4 = 0;
		m_Status_Colour1 = 0;
		m_Status_Colour2 = 0;
		m_Status_Colour3 = 0;
		m_Status_Colour4 = 0;
		m_Status_Text_NoData = 0;
}

int AirframeDataContainer::GetAirframeData_I(const string& dataName)
{
	if (dataName == "Roll")
	{
		return (int)m_Roll;
	}
	else if (dataName == "Pitch")
	{
		return (int)m_Pitch;
	}
	else if (dataName == "True_Heading")
	{
		return (int)m_True_Heading;
	}
	else if (dataName == "Track_Heading")
	{
		return (int)m_Track_Heading;
	}
	else if (dataName == "Latitude")
	{
		return (int)m_Latitude;
	}
	else if (dataName == "Longitude")
	{
		return (int)m_Longitude;
	}
	else if (dataName == "Accel_Body_Fwd")
	{
		return (int)m_Accel_Body_Fwd;
	}
	else if (dataName == "Accel_Body_Right")
	{
		return (int)m_Accel_Body_Right;
	}
	else if (dataName == "Accel_Body_Down")
	{
		return (int)m_Accel_Body_Down;
	}
	else if (dataName == "Airspeed_KT")
	{
		return (int)m_Airspeed_KT;
	}
	else if (dataName == "Ground_Speed_MS")
	{
		return (int)m_Ground_Speed_MS;
	}
	else if (dataName == "Vertical_Speed_FPM")
	{
		return (int)m_Vertical_Speed_FPM;
	}
	else if (dataName == "Altitude_AGL_Feet")
	{
		return (int)m_Altitude_AGL_Meters;
	}
	else if (dataName == "Altitude_MSL_Feet")
	{
		return (int)m_Altitude_MSL_Meters;
	}
	else if (dataName == "Barometric_Pressure")
	{
		return (int)m_Barometric_Pressure;
	}
	else if (dataName == "Engine_RPM")
	{
		return (int)m_Engine_RPM;
	}
	else if (dataName == "Engine_CHT")
	{
		return (int)m_Engine_CHT;
	}
	else if (dataName == "Engine_EGT")
	{
		return (int)m_Engine_EGT;
	}
	else if (dataName == "Engine_Mixture")
	{
		return (int)m_Engine_Mixture;
	}
	else if (dataName == "Voltage_Alternator")
	{
		return (int)m_Voltage_Alternator;
	}
	else if (dataName == "Voltage_Battery")
	{
		return (int)m_Voltage_Battery;
	}
	else if (dataName == "Director_Active")
	{
		return (int)m_Director_Active;
	}
	else if (dataName == "Director_Roll")
	{
		return (int)m_Director_Roll;
	}
	else if (dataName == "Director_Pitch")
	{
		return (int)m_Director_Pitch;
	}
	else if (dataName == "Director_Heading")
	{
		return (int)m_Director_Heading;
	}
	else if (dataName == "Director_Altitude")
	{
		return (int)m_Director_Altitude;
	}
	else if (dataName == "Director_Airspeed")
	{
		return (int)m_Director_Airspeed;
	}
	else if (dataName == "Director_Vertical_Speed")
	{
		return (int)m_Director_Vertical_Speed;
	}
	else if (dataName == "Got_Data")
	{
		return (int)m_Got_Data;
	}
	else if (dataName == "Status_Active")
	{
		return (int)m_Status_Active;
	}
	else if (dataName == "GPS_Mode")
	{
		return m_GPS_Mode;
	}
	else if (dataName == "GPS_Sats")
	{
		return m_GPS_Sats;
	}
	else if (dataName == "Internal_Temp")
	{
		return (int)m_Internal_Temp;
	}
	else if (dataName == "External_Temp")
	{
		return (int)m_External_Temp;
	}
	else if (dataName == "Wind_Direction")
	{
		return (int)m_Wind_Direction;
	}
	else if (dataName == "Wind_Speed")
	{
		return (int)m_Wind_Speed;
	}
	else if (dataName == "Status_Colour1")
	{
		return m_Status_Colour1;
	}
	else if (dataName == "Status_Colour2")
	{
		return m_Status_Colour2;
	}
	else if (dataName == "Status_Colour3")
	{
		return m_Status_Colour3;
	}
	else if (dataName == "Status_Colour4")
	{
		return m_Status_Colour4;
	}
	else
	{
		printf("Error: unsupported data name \"%s\"\n", dataName.c_str());
		return 0;
	}
}

double AirframeDataContainer::GetAirframeData_D(const string& dataName)
{
	if (dataName == "Roll")
	{
		return m_Roll;
	}
	else if (dataName == "Pitch")
	{
		return m_Pitch;
	}
	else if (dataName == "True_Heading")
	{
		return m_True_Heading;
	}
	else if (dataName == "Track_Heading")
	{
		return m_Track_Heading;
	}
	else if (dataName == "Latitude")
	{
		return m_Latitude;
	}
	else if (dataName == "Longitude")
	{
		return m_Longitude;
	}
	else if (dataName == "Accel_Body_Fwd")
	{
		return m_Accel_Body_Fwd;
	}
	else if (dataName == "Accel_Body_Right")
	{
		return m_Accel_Body_Right;
	}
	else if (dataName == "Accel_Body_Down")
	{
		return m_Accel_Body_Down;
	}
	else if (dataName == "Airspeed_KT")
	{
		return m_Airspeed_KT;
	}
	else if (dataName == "Ground_Speed_MS")
	{
		return m_Ground_Speed_MS;
	}
	else if (dataName == "Vertical_Speed_FPM")
	{
		return m_Vertical_Speed_FPM;
	}
	else if (dataName == "Altitude_AGL_Feet")
	{
		return m_Altitude_AGL_Meters;
	}
	else if (dataName == "Altitude_MSL_Feet")
	{
		return m_Altitude_MSL_Meters;
	}
	else if (dataName == "Barometric_Pressure")
	{
		return m_Barometric_Pressure;
	}
	else if (dataName == "Engine_RPM")
	{
		return m_Engine_RPM;
	}
	else if (dataName == "Engine_CHT")
	{
		return m_Engine_CHT;
	}
	else if (dataName == "Engine_EGT")
	{
		return m_Engine_EGT;
	}
	else if (dataName == "Engine_Mixture")
	{
		return m_Engine_Mixture;
	}
	else if (dataName == "Voltage_Alternator")
	{
		return m_Voltage_Alternator;
	}
	else if (dataName == "Voltage_Battery")
	{
		return m_Voltage_Battery;
	}
	else if (dataName == "Director_Active")
	{
		return (double)m_Director_Active;
	}
	else if (dataName == "Director_Roll")
	{
		return m_Director_Roll;
	}
	else if (dataName == "Director_Pitch")
	{
		return m_Director_Pitch;
	}
	else if (dataName == "Director_Heading")
	{
		return m_Director_Heading;
	}
	else if (dataName == "Director_Altitude")
	{
		return m_Director_Altitude;
	}
	else if (dataName == "Director_Airspeed")
	{
		return m_Director_Airspeed;
	}
	else if (dataName == "Director_Vertical_Speed")
	{
		return m_Director_Vertical_Speed;
	}
	else if (dataName == "Got_Data")
	{
		return (double)m_Got_Data;
	}
	else if (dataName == "Status_Active")
	{
		return (double)m_Status_Active;
	}
	else if (dataName == "GPS_Mode")
	{
		return (double)m_GPS_Mode;
	}
	else if (dataName == "GPS_Sats")
	{
		return (double)m_GPS_Sats;
	}
	else if (dataName == "Internal_Temp")
	{
		return m_Internal_Temp;
	}
	else if (dataName == "External_Temp")
	{
		return m_External_Temp;
	}
	else if (dataName == "Wind_Direction")
	{
		return m_Wind_Direction;
	}
	else if (dataName == "Wind_Speed")
	{
		return m_Wind_Speed;
	}
	else if (dataName == "Status_Colour1")
	{
		return (double)m_Status_Colour1;
	}
	else if (dataName == "Status_Colour2")
	{
		return (double)m_Status_Colour2;
	}
	else if (dataName == "Status_Colour3")
	{
		return (double)m_Status_Colour3;
	}
	else if (dataName == "Status_Colour4")
	{
		return (double)m_Status_Colour4;
	}
	else
	{
		printf("Error: unsupported data name \"%s\"\n", dataName.c_str());
		return 0;
	}
}


} // end namespace OpenGC

