/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

/**
 * Sets up the data class structure and methods for receiving a UDP data
 * packet transmitted by the simulation program.
 *
 * This program acts as a "server" receiving data from the flight simulation
 */

#include <stdio.h>
#include <iostream>
#include "FGDataSource.h"
#include "Constants.h"
#include "Globals.h"
#include "Debug.h"

namespace OpenGC {

// Constructor
FGDataSource::FGDataSource()
{ 
	// Initialize the data, but we're going to delay opening the sockets until all
	// the other data files and command line arguments have been read
	this->InitializeData();

	// Set initial state
	m_Airframe->SetStatus_Active(false);
	m_Airframe->SetDirector_Active(false);
	m_Airframe->SetGot_Data(false);
	m_Airframe->SetStatus_Text_NoData("WAITING FOR CONNECTION");

	m_Airframe->SetStatus_Text1("YAW");
	m_Airframe->SetStatus_Text2("ALT");
	m_Airframe->SetStatus_Text3("BNK");
	m_Airframe->SetStatus_Text4("VEL");
	m_Airframe->SetStatus_Colour1(1);
	m_Airframe->SetStatus_Colour2(1);
	m_Airframe->SetStatus_Colour3(1);
	m_Airframe->SetStatus_Colour4(1);


	// Set FlightGear buffer length is
	m_BufferLength = sizeof(FGData);

	// Create the buffer
	m_Buffer = new char[m_BufferLength];

	// Temp buffer used to extract message
	m_TempMsg = new char[m_BufferLength];
}

FGDataSource::~FGDataSource()
{
	m_Socket.close();
	delete[] m_Buffer;
	delete[] m_TempMsg;
}

bool FGDataSource::Open()
{
	// Get the host:port string from the Config system and parse it
	m_Host = globals->m_PrefManager->GetPrefS("FlightGearHost");
	m_ReceivePort = globals->m_PrefManager->GetPrefI("FlightGearPort");
	if (true)
	{
		printf("FGDataSourse: host \"%s\", port %d\n", 
				m_Host.c_str(), m_ReceivePort);
	}
	else
	{
		std::cerr << "FGDataSource: invalid configuration.\n";
		return false;
	}
	
	if (m_ReceivePort < 1025 || m_ReceivePort > 65535)
	{
		std::cerr << "FGDataSource: invalid port number.\n";
		return false;
	}

	// Must call this before any other net stuff
	netInit();

	// We'll assume the connection is valid until proved otherwise
	m_ValidConnection = true;

	// Try to open a socket
	if (!m_Socket.open(false))
	{
		printf("FlightGear data source: error opening socket\n");
		m_ValidConnection = false;
		return false;
	}

	m_Socket.setBlocking(false);

	if (m_Socket.bind(m_Host.c_str(), m_ReceivePort) == -1)
	{
		printf("FGDataSource: error binding to port %d\n", m_ReceivePort);
		m_ValidConnection = false;
		return false;
	}
	return true;
}

bool FGDataSource::GetData()
{
	if (!m_ValidConnection)
		return false;

	// Length of the message received from Flightgear
	int receivedLength = 0;

	// Actual length after flushing accumulated messages
	int finalReceivedLength = 0;

	// The code in the "do" block flushes the buffer so that only
	// the most recent message remains. This eliminates the build-up
	// of old messages in the network buffer (which we don't directly
	// control)
	do
	{
		receivedLength = m_Socket.recv(m_TempMsg, m_BufferLength, 0);

		if(receivedLength >= 0)
		{
			for(int i = 0; i < m_BufferLength; i++)
				m_Buffer[i] = m_TempMsg[i];

			finalReceivedLength = receivedLength;
		}
	} while(receivedLength >=0);

	// At this point, m_Buffer[] contains the most recent message
	if(finalReceivedLength>0)
	{
		assert(finalReceivedLength == sizeof(FGData)); // possible superfluous
		m_FDM = (FGData*) m_Buffer;
		return true;
	}
	else
		return false;
	
	return false;
}

bool FGDataSource::OnIdle()
{
   	// Abort if the connection isn't valid
   	if( !m_ValidConnection )
   	{
   		m_Airframe->SetStatus_Text_NoData("WAITING");
   		return false;
   	}

   	// Input data from the LAN socket
   	if ( !GetData() )
   	{
		m_Airframe->SetStatus_Text_NoData("ERROR");
   		return false;
   	}
   	else
   	{
		m_Airframe->SetGot_Data(true);
		m_Airframe->SetStatus_Active(true);
   	}


	// ++++++ Update Data +++++++++++++++++++++++++++++++++++++++++++++++++++++
	// -------Orientation--------------
	m_Airframe->SetRoll(m_FDM->Roll_Deg);
	m_Airframe->SetPitch(m_FDM->Pitch_Deg);
	m_Airframe->SetTrue_Heading(m_FDM->True_Heading_Deg);
	m_Airframe->SetTrack_Heading(360 - m_FDM->True_Heading_Deg);

	// -------Speeds-------------------
	m_Airframe->SetAirspeed_KT(m_FDM->Airspeed_KMpH);
	m_Airframe->SetVertical_Speed_FPM(m_FDM->Vertical_Speed_MpS);
	m_Airframe->SetGround_Speed_MS(m_FDM->Ground_Speed_KMpH);

	// -------Accelerations------------
	m_Airframe->SetAccel_Body_Fwd(m_FDM->Accel_Body_Fwd_MpS);
	m_Airframe->SetAccel_Body_Right(m_FDM->Accel_Body_Right_MpS);
	m_Airframe->SetAccel_Body_Down(m_FDM->Accel_Body_Down_MpS);

	// -------Altitude-----------------
	m_Airframe->SetAltitude_MSL_Meters(m_FDM->Altitude_MSL_Meters);
	m_Airframe->SetAltitude_AGL_Meters(m_FDM->Altitude_AGL_Meters);
	m_Airframe->SetBarometric_Pressure(m_FDM->Barometric_Pressure_Pa);

	// -------Position-----------------
	m_Airframe->SetLatitude(m_FDM->Latitude_Deg);
	m_Airframe->SetLongitude(m_FDM->Longitude_Deg);
	
	// -------Engine-------------------
	m_Airframe->SetEngine_RPM(m_FDM->Engine_RPM);
	double cht = (m_FDM->Engine_CHT - 32) * 5 / 9;
	m_Airframe->SetEngine_CHT(cht);
	double egt = (m_FDM->Engine_EGT - 32) * 5 / 9;
	m_Airframe->SetEngine_EGT(egt);
	m_Airframe->SetEngine_Mixture(m_FDM->Engine_Mixture * 8);
	m_Airframe->SetVoltage_Alternator(6.0);
	m_Airframe->SetVoltage_Battery(m_FDM->Electric_System_Voltage - 13);

	// -------Extra Stuff--------------
//	m_Airframe->SetInternal_Temp(30 + 2*sin(t / 100.0));
	m_Airframe->SetExternal_Temp(m_FDM->External_Temperature_C);
	m_Airframe->SetWind_Speed(m_FDM->Wind_Speed_KMpS);
	m_Airframe->SetWind_Direction(m_FDM->Wind_Direction_Deg);

	// -------Flight Director---------- (aka autopilot)
//	m_Airframe->SetDirector_Pitch(m_FDM->);
	m_Airframe->SetDirector_Roll(m_FDM->Director_Roll);
	m_Airframe->SetDirector_Heading(m_FDM->Director_Heading);
	m_Airframe->SetDirector_Altitude(m_FDM->Director_Altitude);
	m_Airframe->SetDirector_Airspeed(m_FDM->Director_Airspeed);
	m_Airframe->SetDirector_Vertical_Speed(m_FDM->Director_Vertical_Speed);

	return true;
}

} // end namespace OpenGC
