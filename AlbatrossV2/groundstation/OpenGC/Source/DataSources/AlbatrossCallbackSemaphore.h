/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef AlbatrossCallbackSemaphore_h
#define AlbatrossCallbackSemaphore_h

#include <pthread.h>

/** 
 * This struct serves as semaphore, signaling when new data arrived and is ready for processing
 * structure holds pointers to instance variables (the dataReceived
 * flag et. al.) ans to the callback function, via the user_data argument.
 */
typedef struct AlbatrossCallbackSemaphore
{
	// true, when LWD_DATA command was received and is ready for processing
	bool dataReceived;
	
	// true, when LWC_EXIT_MANUAL_MODE command was received and is ready for processing
	bool exitManualModeReceived;

	// true, when LWC_ENTER_MANUAL_MODE command was received and is ready for processing
	bool enterManualModeReceived;

	// true, when LWC_ADD_WAYPOINT command was received and is ready for processing
	bool addWaypointReceived;

	// true, when LWC_REMOVE_WAYPOINT command was received and is ready for processing
	bool removeWaypointReceived;

	// locks LWD_DATA processing logic
	pthread_mutex_t *dataLock;
	
	// locks LWC_EXIT_MANUAL_MODE processing logic
	pthread_mutex_t *exitManualModeLock;

	// locks LWC_ENTER_MANUAL_MODE processing logic
	pthread_mutex_t *enterManualModeLock;

	// locks LWC_ADD_WAYPOINT processing logic
	pthread_mutex_t *addWaypointLock;

	// locks LWC_REMOVE_WAYPOINT processing logic
	pthread_mutex_t *removeWaypointLock;


	AlbatrossCallbackSemaphore()
	{
		dataReceived = false;
		exitManualModeReceived = false;
		enterManualModeReceived = false;
		addWaypointReceived = false;
		removeWaypointReceived = false;

		dataLock = new pthread_mutex_t();
		exitManualModeLock = new pthread_mutex_t();
		enterManualModeLock = new pthread_mutex_t();
		addWaypointLock = new pthread_mutex_t();
		removeWaypointLock = new pthread_mutex_t();

		pthread_mutex_init(dataLock, NULL);
		pthread_mutex_init(exitManualModeLock, NULL);
		pthread_mutex_init(enterManualModeLock, NULL);
		pthread_mutex_init(addWaypointLock, NULL);
		pthread_mutex_init(removeWaypointLock, NULL);
	}

	~AlbatrossCallbackSemaphore()
	{
		pthread_mutex_destroy(dataLock);
		pthread_mutex_destroy(exitManualModeLock);
		pthread_mutex_destroy(enterManualModeLock);
		pthread_mutex_destroy(addWaypointLock);
		pthread_mutex_destroy(removeWaypointLock);

		delete dataLock;
		delete exitManualModeLock;
		delete enterManualModeLock;
		delete addWaypointLock;
		delete removeWaypointLock;
	}
   	
};


#endif //AlbatrossCallbackSemaphore_h
