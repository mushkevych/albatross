/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef AirframeDataContainer_h
#define AirframeDataContainer_h

#include <string>
#include "DataContainer.h"

using namespace std;

namespace OpenGC
{

class AirframeDataContainer : public DataContainer
{
	protected:
		//
		//------Heading and Location-------
		//

		/** Roll in degrees around the axis of flight, right roll + */
		double m_Roll;

		/** Pitch in degrees from horizontal, pitch up + */
		double m_Pitch;

		/** True heading in degrees */
		double m_True_Heading;

		/** Track over ground heading in degrees */
		double m_Track_Heading;

		/** Latitude in degrees (with fractional degrees) North +, South - */
		double m_Latitude;

		/** Longitude in degrees (with fractional degrees) East +, West - */
		double m_Longitude;

		/** Accelerations in body frame (used for turn coordinator), G's. */
		double m_Accel_Body_Fwd;
		double m_Accel_Body_Right;
		double m_Accel_Body_Down;

		//
		//--------------Speed---------------
		//

		/** True airspeed in knots */
		double m_Airspeed_KT;

		/** Ground speed in meters/sec */
		double m_Ground_Speed_MS;

		/** Rate of climb or descent, in feet per minute */
		double m_Vertical_Speed_FPM;

		//
		//------------Altitude---------------
		//

		/** Altitude in feet above ground level */
		double m_Altitude_AGL_Meters;

		/** Altitude in feet above sea level */
		double m_Altitude_MSL_Meters;

		/** Barometric pressure hPa */
		double m_Barometric_Pressure;

		//
		//--------------Engine Parameters-----------------
		//

		/** Engine revolutions per minute */
		double m_Engine_RPM;

		/** Engine cylinder head temperature (deg. C) */
		double m_Engine_CHT;

		/** Engine exhaust gas temperature (deg. C) */
		double m_Engine_EGT;

		/** Engine mixture (as a percentage, 0 for lean, 100 for rich) */
		double m_Engine_Mixture;

		/** Alternator voltage */
		double m_Voltage_Alternator;

		/** Battery voltage */
		double m_Voltage_Battery;

		//
		//--------------Flight Director-------------------
		//

		/** Flight director Active */
		bool m_Director_Active;

		/** Flight director Bank */
		double m_Director_Roll;

		/** Flight director Pitch */
		double m_Director_Pitch;

		/** Flight director Heading */
		double m_Director_Heading;

		/** Flight director Altitude */
		double m_Director_Altitude;

		/** Flight director Airspeed */
		double m_Director_Airspeed;

		/** Flight director Vertical Speed (FPM) */
		double m_Director_Vertical_Speed;

		//
		//--------------Albatross Specific Stuff----------
		//

		/** Do we have incoming data */
		bool m_Got_Data;

		/** Status indicator panel Active */
		bool m_Status_Active;

		/** Albatross GPS Mode */
		int m_GPS_Mode;

		/** Albatross GPS Number of Satellites in view */
		int m_GPS_Sats;

		/** Albatross internal temperature */
		double m_Internal_Temp;

		/** Albatross external temperature */
		double m_External_Temp;

		/** Albatross wind direction estimate */
		double m_Wind_Direction;

		/** Albatross wind speed estimate */
		double m_Wind_Speed;

		/** Albatross status text strings (max 3 chars) */
		char *m_Status_Text1;
		char *m_Status_Text2;
		char *m_Status_Text3;
		char *m_Status_Text4;

		/** Albatross status text colour (0 = off, 1 = red, 2 = green) */
		int m_Status_Colour1;
		int m_Status_Colour2;
		int m_Status_Colour3;
		int m_Status_Colour4;

		/** Text area for when we have no incoming data */
		char *m_Status_Text_NoData;

	public:

		AirframeDataContainer();
		virtual ~AirframeDataContainer() {};

		//
		//------Heading and Location-------
		//

		double GetRoll() {return m_Roll;}
		void SetRoll(double roll) {m_Roll = roll;}

		double GetPitch() {return m_Pitch;}
		void SetPitch(double pitch) {m_Pitch = pitch;}

		double GetTrue_Heading() {return m_True_Heading;}
		void SetTrue_Heading(double true_Heading) {m_True_Heading = true_Heading;}

		double GetTrack_Heading() {return m_Track_Heading;}
		void SetTrack_Heading(double track_Heading) {m_Track_Heading = track_Heading;}

		double GetLatitude() {return m_Latitude;}
		void SetLatitude(double latitude) {m_Latitude = latitude;}

		double GetLongitude() {return m_Longitude;}
		void SetLongitude(double longitude) {m_Longitude = longitude;}

		double GetAccel_Body_Fwd() {return m_Accel_Body_Fwd;}
		void SetAccel_Body_Fwd(double accel_Body_Fwd) {m_Accel_Body_Fwd = accel_Body_Fwd;}

		double GetAccel_Body_Right() {return m_Accel_Body_Right;}
		void SetAccel_Body_Right(double accel_Body_Right) {m_Accel_Body_Right = accel_Body_Right;}

		double GetAccel_Body_Down() {return m_Accel_Body_Down;}
		void SetAccel_Body_Down(double accel_Body_Down) {m_Accel_Body_Down = accel_Body_Down;}

		//
		//--------------Speed---------------
		//

		double GetAirspeed_KT() {return m_Airspeed_KT;}
		void SetAirspeed_KT(double airspeed_KT) {m_Airspeed_KT = airspeed_KT;}

		double GetGround_Speed_MS() {return m_Ground_Speed_MS;}
		void SetGround_Speed_MS(double ground_Speed_MS) {m_Ground_Speed_MS = ground_Speed_MS;}

		double GetVertical_Speed_FPM() {return m_Vertical_Speed_FPM;}
		void SetVertical_Speed_FPM(double vertical_Speed_FPM) {m_Vertical_Speed_FPM = vertical_Speed_FPM;}

		//
		//------------Altitude---------------
		//

		double GetAltitude_AGL_Meters() {return m_Altitude_AGL_Meters;}
		void SetAltitude_AGL_Meters(double altitude_AGL_Feet) {m_Altitude_AGL_Meters = altitude_AGL_Feet;}

		double GetAltitude_MSL_Meters() {return m_Altitude_MSL_Meters;}
		void SetAltitude_MSL_Meters(double altitude_MSL_Feet) {m_Altitude_MSL_Meters = altitude_MSL_Feet;}

		double GetBarometric_Pressure() {return m_Barometric_Pressure;}
		void SetBarometric_Pressure(double barometric_Pressure) {m_Barometric_Pressure = barometric_Pressure;}

		//
		//--------------Engine Parameters-----------------
		//

		double GetEngine_RPM() {return m_Engine_RPM;}
		void SetEngine_RPM(double engine_RPM) {m_Engine_RPM = engine_RPM;}

		double GetEngine_CHT() {return m_Engine_CHT;}
		void SetEngine_CHT(double engine_CHT) {m_Engine_CHT = engine_CHT;}

		double GetEngine_EGT() {return m_Engine_EGT;}
		void SetEngine_EGT(double engine_EGT) {m_Engine_EGT = engine_EGT;}

		double GetEngine_Mixture() {return m_Engine_Mixture;}
		void SetEngine_Mixture(double engine_Mixture) {m_Engine_Mixture = engine_Mixture;}

		double GetVoltage_Alternator() {return m_Voltage_Alternator;}
		void SetVoltage_Alternator(double voltage_Alternator) {m_Voltage_Alternator = voltage_Alternator;}

		double GetVoltage_Battery() {return m_Voltage_Battery;}
		void SetVoltage_Battery(double voltage_Battery) {m_Voltage_Battery = voltage_Battery;}

		//
		//--------------Flight Director-------------------
		//

		bool GetDirector_Active() {return m_Director_Active;}
		void SetDirector_Active(bool director_Active) {m_Director_Active = director_Active;}

		double GetDirector_Roll() {return m_Director_Roll;}
		void SetDirector_Roll(double director_Roll) {m_Director_Roll = director_Roll;}

		double GetDirector_Pitch() {return m_Director_Pitch;}
		void SetDirector_Pitch(double director_Pitch) {m_Director_Pitch = director_Pitch;}

		double GetDirector_Heading() {return m_Director_Heading;}
		void SetDirector_Heading(double director_Heading) {m_Director_Heading = director_Heading;}

		double GetDirector_Altitude() {return m_Director_Altitude;}
		void SetDirector_Altitude(double director_Altitude) {m_Director_Altitude = director_Altitude;}

		double GetDirector_Airspeed() {return m_Director_Airspeed;}
		void SetDirector_Airspeed(double director_Airspeed) {m_Director_Airspeed = director_Airspeed;}

		double GetDirector_Vertical_Speed() {return m_Director_Vertical_Speed;}
		void SetDirector_Vertical_Speed(double director_Vertical_Speed) {m_Director_Vertical_Speed = director_Vertical_Speed;}

		//
		//--------------Albatross Specific Stuff----------
		//

		bool GetGot_Data() {return m_Got_Data;}
		void SetGot_Data(bool got_Data) {m_Got_Data = got_Data;}

		bool GetStatus_Active() {return m_Status_Active;}
		void SetStatus_Active(bool status_Active) {m_Status_Active = status_Active;}

		int GetGPS_Mode() {return m_GPS_Mode;}
		void SetGPS_Mode(int gps_Mode) {m_GPS_Mode = gps_Mode;}

		int GetGPS_Sats() {return m_GPS_Sats;}
		void SetGPS_Sats(int gps_Sats) {m_GPS_Sats = gps_Sats;}

		double GetInternal_Temp() {return m_Internal_Temp;}
		void SetInternal_Temp(double internal_Temp) {m_Internal_Temp = internal_Temp;}

		double GetExternal_Temp() {return m_External_Temp;}
		void SetExternal_Temp(double external_Temp) {m_External_Temp = external_Temp;}

		double GetWind_Direction() {return m_Wind_Direction;}
		void SetWind_Direction(double wind_Direction) {m_Wind_Direction = wind_Direction;}

		double GetWind_Speed() {return m_Wind_Speed;}
		void SetWind_Speed(double wind_Speed) {m_Wind_Speed = wind_Speed;}

		char* GetStatus_Text1() {return m_Status_Text1;}
		void SetStatus_Text1(char* status_Text1) {m_Status_Text1 = status_Text1;}

		char* GetStatus_Text2() {return m_Status_Text2;}
		void SetStatus_Text2(char* status_Text2) {m_Status_Text2 = status_Text2;}

		char* GetStatus_Text3() {return m_Status_Text3;}
		void SetStatus_Text3(char* status_Text3) {m_Status_Text3 = status_Text3;}

		char* GetStatus_Text4() {return m_Status_Text4;}
		void SetStatus_Text4(char* status_Text4) {m_Status_Text4 = status_Text4;}

		int GetStatus_Colour1() {return m_Status_Colour1;}
		void SetStatus_Colour1(int status_Colour1) {m_Status_Colour1 = status_Colour1;}

		int GetStatus_Colour2() {return m_Status_Colour2;}
		void SetStatus_Colour2(int status_Colour2) {m_Status_Colour2 = status_Colour2;}

		int GetStatus_Colour3() {return m_Status_Colour3;}
		void SetStatus_Colour3(int status_Colour3) {m_Status_Colour3 = status_Colour3;}

		int GetStatus_Colour4() {return m_Status_Colour4;}
		void SetStatus_Colour4(int status_Colour4) {m_Status_Colour4 = status_Colour4;}

		char* GetStatus_Text_NoData() {return m_Status_Text_NoData;}
		void SetStatus_Text_NoData(char* status_Text_NoData) {m_Status_Text_NoData = status_Text_NoData;}


		void InitializeData();

		int GetAirframeData_I(const string& dataName);
		double GetAirframeData_D(const string& dataName);

};

} // end namespace OpenGC

#endif
