/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include "AlbatrossDataSource.h"
#include "Constants.h"
#include "Debug.h"
#include "Globals.h"
#include "WaypointList.h"


/** 
 * Callback from the receiver thread, sets flag to tell main thread new data is here.
 * The callback has to be extern "C" or else the linker complains fearsomely.
 */
extern "C" void ReceiverThreadCallback(int cmd, void *user_data)
{
	AlbatrossCallbackSemaphore *data = (AlbatrossCallbackSemaphore *)user_data;

	if(cmd == LWC_DATA) {
		//printf("AlbatrossDataSource: Received LWD_DATA command.\n"); 
		pthread_mutex_lock(data->dataLock);
		data->dataReceived = true;
		pthread_mutex_unlock(data->dataLock);
	} else if(cmd == LWC_EXIT_MANUAL_MODE) {
		pthread_mutex_lock(data->exitManualModeLock);
		data->exitManualModeReceived = true;
		pthread_mutex_unlock(data->exitManualModeLock);
	} else if(cmd == LWC_ENTER_MANUAL_MODE || cmd == LWC_AUTOPILOT_EXIT) {
		pthread_mutex_lock(data->enterManualModeLock);
		data->enterManualModeReceived = true;
		pthread_mutex_unlock(data->enterManualModeLock);
	} else if(cmd == LWC_ADD_WAYPOINT) {
		pthread_mutex_lock(data->addWaypointLock);
		data->addWaypointReceived = true;
		pthread_mutex_unlock(data->addWaypointLock);
	} else if(cmd == LWC_REMOVE_WAYPOINT) {
		pthread_mutex_lock(data->removeWaypointLock);
		data->removeWaypointReceived = true;
		pthread_mutex_unlock(data->removeWaypointLock);
	}
}



namespace OpenGC {

AlbatrossDataSource::AlbatrossDataSource()
{
	this->InitializeData();

	m_Airframe->SetStatus_Active(false);
	m_Airframe->SetDirector_Active(false);
	m_Airframe->SetGot_Data(false);
	m_Airframe->SetStatus_Text_NoData("WAITING FOR CONNECTION");

	m_Airframe->SetStatus_Text1("YAW");
	m_Airframe->SetStatus_Text2("ALT");
	m_Airframe->SetStatus_Text3("BNK");
	m_Airframe->SetStatus_Text4("VEL");
	m_Airframe->SetStatus_Colour1(2);
	m_Airframe->SetStatus_Colour2(2);
	m_Airframe->SetStatus_Colour3(2);
	m_Airframe->SetStatus_Colour4(2);

	// Set up the datagram for receiving
	datagram_clear(&DgRecv);
	datagram_init(&DgRecv, DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_receive_bitmask(&DgRecv, 0xFFFF);
	parsegram_init(&PgRecv);
	
	// Set up the semaphore and callback
	semaphore = new AlbatrossCallbackSemaphore();
	datagram_set_callback(&DgRecv, ReceiverThreadCallback, semaphore);
	
	// Start the receiver thread
	pthread_t rcv;
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *)&DgRecv);

	// Done
	LogPrintf("AlbatrossDataSource: Constructed\n");
}

AlbatrossDataSource::~AlbatrossDataSource()
{
	// Destructor
	delete semaphore;
}

bool AlbatrossDataSource::OnIdle()
{
	bool updateUI = false;
	
	if (semaphore->dataReceived) {
		updateUI = true;
		
		// Process the datagram
		datagram_process(&DgRecv, 0xFFFF);
		datagram_process_payload(&PgRecv, &DgRecv, 0, 0xFFFF);
		
		m_Airframe->SetGPS_Mode(parsegram_get_as_byte(&PgRecv, STATE_D_GPS_MODE));
		m_Airframe->SetGPS_Sats(parsegram_get_as_byte(&PgRecv, STATE_D_SATS_IN_VIEW));

		m_Airframe->SetGot_Data(true);
		m_Airframe->SetStatus_Active(true);

		// ++++++ Update Data +++++++++++++++++++++++++++++++++++++++++++++++++++++
		// -------Orientation--------------
		m_Airframe->SetRoll(parsegram_get_as_float(&PgRecv, STATE_D_ROLL_ANGLE));
		m_Airframe->SetPitch(parsegram_get_as_float(&PgRecv, STATE_D_PITCH_ANGLE));
		m_Airframe->SetTrue_Heading(parsegram_get_as_float(&PgRecv, STATE_D_HEADING));
//		m_Airframe->SetTrack_Heading(parsegram_get_as_float(&PgRecv, STATE_D_HEADING));

		// -------Speeds-------------------
		m_Airframe->SetAirspeed_KT(parsegram_get_as_float(&PgRecv, STATE_D_SPEED_LOCAL_X));
		m_Airframe->SetGround_Speed_MS(parsegram_get_as_float(&PgRecv, STATE_D_SPEED_GROUND));
		m_Airframe->SetVertical_Speed_FPM(parsegram_get_as_float(&PgRecv, STATE_D_SPEED_LOCAL_Z));

		// -------Accelerations------------
		m_Airframe->SetAccel_Body_Fwd(parsegram_get_as_float(&PgRecv, STATE_D_ACCELERATION_X));
		m_Airframe->SetAccel_Body_Right(parsegram_get_as_float(&PgRecv, STATE_D_ACCELERATION_Y));
		m_Airframe->SetAccel_Body_Down(parsegram_get_as_float(&PgRecv, STATE_D_ACCELERATION_Z));

		// -------Altitude-----------------
		m_Airframe->SetAltitude_MSL_Meters(parsegram_get_as_float(&PgRecv, STATE_D_ALTITUDE));
		m_Airframe->SetAltitude_AGL_Meters(parsegram_get_as_float(&PgRecv, STATE_D_ALTITUDE));

		// -------Position-----------------
		m_Airframe->SetLatitude(parsegram_get_as_float(&PgRecv, STATE_D_LATITUDE));
		m_Airframe->SetLongitude(parsegram_get_as_float(&PgRecv, STATE_D_LONGITUDE));

		// -------Hardware-----------------
		m_Airframe->SetExternal_Temp(parsegram_get_as_float(&PgRecv, STATE_D_EXTERNAL_TEMPERATURE));
		m_Airframe->SetBarometric_Pressure(parsegram_get_as_float(&PgRecv, STATE_D_BAROMETRIC_PRESSURE));
		m_Airframe->SetVoltage_Battery(parsegram_get_as_float(&PgRecv, STATE_D_VOLTAGE_BATTERY));

		// -------Director-----------------
		m_Airframe->SetDirector_Airspeed(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_SPEED_X));
		m_Airframe->SetDirector_Altitude(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_ALTITUDE));
		m_Airframe->SetDirector_Vertical_Speed(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_SPEED_Z));
		m_Airframe->SetDirector_Heading(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_HEADING_ANGLE));
		m_Airframe->SetDirector_Pitch(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_PITCH_ANGLE));
		m_Airframe->SetDirector_Roll(parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_ROLL_ANGLE));

		FlightCourse *course = globals->m_NavDatabase->GetFlightCourse();
		double wp_dir_lat = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_WAYPOINT_LATITUDE);
		double wp_dir_lon = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_WAYPOINT_LONGITUDE);
		double wp_dir_alt = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_DIRECTOR_WAYPOINT_ALTITUDE);
		course->SetDirector_Point(wp_dir_lat, wp_dir_lon, wp_dir_alt);

		// Reset the flag
		pthread_mutex_lock(semaphore->dataLock);
		semaphore->dataReceived = false;
		pthread_mutex_unlock(semaphore->dataLock);
	}

	if (semaphore->exitManualModeReceived) {
		updateUI = true;
		m_Airframe->SetDirector_Active(true);
		LogPrintf("AlbatrossDataSource: Autopilot ON.\n");

		// Reset the flag
		pthread_mutex_lock(semaphore->exitManualModeLock);
		semaphore->exitManualModeReceived = false;
		pthread_mutex_unlock(semaphore->exitManualModeLock);
	}

	if (semaphore->enterManualModeReceived) {
		updateUI = true;
		m_Airframe->SetDirector_Active(false);
		LogPrintf("AlbatrossDataSource: Autopilot OFF.\n");

		// Reset the flag
		pthread_mutex_lock(semaphore->enterManualModeLock);
		semaphore->enterManualModeReceived = false;
		pthread_mutex_unlock(semaphore->enterManualModeLock);
	}

	if (semaphore->addWaypointReceived) {
		updateUI = true;

		// method goes thru the parsegram and forms Waypoint base on arrived data
		datagram_process_payload(&PgRecv, &DgRecv, 0, 0xFFFF);
		int _id = parsegram_get_as_integer(&PgRecv, AUTOPILOT_D_WAYPOINT_ID);
		int lwc = parsegram_get_as_integer(&PgRecv, AUTOPILOT_D_WAYPOINT_LWC);
		double longitude = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_WAYPOINT_LONGITUDE);
		double latitude = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_WAYPOINT_LATITUDE);
		double altitude = parsegram_get_as_float(&PgRecv, AUTOPILOT_D_WAYPOINT_ALTITUDE);

		char buffer[32];
		sprintf(buffer,"%d",_id);

		WaypointList *waypointList = globals->m_NavDatabase->GetWaypointList();
		waypointList->RemoveWaypoint(buffer);
		waypointList->AddWaypoint(buffer, latitude, longitude, altitude, lwc);
		waypointList->SetTypeOfInit(waypointList->INIT_FROM_STREAM);

		// Reset the flag
		pthread_mutex_lock(semaphore->addWaypointLock);
		semaphore->addWaypointReceived = false;
		pthread_mutex_unlock(semaphore->addWaypointLock);
	}

	if (semaphore->removeWaypointReceived) {
		updateUI = true;

		// method goes thru the parsegram and forms Waypoint base on arrived data
		datagram_process_payload(&PgRecv, &DgRecv, 0, 0xFFFF);
		int _id = parsegram_get_as_integer(&PgRecv, AUTOPILOT_D_WAYPOINT_ID);

		char buffer[32];
		sprintf(buffer,"%d",_id);

		WaypointList *waypointList = globals->m_NavDatabase->GetWaypointList();
		waypointList->RemoveWaypoint(buffer);

		// Reset the flag
		pthread_mutex_lock(semaphore->removeWaypointLock);
		semaphore->removeWaypointReceived = false;
		pthread_mutex_unlock(semaphore->removeWaypointLock);
	}

	return updateUI; //semaphore->dataReceived || semaphore->enableDisableReceived; // should be true once data is coming in
}

} // end namespace OpenGC

