/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef AlbatrossDataSource_h
#define AlbatrossDataSource_h

#include "DataSource.h"
#include "AlbatrossCallbackSemaphore.h"

extern "C" {
#include "../../../../onboard/protocol/datagram.h"
#include "../../../../onboard/protocol/parsegram.h"
#include "../../../../onboard/protocol/protocol.h"
}

namespace OpenGC
{

class AlbatrossDataSource : public DataSource
{
	public:
		AlbatrossDataSource();
		~AlbatrossDataSource();

		// The money function
		bool OnIdle();
		
	private:
		// class variables here
		datagrizzle_t DgRecv;
		parsegram_t PgRecv;

    // struct for data transmission between Albatros Data Source and other threads
    AlbatrossCallbackSemaphore *semaphore;            
		
};

} // end namespace OpenGC

#endif
