/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "WaypointList.h"
#include "WaypointGeoObj.h"
#include "Constants.h"

namespace OpenGC
{

using namespace std;

WaypointList::WaypointList()
{
	m_TypeOfInit = NOT_INITIALIZED;
}

WaypointList::~WaypointList()
{

}

// Define way of initialization
bool WaypointList::SetTypeOfInit(int typeOfInit)
{
	m_TypeOfInit = typeOfInit;
}

bool WaypointList::LoadData(const string& fileName)
{
	// Buffer used to read from file
	string lineData;

	// Open the input file for reading
	ifstream inputFile( fileName.c_str() );

	// Test to see if the file is open
	if( inputFile.is_open() == 0 )
	{
		printf("Error: unable to load the waypoint database file.\n");
		return false;
	}

	while (inputFile.eof() != 1)
	{
		// Extract a line
		getline(inputFile, lineData);
		
		// Ignore blank lines
		if (lineData == "" || (lineData.size() == 1 && isspace(lineData[0]))) // FIXME need python string.strip() operation...
			continue;
		
		// Ignore comments (lines starting with "#")
		if (lineData[0] == '#')
			continue;

		// Variables to parse into
		int _id, lwc;
		double lat, lon, alt;
		char buffer[32];

		if(sscanf(lineData.c_str(), "%i,%lf,%lf,%lf,%i", &_id, &lat, &lon, &alt, &lwc) == 5)
		{
			//printf("\nParsed waypoint: %i,%lf,%lf,%lf,%i", _id, lat, lon, alt, lwc);
			sprintf(buffer,"%d",_id);
			this->AddWaypoint(buffer, lat, lon, alt, lwc);
		}
	}

	// Everything worked ok
	m_TypeOfInit = INIT_FROM_FILE;
	return true;
}

// Add a new waypoint to the Route (WaypointList); returns true if successful, else false
bool WaypointList::AddWaypoint(string _id, double lat, double lon, double alt, int lwc)
{
	WaypointGeoObj* pWaypoint = new WaypointGeoObj();
	pWaypoint->SetIdentification(_id);
	pWaypoint->SetDegreeLat(lat);
	pWaypoint->SetDegreeLon(lon);
	pWaypoint->SetAltitudeMeters(alt);
	pWaypoint->SetStyle(WaypointGeoObj::STYLE_FUNDAMENTAL);
	pWaypoint->SetLwc(lwc);

	// mercator coordinates
	double northing, easting;
	GeographicObject::LatLonToMercator(lat, lon, northing, easting);
	pWaypoint->SetMercatorMeters(northing, easting);
	this->push_back(pWaypoint);

	// Everything worked ok
	return true;
}

// Remove existing waypoint from the Route (WaypointList); returns true if successful, else false
bool WaypointList::RemoveWaypoint(string _id)
{
	bool result = false;
	WaypointList::iterator iter;

	for (iter = begin(); iter != end(); ++iter)
	{
		string current_id = (*iter)->GetIdentification();
		if (current_id == _id)
		{
			iter = erase(iter);
			result = true;
		}
	}

	return result;
}

} // end namespace OpenGC

