/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include "FlightCourse.h"
#include "WaypointGeoObj.h"

namespace OpenGC
{

using namespace std;

FlightCourse::FlightCourse()
{
	m_Director_Point = NULL;
}

FlightCourse::~FlightCourse()
{
	if (m_Director_Point != NULL)
	{
		delete m_Director_Point;
	}
}

// Set a CoursePoint that Director (Autopilot wants to reach)
void FlightCourse::SetDirector_Point(double lat, double lon, double alt)
{
	if (m_Director_Point != NULL)
	{
		delete m_Director_Point;
	}

	m_Director_Point = new GeographicObject();
	m_Director_Point->SetDegreeLat(lat);
	m_Director_Point->SetDegreeLon(lon);
	m_Director_Point->SetAltitudeMeters(alt);

	// mercator coordinates
	double northing, easting;
	GeographicObject::LatLonToMercator(lat, lon, northing, easting);
	m_Director_Point->SetMercatorMeters(northing, easting);
}

// Point (GeographicObject) that Director (Autopilot) wants to reach
GeographicObject* FlightCourse::GetDirector_Point()
{
	return m_Director_Point;
}

// Add a new Point to the FlightCourse; returns true if successful, else false
bool FlightCourse::AddFlightPoint(double lat, double lon, double alt)
{
	WaypointGeoObj* flight_point = new WaypointGeoObj();
	flight_point->SetDegreeLat(lat);
	flight_point->SetDegreeLon(lon);
	flight_point->SetAltitudeMeters(alt);

	// mercator coordinates
	double northing, easting;
	GeographicObject::LatLonToMercator(lat, lon, northing, easting);
	flight_point->SetMercatorMeters(northing, easting);
	this->push_back(flight_point);

	// Everything worked ok
	return true;
}

} // end namespace OpenGC

