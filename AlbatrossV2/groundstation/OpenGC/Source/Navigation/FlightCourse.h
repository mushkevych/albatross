/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#ifndef FlightCourse_h
#define FlightCourse_h

#include <vector>
#include "GeographicObjectList.h"

namespace OpenGC
{
/*
 * Class FlightCourse is used to hold information about path that airplane _has_ flown by given moment
 */
class FlightCourse : public GeographicObjectList
{
public:
	FlightCourse();
	~FlightCourse();

	// Set a Point that Director (Autopilot wants to reach)
	void SetDirector_Point(double lat, double lon, double alt);

	// Point (GeographicObject) that Director (Autopilot) wants to reach
	GeographicObject* GetDirector_Point();

	// Add a new Point to the FlightCourse; returns true if successful, else false
	bool AddFlightPoint(double lat, double lon, double alt);

protected:
	GeographicObject *m_Director_Point;
};

} // end namespace OpenGC

#endif

