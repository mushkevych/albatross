/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

/**
 * The base class for all gauges in the OpenGC object model.
 * GaugePanel's define a client rendering space and an associated
 * orthographic projection. Use of a client rendering space, in
 * the form of a glRenderWindow(), allows for greatly simplified
 * clipping, since objects drawn outside of the render window
 * are not displayed. The client window calculations are for the
 * most part used to manage the placement of Gauges (i.e.
 * the intent is to use Gauges for drawing, rather than
 * drawing within a Gauge Panel, in order to promote reusability of
 * code.
 *
 * GaugePanel's contain one or more Gauges, which also define
 * client rendering space and a viewport transform.
 */

#ifndef GaugePanel_h
#define GaugePanel_h

#include "FontManager.h"
#include "Gauge.h"
#include "RenderObject.h"
#include "XMLNode.h"

namespace OpenGC
{

class GaugePanel: public RenderObject
{
	public:
		GaugePanel(string& gaugePanelName);
		virtual ~GaugePanel();

		/** Overloaded render method */
		void Render();

		/** Set up using XML options. Pass it the <Panel> node. */
		void InitFromXMLNode(XMLNode gaugeNode);

		/** Add a gauge */
		void AddGauge(Gauge* pGauge);

		/** Overloaded method for setting the monitor calibration */
		void SetUnitsPerPixel(double unitsPerPixel);

		/** Set the x and y scale of the gauge panel (and member components) */
		void SetScale(double xScale, double yScale);

		/** Recalculates placement of the gauge panel in the window */
		void RecalcWindowPlacement();

		/** Resets the gauge panel coordinate system before and after rendering gauges */
		void ResetPanelCoordinateSystem();

		/** Return true if the click is inside the gauge panel
		    If true, tests gauges prior to returning */
		bool ClickTest(int button, int state, int x, int y);

		/** Determine whether or not to draw the gauge panel outline */
		void SetPanelOutline(bool outline){m_DrawPanelOutline = outline;};

	protected:

		/** Draw the gauge panel outline */
		void DrawPanelOutline();

		/** All of the gauge */
		std::list<Gauge*> m_GaugeList;

		/** The name of gauge panel */
		string m_gaugePanelName;

		/** The number of gauge in this gauge panel */
		int m_NumGauges;

		/** Whether or not to draw a blue line around the gauge panel */
		bool m_DrawPanelOutline;
};

} // end namespace OpenGC

#endif
