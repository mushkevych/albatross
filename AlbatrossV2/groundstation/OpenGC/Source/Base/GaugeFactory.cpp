/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include "AlbatrossAnnunciator.h"
#include "AltitudeTape.h"
#include "AltitudeTicker.h"
#include "ArtificialHorizon.h"
#include "Bargraph.h"
#include "PieDial.h"
#include "FlightDirector.h"
#include "FPSIndicator.h"
#include "GaugeFactory.h"
#include "Gmeter.h"
#include "HeadingIndicator.h"
#include "MarkedDial.h"
#include "NavDisplay.h"
#include "PFDOverlay.h"
#include "SpeedTape.h"
#include "SpeedTicker.h"
#include "Tachometer.h"
#include "VSI.h"

namespace OpenGC
{

Gauge* GaugeFactory::CreateGaugeInstance(XMLNode gaugeNode)
{
	Gauge *pGauge;
	string gaugeType = gaugeNode.GetProperty("type");

	if(gaugeType == "AlbatrossAnnunciator")
	{
		pGauge = new AlbatrossAnnunciator(gaugeNode);
	}
	else if(gaugeType == "AltitudeTape")
	{
		pGauge = new AltitudeTape(gaugeNode);
	}
	else if(gaugeType == "AltitudeTicker")
	{
		pGauge = new AltitudeTicker(gaugeNode);
	}
	else if(gaugeType == "ArtificialHorizon")
	{
		pGauge = new ArtificialHorizon(gaugeNode);
	}
	else if(gaugeType == "Bargraph")
	{
		pGauge = new Bargraph(gaugeNode);
	}
	else if(gaugeType == "FlightDirector")
	{
		pGauge = new FlightDirector(gaugeNode);
	}
	else if(gaugeType == "FPSIndicator")
	{
		pGauge = new FPSIndicator(gaugeNode);
	}
	else if(gaugeType == "Gmeter")
	{
		pGauge = new Gmeter(gaugeNode);
	}
	else if(gaugeType == "HeadingIndicator")
	{
		pGauge = new HeadingIndicator(gaugeNode);
	}
	else if(gaugeType == "MarkedDial")
	{
		pGauge = new MarkedDial(gaugeNode);
	}
	else if(gaugeType == "NavDisplay")
	{
		pGauge = new NavDisplay(gaugeNode);
	}
	else if(gaugeType == "PFDOverlay")
	{
		pGauge = new PFDOverlay(gaugeNode);
	}
	else if(gaugeType == "PieDial")
	{
		pGauge = new PieDial(gaugeNode);
	}
	else if(gaugeType == "SpeedTape")
	{
		pGauge = new SpeedTape(gaugeNode);
	}
	else if(gaugeType == "SpeedTicker")
	{
		pGauge = new SpeedTicker(gaugeNode);
	}
	else if(gaugeType == "Tachometer")
	{
		pGauge = new Tachometer(gaugeNode);
	}
	else if(gaugeType == "VSI")
	{
		pGauge = new VSI(gaugeNode);
	}
	else
	{
		printf("Error: unsupported gauge type \"%s\"\n", gaugeType.c_str());
		return NULL;
	}

	return pGauge;
}

};
