/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

#include <stdio.h>

#include "Debug.h"
#include "GaugeFactory.h"
#include "GaugePanel.h"
#include "Globals.h"

namespace OpenGC
{

GaugePanel::GaugePanel(string& gaugePanelName)
{
	m_gaugePanelName = gaugePanelName;

	m_NumGauges = 0;
	m_DrawPanelOutline = false;

	m_Scale.y = 1.0;
	m_Scale.x = 1.0;

	m_PhysicalPosition.x = 0;
	m_PhysicalPosition.y = 0;

	m_PixelPosition.x = 0;
	m_PixelPosition.y = 0;

	m_PixelSize.x = 0;
	m_PixelSize.y = 0;
}

GaugePanel::~GaugePanel()
{
	// If there are gauges, delete them
	if( m_NumGauges != 0 )
	{
		std::list<Gauge*>::iterator it;
		for (it = m_GaugeList.begin(); it != m_GaugeList.end(); ++it)
		{
			delete *it;
		}
	}
}

void GaugePanel::InitFromXMLNode(XMLNode gaugeNode)
{
	Check(gaugeNode.IsValid() && gaugeNode.GetName() == "Panel");

	// Create gauges as described by the XML file
	XMLNode::NodeList nodeList = gaugeNode.GetChildList("Gauge");
	XMLNode::NodeList::iterator iter;
	for (iter = nodeList.begin(); iter != nodeList.end(); ++iter)
	{
		Gauge *pGauge = GaugeFactory::CreateGaugeInstance(*iter);

		if (pGauge != NULL) {
			pGauge->SetParentRenderObject(this);
			AddGauge(pGauge);
		}
	}

	double scale = globals->m_PrefManager->GetPrefD("DefaultPanelScale");
	double zoom = globals->m_PrefManager->GetPrefD("Zoom");
	double x, y; // temp variables

	// Set the units per pixel
	if (gaugeNode.HasChild("UnitsPerPixel"))
	{
		SetUnitsPerPixel(gaugeNode.GetChild("UnitsPerPixel").GetTextAsDouble());
	}
	else
	{
		SetUnitsPerPixel(globals->m_PrefManager->GetPrefD("UnitsPerPixel"));
	}

	// Set the scale
	if (gaugeNode.HasChild("Scale"))
	{
		gaugeNode.GetChild("Scale").GetTextAsCoord(x, y);
		SetScale(x * zoom * scale, y * zoom * scale);
	}
	else
	{
		SetScale(zoom * scale, zoom * scale);
	}

	// Set the position
	if (gaugeNode.HasChild("Position"))
	{
		gaugeNode.GetChild("Position").GetTextAsCoord(x, y);
		SetPosition(x * zoom, y * zoom);
	}
	else
	{
		SetPosition(0.0, 0.0);
	}

	// Set the size
	if (gaugeNode.HasChild("Size"))
	{
		gaugeNode.GetChild("Size").GetTextAsCoord(x, y);
		SetSize(x, y);
	}
	else
	{
		SetSize(0.0, 0.0);
	}

	// Set the gauge outline
	if (gaugeNode.HasChild("Outline"))
	{
		SetPanelOutline(gaugeNode.GetChild("Outline").GetTextAsBool());
	}
}

void GaugePanel::AddGauge(Gauge* pGauge)
{
	m_GaugeList.push_back(pGauge);
	m_NumGauges++;
}

void GaugePanel::Render()
{
	// Overload this function in derived classes to render
	// parts of the gauge panel not defined by gauge panels

	// BUT!!! you should always call the base class render function
	// as well in order to render the gauge panels

	this->ResetPanelCoordinateSystem();

	if(m_NumGauges > 0)
	{
		std::list<Gauge*>::iterator it;
		for (it = m_GaugeList.begin(); it != m_GaugeList.end(); ++it)
		{
			(*it)->Render();
		}
	}

	this->ResetPanelCoordinateSystem();

	if(m_DrawPanelOutline)
	{
		this->DrawPanelOutline();
	}
}

// Resets the gauge panel coordinate system before and after rendering gauges
void GaugePanel::ResetPanelCoordinateSystem()
{
	this->RecalcWindowPlacement();

	// The viewport is established in order to clip things
	// outside the bounds of the gauge panel
	glViewport(m_PixelPosition.x, m_PixelPosition.y, m_PixelSize.x, m_PixelSize.y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Define the projection so that we're drawing in "real" space
	glOrtho(0,m_PhysicalSize.x,0, m_PhysicalSize.y, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GaugePanel::RecalcWindowPlacement()
{
	// Figure out where we're drawing in the window
	m_PixelPosition.x = (int) (m_PhysicalPosition.x / m_UnitsPerPixel);
	m_PixelPosition.y = (int) (m_PhysicalPosition.y / m_UnitsPerPixel);

	m_PixelSize.x = (int) (m_PhysicalSize.x / m_UnitsPerPixel * m_Scale.x);
	m_PixelSize.y = (int) (m_PhysicalSize.y / m_UnitsPerPixel * m_Scale.y);
}

void GaugePanel::SetUnitsPerPixel(double unitsPerPixel)
{
	m_UnitsPerPixel = unitsPerPixel;

	if(m_NumGauges > 0)
	{
		std::list<Gauge*>::iterator it;
		for (it = m_GaugeList.begin(); it != m_GaugeList.end(); ++it)
		{
			(*it)->SetUnitsPerPixel(m_UnitsPerPixel);
		}
	}
}

void GaugePanel::SetScale(double xScale, double yScale)
{
	// Set gauge scaling factors, must be greater than 0
	if( (xScale > 0) && (yScale > 0 ) )
	{
		m_Scale.x = xScale;
		m_Scale.y = yScale;

		if(m_NumGauges > 0)
		{
			std::list<Gauge*>::iterator it;
			for (it = m_GaugeList.begin(); it != m_GaugeList.end(); ++it)
			{
				(*it)->SetScale(xScale, yScale);
			}
		}
	}
}

bool GaugePanel::ClickTest(int button, int state, int x, int y)
{
	if( (x >= (int)m_PixelPosition.x)&&(x <= (int)(m_PixelPosition.x + m_PixelSize.x))
			&&(y >= (int)m_PixelPosition.y)&&(y <= (int)(m_PixelPosition.y + m_PixelSize.y)) )
	{
		if(m_NumGauges > 0)
		{
			std::list<Gauge*>::iterator it;
			for (it = m_GaugeList.begin(); it != m_GaugeList.end(); ++it)
			{
				(*it)->HandleMouseButton(button, state, x, y);
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

void GaugePanel::DrawPanelOutline()
{
	const float vertices[] = {
		0.0, 0.0,
		0.0, m_PhysicalSize.y,
		m_PhysicalSize.x, m_PhysicalSize.y,
		m_PhysicalSize.x, 0.0
	};

	glLineWidth(2.0);
	glColor3ub(0, 190, 190);

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, &vertices);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glDisableClientState(GL_VERTEX_ARRAY);
}

} // end namespace OpenGC
