/*=========================================================================
    OpenGC - The Open Source Glass Cockpit Project

    Copyright (c) 2001-2004 Damion Shelton
    Copyright (c) 2005-2010 Hugo Vincent <hugo.vincent@gmail.com>
    Copyright (c) 2008-2010 Yuriy Kinakh <ykinakh@gmail.com>
    Copyright (c) 2008-2010 Bohdan Mushkevych <mushkevych@gmail.com>
    All rights reserved.

    This branch of OpenGC is developed within "Albatross UAV Project"
    http://gitorious.org/albatross/

    This software is distributed WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See Copyright.txt or http://opengc.sourceforge.net/licensing.html for more information.
=========================================================================*/

/**
 * Gauges are the most "primitive" building block in the
 * OpenGC design scheme. GaugePanels are composed of a number of
 * Gauges, each of which is capable of positioning and
 * drawing itself.
 *
 * By breaking a gauge into multiple Gauges, recycling
 * of code is encouraged between different gauge panel designs.
 */

#ifndef Gauge_h
#define Gauge_h

#include "Globals.h"
#include "FontManager.h"
#include "RenderObject.h"
#include "XMLNode.h"

namespace OpenGC
{

class Gauge : public RenderObject
{
	public:
		Gauge();
		Gauge(XMLNode gaugeNode);

		virtual ~Gauge() {};

		/** Render the gauge */
		virtual void Render();

		/** Return true if the click is inside the gauge */
		bool ClickTest(int button, int state, int x, int y);

		/** Set to true to have an opaque background */
		void SetOpaque(bool opaque) { m_Opaque = opaque; }

	protected:
		/** The font number provided to us by the font manager */
		int m_Font;

	private:
		/** The flag of background transparency */
		bool m_Opaque;

};

} // end namespace OpenGC

#endif

