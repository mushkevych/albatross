#!/bin/bash

# Runs the Groundstation:
#	1. Start LogD (logging to groundstation/logs/)
#	2. Starts CommD (on port specified in arg 1, or defaulting to /dev/ttyUSB0)
#	3. Runs the Main window of the Groundstation
#	(4. Runs the Map window of the Groundstation - unfinished)
#	(5. Runs the OpenGC-based Glass Cockpit - unfinished)
#
# Usage: run.sh [/dev/ttyS0]

echo "*** ALBATROSS GROUNDSTATION ***"
echo "(c) 2005, Hugo Vincent & John Stowers."

# Check we are in the right directory
if [ "`pwd | grep "groundstation$" `" == "" ] ; then
	echo "Error: needs to be run from in the groundstation directory." 
	exit 1
fi

# Check the Groundstation is not already running
LOCK_FILE=logs/.groundstation_running
if [ -e $LOCK_FILE ] ; then
	echo "Error: Groundstation already running."
	exit 1
else
	touch $LOCK_FILE
fi

# Programs we need:
LOGD_BIN=../onboard/logd/obj-i686/logd
COMMD_BIN=../onboard/commd/obj-i686/commd
OPENGC_BIN=./OpenGC/Build/OpenGC

# Test they are all present/compiled:
for i in $LOGD_BIN $COMMD_BIN $OPENGC_BIN; do
	if [ -e $i ] ; then
		echo -e "\t(Found program: $i)"
	else
		echo "Error: Program $i missing. Please compile it."
		exit 1
	fi;
done

#------------------------------------------------------------------------------
# Start LogD
#------------------------------------------------------------------------------
# (a) Make a directory for the logs based on the current date and time
MYTIME=`date +%Y%m%d-%H%M`
LOG_DIR=logs/$MYTIME

if [ -d $LOG_DIR ] ; then # Check its not already there
	echo -e "\tWarning: log subdirectory already present."
else
	mkdir $LOG_DIR
fi

echo -e "\t(Logging to $LOG_DIR)"

# (b) Run LogD
$LOGD_BIN $LOG_DIR/packet.log $LOG_DIR/message.log w &
 
#------------------------------------------------------------------------------
# Start CommD
#------------------------------------------------------------------------------
# (a) Work out what port to use
if [ -c "$1" ] ; then
	SERIAL_DEV=$1
	# (b) Run CommD on that port
	$COMMD_BIN $@ &
	echo -e "\t(Using serial port $SERIAL_DEV)"
else
	echo -e "\t(Not using serial port)"
fi


#------------------------------------------------------------------------------
# Start the OpenGC glass cockpit
#------------------------------------------------------------------------------
if [ -d ./OpenGC/Build/Data ]; then
    echo -e "\tData dir link exists"
else
    echo -e "\tData dir link creating"
    ln -s "`pwd`/OpenGC/Data" ./OpenGC/Build/Data
fi
cd ./OpenGC/Build/
./OpenGC > ../../$LOG_DIR/opengc.log &
ogc_pid=$!
cd ../../

#------------------------------------------------------------------------------
# Start Groundstation Main Application
#------------------------------------------------------------------------------
cd ./control_panel
PYTHONPATH=../../onboard/autopilot:../../util/protocol_python_bindings
export PYTHONPATH
python -O MainWindow.py ../$LOG_DIR > ../$LOG_DIR/groundstation.log
cntr_pnl_pid=$!
cd ..

#------------------------------------------------------------------------------
# Cleanup
#------------------------------------------------------------------------------
# Main App has been closed so kill background daemons
PYTHONPATH=../onboard/autopilot:../util/protocol_python_bindings
export PYTHONPATH
python -c "
import datagram; 
d = datagram.Datagram()
d.set_destination(0xFFFF); 
d.set_sender(datagram.GROUNDSTATION_D_BITMASK)
for cmd in [datagram.LWC_LOGD_EXIT, datagram.LWC_COMMD_EXIT]: 
	d.set_command(cmd)
	d.pack_and_send()
"

#Close OpenGC
kill $ogc_pid

# Release the Groundstation lock
rm $LOCK_FILE

echo "Closed cleanly."

