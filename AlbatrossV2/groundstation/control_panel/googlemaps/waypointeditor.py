# -*- coding: utf-8 -*-

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

# Module was inspired by PyTrainer project
# (c) Bohdan Mushkevych 2010
# (c) Mykhaylo Ivankiv 2010

import gtkmozembed
import re
import logging

from FileUtils import FileUtils


class WaypointEditor:
	def __init__(self, data_path, vbox, parent):	
		self.init_logger()
			
#		self.logger.debug("<--- INIT --->")
		self.data_path = data_path
		self.parent = parent

		self.moz = gtkmozembed.MozEmbed()
		self.moz.connect('title', self.handle_title_changed) 
		vbox.pack_start(self.moz, True, True)
		vbox.show()
		self.moz.show()
		vbox.show_all()

		self.htmlfile = self.data_path + "/googlemaps/waypointeditor.html"
#		self.logger.debug("<--- COMPLETE --->")

	def init_logger(self):
		# create loger and set it up
		self.logger = logging.getLogger("waypointeditor")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)

		
	def handle_title_changed(self, *args): 
		title = self.moz.get_title() 
#		self.logger.debug("Received title: "+ title)
		m = re.match("call:([a-zA-Z]*)[(](.*)[)]", title) 
		if m: 
			fname = m.group(1) 
			args = m.group(2) 
			if fname == "addWaypoint": 
				am = re.match("([+-]?[0-9]+[.][0-9]+),([+-]?[0-9]+[.][0-9]+)", args) 
				if am: 
					lat, lon = am.group(1), am.group(2) 
					lat, lon = float(lat), float(lon) 
					self.add_waypoint(lat, lon)
					self.logger.debug("new waypoint: lat %f, lon %f" % (lat, lon))
				else: 
					raise ValueError("Error parsing Add Waypoint parameters: %s" % args) 
			elif fname == "updateWaypoint": 
				am = re.match("([+-]?[0-9]+[.][0-9]+),([+-]?[0-9]+[.][0-9]+),([0-9]*)", args) 
				if am: 
					lat, lon, id_waypoint = am.group(1), am.group(2), am.group(3) 
					try:
						lat, lon, id_waypoint = float(lat), float(lon), int(id_waypoint)
						self.update_waypoint(id_waypoint, lat, lon)
						self.logger.debug("updated waypoint: id %d, lat %f, lon %f" % (id_waypoint, lat, lon))
					except ValueError as e:
						print "Error parsing Update Waypoint parameters: " % args
						print e
				else: 
					raise ValueError("Error parsing updateWaypoint parameters: %s" % args) 
			elif fname == "removeWaypoint": 
				am = re.match("([0-9]*)", args) 
				if am: 
					id_waypoint = am.group(1) 
					try:
						id_waypoint = int(id_waypoint)
						self.remove_waypoint(id_waypoint)
						self.logger.debug("removed waypoint: id %d" % id_waypoint)
					except ValueError as e:
						print "Error parsing Remove Waypoint parameters: " % args
						print e
				else: 
					raise ValueError("Error parsing removeWaypoint parameters: %s" % args) 
			else: 
				raise ValueError("Unexpected function name %s" % fname) 
		return False 
	
	def add_waypoint(self, lat, lon):
		self.parent.waypoint_add_candidate(lat, lon)
		
	def update_waypoint(self, _id, lat, lon):
		self.parent.waypoint_update_candidate(_id, lat, lon)
			
	def remove_waypoint(self, _id):
		self.parent.waypoint_remove(_id)

	def drawMap(self):
		self.createHtml()
		self.moz.load_url("file://" + self.htmlfile)
	
	def createHtml(self,default_waypoint=None):
#		self.logger.debug("<--- CREATE HTML --->")
	
		route = self.parent.get_route()
#		self.logger.debug("<--- Route Len = %d -->" % len(route))
		map_center_lat = 49
		map_center_lon = 25
		content = """

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Route Planner</title>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="waypointeditor.js"></script>
	<script type="text/javascript">
		/**
		 * @author Mykhaylo Ivankiv (neformal)  [neformal.lviv@gmail.com]
		 *
		 */

"""
		i = 0
		content += "var waypointList = [ \n"
		for wp in route:
			map_center_lat = wp.latitude
			map_center_lon = wp.longitude
			
			if i != 0: content += ",\n "
			content += "{lat:%f, lon:%f, _id:%d}" % (wp.latitude, wp.longitude, wp._id)
			i = i+1
		content += """]\n"""
		content += """var mapInitParams = {lat:%f, lon:%f, zoom:3}; \n""" % (map_center_lat, map_center_lon)
		content += """ 
	</script>

	<style type=text/css>
		html, body {height: 100%; width: 100%; margin: 0; padding: 0}
		#map { position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 1 }
		#set { position: absolute; z-index: 2; top: 20px; left: 90px;}
		#pointer	{display:none; position:absolute; z-index:99; height:50px; width:50px; background:url("../glade/set_marker.png") no-repeat scroll 2px 6px transparent;}
	</style>
</head>
<body>
	<div id="map"></div>
	<button id="set">New Waypoint</button>
	<div id="pointer" ></div>
</body>
</html>
"""
		file = FileUtils(self.htmlfile, content)
		file.run()
#		self.logger.debug("<--- COMPLETE --->")


