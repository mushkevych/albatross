/**
* @author Mykhaylo Ivankiv (neformal)  [neformal.lviv@gmail.com]
*
*/
	var eventCommander = function() {
		var events = {};

		this.bind = function (ev, fn) {
			var ev = ev.toLocaleLowerCase();
			ev = (ev.indexOf('on') != 0) ? 'on' + ev : ev;
			events[ev] ? events[ev].push(fn) : (events[ev] = []).push(fn)
			return this;
		}

		this.one = function (ev, fn) {
			var ev = ev.toLocaleLowerCase();
			ev = (ev.indexOf('on') != 0) ? 'on' + ev : ev;
			return this;
		}

		this.trigger = function (ev, arg) {
			var ev = ev.toLocaleLowerCase();
			ev = (ev.indexOf('on') != 0) ? 'on' + ev : ev;
			for (var i = 0; events[ev] && i < events[ev].length; i++) {events[ev][i](this, arg)}
			return this;
		}

		this.unbind = function (ev, fn) {
			var ev = ev.toLocaleLowerCase();
			ev = (ev.indexOf('on') != 0) ? 'on' + ev : ev;
			if (events[ev])
				for (var i = 0; i < events[ev].length; i++)
					if (events[ev][i].toString() === fn.toString()) {events[ev].splice(i, 1); break;}
			return this;
		}
	}

	var Map = function (initObj) {
		var _this = this;
		var gMap = google.maps;

		this.map = {}
		this.markers = [];
		this.polylineArray = [];
		this.polyline = {};

		this.markersIcon ='../glade/markers.png'
		this.activeMarkersIcon ='../glade/active_markers.png';

		this.addMarker = function(initObj) {
			var marker = new gMap.Marker ({ map :_this.map, position: new google.maps.LatLng(initObj.lat, initObj.lon), draggable:true /*, icon:_this.markerIcon*/});

			marker.changeIcon = function (type){
				if (type == 'active')
					this.setIcon (new gMap.MarkerImage(_this.activeMarkersIcon,new gMap.Size(45,45),new gMap.Point((this.id-1) % 10 *49, Math.floor((this.id-1)/10) *47),new gMap.Point(37,37)));
				else
					this.setIcon (new gMap.MarkerImage(_this.markersIcon,new gMap.Size(45,45),new gMap.Point((this.id-1) % 10 *49, Math.floor((this.id-1)/10) *47),new gMap.Point(37,37)));
			}
			marker.bind = function(ev, func){
				google.maps.event.addListener(this,ev,func);
				return this;
			}

			marker
				.bind('click',function(e){_this.setActiveMarker(this)})
				.bind('dragstart',function(e){_this.setActiveMarker(this)})
				.bind('dragend',function(e){
					_this.polylineArray[this.id-1] = this.position;
					_this.writeLine()
					_this.trigger('updateMarker',this);
				})

			_this.markers.push(marker);
			_this.polylineArray.push(marker.position);
			_this.writeLine()

			marker.id =  _this.markers.length
			marker.changeIcon('default')

			return marker;
		};

		this.writeLine = function(){_this.polyline.setPath (_this.polylineArray);}

		this.removeActiveMarker = function(){
			var marker = _this.activeMarker, i = marker.id - 1;

			_this.setActiveMarker(false);
			_this.markers[i].setVisible(false);

			_this.trigger('removeMarker',_this.markers[i]);

			_this.markers.splice(i,1);
			// Update markers id;
			for (var j = i; j < _this.markers.length; j++){
				_this.markers[j].id = j+1;
				_this.markers[j].changeIcon ('default');
			}

			_this.polylineArray.splice(i,1);
			_this.writeLine();

			return
		};

		this.setActiveMarker = function (marker){
			if (_this.activeMarker) _this.activeMarker.changeIcon('dafault')
			_this.activeMarker = marker;
			if (_this.activeMarker)  _this.activeMarker.changeIcon('active')
		};

		this.init = function(initObj) {
			eventCommander.apply(_this);
			for (var tmp in initObj) _this[tmp] = initObj[tmp];
			_this.map = new gMap.Map(document.getElementById("map"), { zoom: _this.zoom, center: new gMap.LatLng(_this.lat, _this.lon), mapTypeId: google.maps.MapTypeId.ROADMAP});
			_this.polyline = new gMap.Polyline ({map:_this.map, strokeColor:'#5867a7',strokeWeight:5})

			google.maps.event.addListener(_this.map,'click',function(e){_this.trigger('click',e)});
		};

		this.init(initObj);
		return this
	}

	window.addEventListener('load', function() {
		function setMarker (_this, data){
			stopMarkerSet ();
			var marker = _this.addMarker({lat:data.latLng.b, lon:data.latLng.c})
			map.trigger('addMarker',marker);
			_this.setActiveMarker(marker);
		}

		function moveMarker(e){
			document.getElementById('pointer').style.top = (e.clientY-50) +"px";
			document.getElementById('pointer').style.left =  (e.clientX-50) +"px";
			document.getElementById('pointer').style.display='block';
		}

		function stopMarkerSet (){
			document.getElementById('pointer').style.display='none';
			map.unbind('click',setMarker);
			document.body.removeEventListener('mousemove',moveMarker,false)
		}

		function startMarkerSet (){
			map.bind('click',setMarker);
			document.body.addEventListener("mousemove",moveMarker ,false);
		}

		document.getElementById('set').addEventListener('click',startMarkerSet,false)
		document.addEventListener('keydown',function(e){
			if (e.keyCode == 46) {map.removeActiveMarker();}	//Delete
			if (e.keyCode == 27) {stopMarkerSet();} 			//Esc
		},false)

		var map = (new Map(mapInitParams))
			.bind ('click', function(_this,data){ _this.setActiveMarker(false);})
			.bind ('removeMarker', function (_this,data){document.title = "call:removeWaypoint(" + (data.id-1) + ")";})
			.bind ('updateMarker', function (_this,data){document.title = "call:updateWaypoint(" + data.position.b + "," + data.position.c + "," + (data.id-1) + ")";})
			.bind ('addMarker', function (_this,data){ document.title = "call:addWaypoint(" + data.position.b + "," + data.position.c + ")";})

		waypointList.some (function(el){map.addMarker(el)})

	}, false)