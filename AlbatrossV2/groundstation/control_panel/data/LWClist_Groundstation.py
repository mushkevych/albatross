# -*- coding: utf-8 -*-
lightWeightCommands_Groundstation = [
	["LWC_STATED_EXIT", 	"kill StateD."],
	["LWC_HWD_EXIT", 		"kill HWD."],
	["LWC_SUPERVISORD_EXIT","kill SupervisorD."],
	["LWC_LOGD_EXIT", 		"kill LogD."],
	["LWC_COMMD_EXIT", 		"kill CommD."],
	["LWC_TESTD_EXIT", 		"kill TestD."],
	["LWC_RADIO_SLEEP", 	"put radio to sleep."],
	["LWC_RADIO_UNSLEEP", 	"wake the radio from sleep."],
	["LWC_AUTOPILOT_EXIT",	"kill Autopilot"]
]
