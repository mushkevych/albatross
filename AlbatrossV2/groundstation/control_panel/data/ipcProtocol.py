# -*- coding: utf-8 -*-
# This file prescribes how to extract data from incoming telemetry/IPC
# data, how to format it, and how to display it.
#
# Hugo Vincent, 1 August 2005.
# Bohdan Mushkevych, September 2010

# These functions describe how to get data out of the parsegram:
def dgFloat (pg, key):
	return pg.get_float(key)

def dgUint8 (pg, key):
	return pg.get_byte(key)
	

# These functions describe how to string-ify data for display. They could be
# used to have different display formats per variable, or to add units (e.g. "1.23 m/s")
strFlt = lambda value: "%.4f" % value # Returns stringified float in 1.2345 format

def strGpsMode(value): 
	if value == 0: return "No GPS"
	elif value == 2: return "2D Fix"
	elif value == 3: return "3D Fix"
	else: return "Mode %d" % value

#--------------------------------------------------------------------------
#						DATAGRAM FORMAT TABLE
#--------------------------------------------------------------------------
# Format: "key: [extractor, string-ifier, C name (in protocol.h), GUI category, GUI description]"
#
# Key serves both to determine the order the fields are listed in the GUI, 
# and as a handle when indexing the table.
# First column is a function to of prototype func(addr) to extract the value from the 
# datagram buffer at address addr.
# Second column is a function to display the value as a string.
# Third column is the protocol.h value (e.g. datagram.STATE_D_U_DOT), i.e. the address
# into the buffer of the start of the value.
# Forth and Fifth columns are strings, used in the GUI for Category and Name respectively.
datagramProtocol = {
#--------------------------------------------------------------------------
# Inertial Measurement Unit
	# Velocities (Integrated)
	datagram.STATE_D_SPEED_LOCAL_X:  [dgFloat, strFlt, datagram.STATE_D_SPEED_LOCAL_X, 'IMU (Local frame)', 'Velocity X (forward)'],
	datagram.STATE_D_SPEED_LOCAL_Y:  [dgFloat, strFlt, datagram.STATE_D_SPEED_LOCAL_Y, 'IMU (Local frame)', 'Velocity Y (right)'],
	datagram.STATE_D_SPEED_LOCAL_Z:  [dgFloat, strFlt, datagram.STATE_D_SPEED_LOCAL_Z, 'IMU (Local frame)', 'Velocity Z (down)'],
	# Accelerometers
	datagram.STATE_D_ACCELERATION_X:  [dgFloat, strFlt, datagram.STATE_D_ACCELERATION_X, 'IMU (Local frame)', 'Accel. X (forward)'],
	datagram.STATE_D_ACCELERATION_Y:  [dgFloat, strFlt, datagram.STATE_D_ACCELERATION_Y, 'IMU (Local frame)', 'Accel. Y (right)'],
	datagram.STATE_D_ACCELERATION_Z:  [dgFloat, strFlt, datagram.STATE_D_ACCELERATION_Z, 'IMU (Local frame)', 'Accel. Z (down)'],
	# Gyros
	datagram.STATE_D_ROLL_ANGLE:  [dgFloat, strFlt, datagram.STATE_D_ROLL_ANGLE, 'IMU (Local frame)', 'Roll Angle'],
	datagram.STATE_D_PITCH_ANGLE: [dgFloat, strFlt, datagram.STATE_D_PITCH_ANGLE, 'IMU (Local frame)', 'Pitch Angle'],
	datagram.STATE_D_YAW_ANGLE:   [dgFloat, strFlt, datagram.STATE_D_YAW_ANGLE, 'IMU (Local frame)', 'Yaw Angle'],
#--------------------------------------------------------------------------
# Inertial Navigation System
	# Velocities (Integrated)
	datagram.STATE_D_SPEED_GLOBAL_X:  [dgFloat, strFlt, datagram.STATE_D_SPEED_GLOBAL_X, 'INS (World frame)', 'Velocity X (forward)'],
	datagram.STATE_D_SPEED_GLOBAL_Y:  [dgFloat, strFlt, datagram.STATE_D_SPEED_GLOBAL_Y, 'INS (World frame)', 'Velocity Y (right)'],
	datagram.STATE_D_SPEED_GLOBAL_Z:  [dgFloat, strFlt, datagram.STATE_D_SPEED_GLOBAL_Z, 'INS (World frame)', 'Velocity Z (down)'],
	# Heading and Compass 
	datagram.STATE_D_HEADING_MAGNETIC: [dgFloat, strFlt, datagram.STATE_D_HEADING_MAGNETIC, 'INS (World frame)', 'Heading Magnetic'],
	datagram.STATE_D_HEADING:          [dgFloat, strFlt, datagram.STATE_D_HEADING, 'INS (World frame)', 'Heading'],
	datagram.STATE_D_DCM00:          [dgFloat, strFlt, datagram.STATE_D_DCM00, 'INS (World frame)', 'DCM00'],
	datagram.STATE_D_DCM01:          [dgFloat, strFlt, datagram.STATE_D_DCM01, 'INS (World frame)', 'DCM01'],
	datagram.STATE_D_DCM02:          [dgFloat, strFlt, datagram.STATE_D_DCM02, 'INS (World frame)', 'DCM02'],
	datagram.STATE_D_DCM10:          [dgFloat, strFlt, datagram.STATE_D_DCM10, 'INS (World frame)', 'DCM10'],
	datagram.STATE_D_DCM11:          [dgFloat, strFlt, datagram.STATE_D_DCM11, 'INS (World frame)', 'DCM11'],
	datagram.STATE_D_DCM12:          [dgFloat, strFlt, datagram.STATE_D_DCM12, 'INS (World frame)', 'DCM12'],
	datagram.STATE_D_DCM20:          [dgFloat, strFlt, datagram.STATE_D_DCM20, 'INS (World frame)', 'DCM20'],
	datagram.STATE_D_DCM21:          [dgFloat, strFlt, datagram.STATE_D_DCM21, 'INS (World frame)', 'DCM21'],
	datagram.STATE_D_DCM22:          [dgFloat, strFlt, datagram.STATE_D_DCM22, 'INS (World frame)', 'DCM22'],        

#--------------------------------------------------------------------------
# GPS outputs
	datagram.STATE_D_LATITUDE:       [dgFloat, strFlt, datagram.STATE_D_LATITUDE, 'GPS', 'Latitude'],
	datagram.STATE_D_LONGITUDE:      [dgFloat, strFlt, datagram.STATE_D_LONGITUDE, 'GPS', 'Longitude'],
	datagram.STATE_D_ALTITUDE:       [dgFloat, strFlt, datagram.STATE_D_ALTITUDE, 'GPS', 'Altitude'],
	datagram.STATE_D_SPEED_GROUND:   [dgFloat, strFlt, datagram.STATE_D_SPEED_GROUND, 'GPS', 'Ground speed'],
#--------------------------------------------------------------------------
# Hardware Information
	datagram.STATE_D_EXTERNAL_TEMPERATURE:    [dgFloat, strFlt, datagram.STATE_D_EXTERNAL_TEMPERATURE, 'Hardware', 'External Temperature'],
	datagram.STATE_D_INTERNAL_TEMPERATURE:    [dgFloat, strFlt, datagram.STATE_D_INTERNAL_TEMPERATURE, 'Hardware', 'Internal Temperature'],
	datagram.STATE_D_GPS_MODE:                [dgUint8, strGpsMode, datagram.STATE_D_GPS_MODE, 'Hardware', 'GPS Mode'],
	datagram.STATE_D_SATS_IN_VIEW:            [dgUint8, str, datagram.STATE_D_SATS_IN_VIEW, 'Hardware', 'Satellites in View'],
	datagram.STATE_D_BAROMETRIC_PRESSURE:     [dgFloat, strFlt, datagram.STATE_D_BAROMETRIC_PRESSURE, 'Hardware', 'Barometric Pressure'],
	datagram.STATE_D_VOLTAGE_BATTERY:         [dgFloat, strFlt, datagram.STATE_D_VOLTAGE_BATTERY, 'Hardware', 'Voltage Battery']
}

#--------------------------------------------------------------------------
#					SENDER ADDRESS TO SENDER NAME MAPPING
#--------------------------------------------------------------------------
senderNames = {
	datagram.GROUNDSTATION_D_BITMASK:		'Groundstation',
	datagram.STATE_D_BITMASK:				'StateD',
	datagram.HW_D_BITMASK:					'HardwareD',
	datagram.SUPERVISOR_D_BITMASK:			'SupervisorD',
	datagram.COMM_D_BITMASK:				'CommD',
	datagram.TEST_D_BITMASK:				'Test Application',
	datagram.LOGGING_D_BITMASK:				'LogD',
	datagram.GROUNDSTATION_NOTE_D_BITMASK:	'User Note',
	datagram.AUTOPILOT_D_BITMASK:			'Autopilot'
}
