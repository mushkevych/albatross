#!/usr/bin/python -O
# -*- coding: utf-8 -*-

# MainWindow.py
#
# Hugo Vincent, 29 May 2005
# Bohdan Mushkevych 2009, 2010

# PyGTK imports
import time 
import gtk.gdk
import gtk.glade
import pygtk
from MapWindow import MapWindow
pygtk.require('2.0')

import datagram


# Some glue functions
def now(): # Returns current time
	return time.strftime("%H:%M:%S")

def log(text, sender=datagram.GROUNDSTATION_D_BITMASK):
	datagram.send_log_message(text, sender)

def date_suffix():
	"""Returns a suffix to be added to a filename to time-stamp it."""
	return time.strftime("%m%d-%H%M%S") # Month Day - Hours Minutes Seconds

def console_msg(text):
	print "[%s]" % now(), text

# Application imports
from DataManager import DataManager
try:
	from PlottingWindow import PlottingWindow
	allow_plotting = True
except ImportError:
	console_msg("Plotting is disabled because PlottingWindow of matplotlib could not be imported.")
	allow_plotting = False

#-----------------------------------------------------------------------------
# MAIN APPLICATION WINDOW CLASS
class MainWindow:
	version = 2.0
	databaseFile = "data/database.sqlite"
	lwc_list_file = "data/LWClist_Groundstation.py"
	ipcProtocolFile = "data/ipcProtocol.py"

	# Constructor
	def __init__(self):
		# Make the GUI from the Glade file and autoconnect the signals
		wtree = gtk.glade.XML("glade/MainWindow.glade")
		wtree.signal_autoconnect(self)
		
		# Set all the widgets as attributes of self (so e.g. self.statusbar works)
		for w in wtree.get_widget_prefix(''):
			name = w.get_name()
			# make sure we don't clobber existing attributes
			if not hasattr(self, name):
				setattr(self, name, w)

		console_msg("MainWindow object constructed.")
	
	# Main function
	def run(self):
		console_msg("Start of MainWindow.run() method.")

		# Initialize the Groundstation data handling backend
		self.dataManager = DataManager(self.databaseFile, self.ipcProtocolFile)

		# Initialize backends to the GUI
		self.flightVariables = FlightVariables(self.flightVarsTree, self.dataManager)
		self.inspector = Inspector(self.inspectorTree, self.dataManager)
		self.watchList = WatchList(self.watchListTree, self.dataManager)
		self.logViewer = LogViewer(self.logViewerTree)

		# Set up the GUI initial values
		self.init_lwcList(self.lwc_list_file)

		# Start the data handling running
		self.dataManager.registerGuiUpdateListener(self.flightVariables.new_data_arrived)
		self.dataManager.registerGuiUpdateListener(self.watchList.new_data_arrived)
		self.dataManager.registerGuiUpdateListener(self.inspector.new_data_arrived)
		self.dataManager.setGuiUpdateInterval(10)
		self.dataManager.receiver.registerLogHandler(self.logViewer.addEntry)
		self.dataManager.start()
		time.sleep(0.10) # Give the background threads time to get started

		log("log: Groundstation Started.")
		console_msg("console_msg: Groundstation Started.")
		# Main GTK event loop
		gtk.gdk.threads_init()
		with gtk.gdk.lock:
			gtk.main()

	# Sets up the Light Weight Command combo box and associated datastore
	def init_lwcList(self, dataFile):
		# Get the data by invoking the Python interpreter
		env = {}
		execfile(dataFile, env)
		lwcmds = env['lightWeightCommands_Groundstation']

		# FIXME this assumes lwcComboBox's list is empty to start with
		self.lwcList = []
		for i in lwcmds:
			if hasattr(datagram, i[0]):
				self.lwcComboBox.append_text(i[0] + " - " + i[1])
				self.lwcList.append(getattr(datagram, i[0]))
			else:
				console_msg("Warning: %s is not a valid LWC.\n\tPlease regen data/LWClist.dat and the bindings." % i)

		self.lwcComboBox.set_active(0)
		console_msg("Read LWC list from" + dataFile)

	#-------------------------------------------------------------------------
	#	*** UI Event Handlers ***
	def app_quit_event(self, obj, data=None):
		self.dataManager.stop()
		gtk.main_quit()

	def helpAbout(self, obj, data=None):
		text = "Albatross Groundstation\nVersion " + str(self.version )+ "\n\n(c) 2005 Hugo Vincent & John Stowers.\n(c) 2009 Bohdan Mushkevych.\nLicensed under GPLv2."
		dialog = gtk.MessageDialog(self.mainWindow, buttons=gtk.BUTTONS_CLOSE, message_format=text)
		dialog.run()
		dialog.destroy()

	def on_flightVarsTree_button_press_event(self, widget, event):
		# popup the right-mouse menu
		if event.button == 3:
			self.menuFlightVars.popup(None, self.flightVariables.tree, None, event.button, event.time)

	def menu_add_to_watcher(self, obj, data=None):
		# get selected row from flightVarsTree
		treeselection = self.flightVariables.tree.get_selection()
		model, iter = treeselection.get_selected()
		key = model.get_value(iter, 0)
		# ask WatchList class to watch given item
		self.watchList.add_watch_item(key)

	def menu_fv_add_to_inspector(self, obj, data=None):
		# get selected row from flightVarsTree
		treeselection = self.flightVariables.tree.get_selection()
		model, iter = treeselection.get_selected()
		key = model.get_value(iter, 0)
		# ask Inspector class to watch given item
		self.inspector.inspect_item(key)
			
	def menu_wl_add_to_inspector(self, obj, data=None):
		# get selected row from watch list
		treeselection = self.watchList.tree.get_selection()
		model, iter = treeselection.get_selected()
		key = model.get_value(iter, 0)
		# ask Inspector class to watch given item
		self.inspector.inspect_item(key)

	def on_watchListTree_button_press_event(self, widget, event):
		# popup the right-mouse menu
		if event.button == 3:
			self.menuWatchList.popup(None, self.watchList.tree, None, event.button, event.time)

	def menu_remove_from_watcher(self, obj, data=None):
		# get selected row from watch list
		treeselection = self.watchList.tree.get_selection()
		model, iter = treeselection.get_selected()
		key = model.get_value(iter, 0)
		# ask WatchList class to watch given item
		self.watchList.remove_watch_item(key)

	#-------------------------------------------------------------------------
	# Plotting/History Events
	def event_button_plot(self, obj, data=None):
		global allow_plotting
		if not allow_plotting:
			self.message("Unable to plot. Check console for details.",
				status="Unable to plot.")		
			return

		self.status("Plotting...")

		if self.inspector.inspectedKey == -1:
			# nothing to show - just generic 0
			plotter = PlottingWindow('Tracker Item Is Not Defined')
			plotter.show([0,0,0])
		else:
			keyName = self.dataManager.receiver.datagramFormat[int(self.inspector.inspectedKey)][4]
			getter = self.dataManager.receiver.datagramFormat[int(self.inspector.inspectedKey)][0]
			plotter = PlottingWindow(keyName)
			serie = []

			history = self.dataManager.getParsegramHistory()
			for pg in history:
				value = getter(pg, int(self.inspector.inspectedKey))
				serie.append(value)

			plotter.show(serie)

	#-------------------------------------------------------------------------
	# Visualize the Route window
	def event_button_route(self, obj, data=None):
		map_window = MapWindow()
		map_window.run()

	#-------------------------------------------------------------------------
	# Logging Event Handlers
	def event_button_log_note(self, obj, data=None):
		log(self.logNoteEntry.get_text(), sender=datagram.GROUNDSTATION_NOTE_D_BITMASK)

	#-------------------------------------------------------------------------
	# Control Handlers

	# Light weight commands
	def lwcSend(self, obj, data=None, cmd=None):
		if cmd == None:
			cmd = self.lwcList[self.lwcComboBox.get_active()]
			console_msg("Sending %s (0x%x)" % (self.lwcComboBox.get_active_text(), cmd))
		else:
			console_msg("Sending internal LWC (0x%x)" % cmd)

		d = datagram.Datagram()
		d.set_destination(0xFFFF)
		d.set_sender(datagram.GROUNDSTATION_D_BITMASK)
		d.set_command(cmd)
		d.pack_and_send()

	# Mode/Override controls
	def controlEmergencyOverride(self, obj, data=None):
		if(self.question("Are you sure?")):
			self.lwcSend(None, cmd=datagram.LWC_EMERGENCY_OVERRIDE)
			console_msg("Sent Emergency Override")
	
	def controlResetSnapper(self, obj, data=None):
		if(self.question("Are you sure?")):
			self.lwcSend(None, cmd=datagram.LWC_RESET_SNAPPER)
			console_msg("Sent Snapper RESET")

	def controlAutoMode(self, obj, data=None):
		self.lwcSend(None, cmd=datagram.LWC_EXIT_MANUAL_MODE)
		console_msg("Sent Enter Auto Mode")

	def controlManualMode(self, obj, data=None):
		self.lwcSend(None, cmd=datagram.LWC_ENTER_MANUAL_MODE)
		console_msg("Sent Enter Manual Mode")

	def autoManualModeUpdate(self, obj, data=None):
		"""Called by Auto/Manual radio/toggle buttons in main toolbar."""
		if self.radioManualMode.get_active():
				self.controlManualMode(obj, data)
		elif self.radioAutoMode.get_active():
				self.controlAutoMode(obj, data)
				
	# Simulator Reset
	def simReset(self, obj, data=None):
		self.lwcSend(None, cmd=datagram.LWC_RESET_SIM)
		console_msg("Send reset simulator and control integrators.")

	def startAllDaemons(self, obj, data=None):
		self.lwcSend(None, cmd=datagram.LWC_START_ALL_DAEMONS)
		console_msg("Sent \"SupervisarD start all daemons\".")

	def stopAllDaemons(self, obj, data=None):
		if(self.question("Are you sure?")):
			self.lwcSend(None, cmd=datagram.LWC_STOP_ALL_DAEMONS)
			console_msg("Sent \"SupervisorD stop all daemons\".")

	def restartAllDaemons(self, obj, data=None):
		if(self.question("Are you sure?")):
			self.lwcSend(None, cmd=datagram.LWC_RESTART_ALL_DAEMONS)
			console_msg("Sent \"SupervisorD restart all daemons\".")

	#-------------------------------------------------------------------------
	# Utility Functions
	def status(self, text):
		self.statusBar.pop(0)
		self.statusBar.push(0, text)

	def message(self, text, status=None):
		"""Shows a message dialog containing text, with a close button and a 
		warning icon. Optional argument status can update the status bar at
		the same time. Blocks until the user responds."""
		if status != None: self.status(status)
		dialog = gtk.MessageDialog(self.mainWindow, type=gtk.MESSAGE_WARNING, buttons=gtk.BUTTONS_CLOSE, message_format=text)
		dialog.run()
		dialog.destroy()

	def question(self, text, status=None):
		"""Shows a message dialog containing text, which should be a yes or no
		question."""
		if status != None: self.status(status)
		dialog = gtk.MessageDialog(self.mainWindow, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO, message_format=text)
		response = dialog.run()
		dialog.destroy()
		if response == gtk.RESPONSE_YES: return True
		else: return False

#-----------------------------------------------------------------------------
# FLIGHT VARIABLES TREE CLASS
class FlightVariables:
	def __init__(self, treeView, dataManager):
		self.tree = treeView
		self.dataManager = dataManager
		# rows hold dictionary for fast access to model rows
		self.rows = {}
		# addresses holds list of addresses presented in the tree
		self.addresses = []

		self.init_model()
		self.init_columns()
		self.init_data()

	# Sets up the model
	def init_model(self):
		self.model = gtk.TreeStore(str, str, str) # Column Types are: Key (hidden), Category/Name, Value
		self.tree.set_model(self.model)
		self.tree.set_search_column(1) # Name column			

	# Sets up the columns
	def init_columns(self):
		# First Column (Variables)
		col = gtk.TreeViewColumn("Variable", gtk.CellRendererText(), text=1)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(170)
		col.set_resizable(True)
		col.set_sort_column_id(1) # Make it sortable by clicking on the header
		self.tree.append_column(col)
		
		# Second Column (Value)
		col = gtk.TreeViewColumn("Value", gtk.CellRendererText(), text=2)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)		# FIXME why is this necessary...
		self.tree.append_column(col)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE) # ...when this is here?

	# Initializes the data linkage
	def init_data(self):
		categories = []
		parents = []
		parent = None
		
		map = self.dataManager.receiver.datagramFormat
		for key in map:
			currentRow = map[key]
			category = currentRow[3]
			if category not in categories:
				parent = self.model.append(None, ['', category, ''])
				categories.append(category)
				parents.append(parent)
			else:
				i = categories.index(category)
				parent = parents[i] 
			
			address = currentRow[2]
			row = self.model.append(parent, [address, currentRow[4], ''])
			self.rows[int(address)] = row
			self.addresses.append(address)

	# Called by the DataManager when new data is available
	# Data has already been stringified by this point
	def new_data_arrived(self, parsegram):
		parsegramKeys = parsegram.get_keys()
		for key in parsegramKeys:
			shortKey = ord(key)
			
			# skip items from parsegram, that are not meant to be presented in the tree
			if shortKey not in self.addresses:
				continue
				
			# function that retrieves proper value (i.e. get_byte vs get_float etc) from parsegram
			getter = self.dataManager.receiver.datagramFormat[shortKey][0]
			formatter = self.dataManager.receiver.datagramFormat[shortKey][1]
			value = getter(parsegram, shortKey)
			value = formatter(value)
			
			self.model.set_value(self.rows[shortKey], 2, value)
			self.tree.queue_draw() 
			

#-----------------------------------------------------------------------------
# INSPECTOR CLASS
class Inspector:
	def __init__(self, treeView, dataManager):
		self.tree = treeView
		self.dataManager = dataManager
		self.init_model()
		self.init_columns()
		self.init_data()
		self.inspectedKey = -1 # key to track

	# Sets up the model
	def init_model(self):
		self.model = gtk.ListStore(str, str, str)
		self.tree.set_model(self.model)
		self.tree.set_search_column(2)
	
	# Sets up the columns
	def init_columns(self):
		# First Column (Time)
		col = gtk.TreeViewColumn("Time", gtk.CellRendererText(), text=0)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(70)
		col.set_resizable(True)
		col.set_sort_column_id(0) # Make it sortable by clicking on the header
		self.tree.append_column(col)
		
		# Second Column (Variable)
		col = gtk.TreeViewColumn("Variable", gtk.CellRendererText(), text=1)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(155)
		col.set_resizable(True)
		col.set_sort_column_id(1)
		self.tree.append_column(col)
		
		# Third Column (Value)
		col = gtk.TreeViewColumn("Value", gtk.CellRendererText(), text=2)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) # FIXME make this autosized
		self.tree.append_column(col)

	# Initializes the data linkage
	def init_data(self):
		self.model.prepend([now(), "Waiting for transmission", "..."])
	
	def inspect_item(self, key):
		if self.inspectedKey == key:
			# asked item is already being inspected
			return
	
		self.inspectedKey = key
		#clears the model
		self.model.clear()
		
		#iterate thru ParsegramHistory and fill in the inspector model
		history = self.dataManager.getParsegramHistory()
		for pg in history:
			self.new_data_arrived(pg)

	# Called by the DataManager when new data is available
	# Data has already been stringified by this point
	def new_data_arrived(self, parsegram):
		if self.inspectedKey == -1:
			return
	
		# function that retrieves proper value (i.e. get_byte vs get_float etc) from parsegram
		getter = self.dataManager.receiver.datagramFormat[int(self.inspectedKey)][0]
		formatter = self.dataManager.receiver.datagramFormat[int(self.inspectedKey)][1]
		value = getter(parsegram, int(self.inspectedKey))
		
		if value != None:
			timestamp = parsegram.get_datetime().strftime("%H:%M:%S")
			value = formatter(value)
			self.model.prepend([timestamp, self.inspectedKey, value])
			self.tree.queue_draw()
		

#-----------------------------------------------------------------------------
# WATCH LIST CLASS
class WatchList:
	def __init__(self, treeView, dataManager):
		self.tree = treeView
		self.dataManager = dataManager
		self.inspectedKeys = []
		
		self.init_model()
		self.init_columns()
		self.init_data()

	# Sets up the model
	def init_model(self):
		self.model = gtk.ListStore(str, str, str)
		self.tree.set_model(self.model)
		self.tree.set_search_column(0)
	
	# Sets up the columns
	def init_columns(self):
		# First Column (Variable)
		col = gtk.TreeViewColumn("Variable", gtk.CellRendererText(), text=0)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(155)
		col.set_resizable(True)
		col.set_sort_column_id(0) # Make it sortable by clicking on the header
		self.tree.append_column(col)
		
		# Second Column (Value)
		col = gtk.TreeViewColumn("Value", gtk.CellRendererText(), text=1, foreground=2)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) # FIXME make this autosized
		col.set_expand(True)
		self.tree.append_column(col)
		
	# Initializes the data linkage
	def init_data(self):
		self.inspectedKeys.append(-1)
		self.model.prepend(["-1", "Waiting for transmission", "green"])
	
	# Add new item to watch
	def add_watch_item(self, key):
		if key in self.inspectedKeys:
				# given key is already being watched
				return
		
		self.inspectedKeys.append(key)
		self.model.prepend([key, key, "green"])
		parsegram = self.dataManager.latestData
		self.new_data_arrived(parsegram)

	# Add new item to watch
	def remove_watch_item(self, key):
		iter = self.model.get_iter_first()
		while iter != None:
			currentKey = self.model.get_value(iter, 0)
			if currentKey == key:
				self.model.remove(iter)
				self.inspectedKeys.remove(key)
				break
			else:
				iter = self.model.iter_next(iter)
		

	# Called by the DataManager when new data is available
	def new_data_arrived(self, parsegram):
		iter = self.model.get_iter_first()
		while iter != None:
			key = self.model.get_value(iter, 0)
			if key == "-1":
				return
			
			getter = self.dataManager.receiver.datagramFormat[int(key)][0]
			formatter = self.dataManager.receiver.datagramFormat[int(key)][1]
			value = getter(parsegram, int(key))
			
			if value != None:
				value = formatter(value)
				self.model.set_value(iter, 1, value)
			iter = self.model.iter_next(iter)

#-----------------------------------------------------------------------------
# LOG VIEWER CLASS
class LogViewer:
	def __init__(self, treeView):
		self.tree = treeView
		self.init_model()
		self.init_columns()

	# Sets up the model
	def init_model(self):
		self.model = gtk.ListStore(str, str)
		self.tree.set_model(self.model)
		self.tree.set_search_column(1)
	
	# Sets up the columns
	def init_columns(self):
		# First Column (Time)
		col = gtk.TreeViewColumn("Time", gtk.CellRendererText(), text=0)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(70)
		col.set_resizable(True)
		col.set_sort_column_id(0) # Make it sortable by clicking on the header
		self.tree.append_column(col)
		
		# Second Column (Message)
		col = gtk.TreeViewColumn("Message", gtk.CellRendererText(), text=1)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) # FIXME make this autosized
		self.tree.append_column(col)

	# Adds an entry to the log display 
	def addEntry(self, sender, text):
		self.model.prepend([now(), "[" + sender + "] " + text])


#-----------------------------------------------------------------------------
# Main Function
if __name__ == '__main__':
	mw = MainWindow()
	mw.run()
