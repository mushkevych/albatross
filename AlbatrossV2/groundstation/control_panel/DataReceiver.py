#!/usr/bin/env python

""" Responsible for receiving data from the aircraft.

Abstracts the reception so that the data can come from either
the serial port or the local IPC datagram traffic on the laptop.
Also decodes/extracts the information.

Only does data, so log messages and commands are done separately.

Hugo Vincent, 23 June 2005.
"""

import datagram
import parsegram
import threading
import time

class DataReceiver:
	def __init__(self, ipcProtocolFile):
		self.datagram = datagram.Datagram()
		self.parsegram = parsegram.Parsegram()
		self.datagram.set_start_addr(0) # Make the buffer large enough to hold any possible input
		self.datagram.set_end_addr(0xFFFF)
		self.threadRunning = False
		self.callbacks = []
		
		env =  {'datagram': datagram,	 # access to the datagram module.
						'parsegram': parsegram}  # access to the parsegram module.
		execfile(ipcProtocolFile, env)
		self.datagramFormat = env['datagramProtocol'] # get the resultant dictionary
		self.senderNames = env['senderNames'] # and the dictionary of sender IDs
	
	def start(self):
		""" Start listening for data from the aircraft or log replayer
		and add it to the database. Forks into a new thread and returns
		as soon as possible."""
		self.datagram.start_threaded_recv()
		rxThread = threading.Thread(target=self._receiverThreadRun)
		rxThread.setDaemon(True)
		self.threadRunning = True
		rxThread.start()

	def stop(self):
		""" Stops the listener thread."""
		self.threadRunning = False
		self.datagram.stop_threaded_recv()

	def _receiverThreadRun(self):
		"""(Private) The receive thread. Do not use manually."""
		print "Starting DataReceiver UDP listener thread."
		while self.threadRunning:
			if self.datagram.received_and_begin_processing():
				try:
					cmd = self.datagram.process(0xFFFF)             # Listen for everything
					self.parsegram = self.datagram.getParsegram()   # get parsed data
				except datagram.IpcException:
					print "Invalid datagram received."

				if cmd == datagram.LWC_DATA:
					self.dataReceived()
				elif cmd == datagram.LWC_LOGMSG:
					self.logReceived()
				else:
					print "Ignoring incoming datagram with command =", cmd
					
			self.datagram.end_processing()
			time.sleep(0.01) # 100 Hz

	def registerListener(self, callback):
		self.callbacks.append(callback)

	def unregisterListener(self, callback):
		self.callbacks.remove(callback)

	def dataReceived(self):
		"""Runs from within the Reception thread and processes the received data.
		Ignores anything thats not data to certain destinations."""
		# Run callbacks to notify interested objects
		for func in self.callbacks:
			func(self.parsegram)

	def logReceived(self):
		msg = self.parsegram.get(datagram.LOGGING_D_MESSAGE)
		msgFrom = self.datagram.get_sender()
		if self.senderNames.has_key(msgFrom):
			msgFromName = self.senderNames[msgFrom]
		else:
			msgFromName = "Unknown"
		self.logHandler(msgFromName, msg.strip())
		
	def registerLogHandler(self, callback):
		self.logHandler = callback

	def unregisterLogHandler(self, callback):
		self.logHandler.remove(callback)

if __name__ == "__main__":
	print "Testing mode."
	ipcProtocolFile = "data/ipcProtocol.py"
	dr = DataReceiver(ipcProtocolFile)
	dr.start()


