#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Module to setup UAV route.
# Module has ability to: store the route in the local file system, load it from there, transmit to UAV onboard.
# Known constraint: Internet connection is required for work 
# (c) Bohdan Mushkevych 2010

# PyGTK dependencies
import gtk
import gtk.glade
import time
import os.path
import threading
import logging

from googlemaps.waypointeditor import WaypointEditor
from route import Route
from waypoint import Waypoint
import datagram
import parsegram
from WaypointProperties import WaypointProperties

#-----------------------------------------------------------------------------
# MAP WINDOW CLASS
class MapWindow:
	version = 2.0

	# Constructor
	def __init__(self):
		global waypoint_menu
		# Make the GUI from the Glade file and autoconnect the signals
		wtree = gtk.glade.XML("glade/MapWindow.glade")
		wtree.signal_autoconnect(self)
		
		# Set all the widgets as attributes of self (so e.g. self.statusbar works)
		for w in wtree.get_widget_prefix(''):
			name = w.get_name()
			# make sure we don't clobber existing attributes
			if not hasattr(self, name):
				setattr(self, name, w)
		
		# Route file which was last read/written 
		self.route_filename = ""
		self.route_path = ""
		self.thread_is_running = False
		self.received_route = None
		self.event = threading.Event()
				
		self.init_logger()
		self.init_maps()
		self.init_model()
		self.init_columns()
		
	def init_logger(self):
		# create loger and set it up
		self.logger = logging.getLogger("MapWindow")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)

	def init_maps(self):
		self.route = Route()
		
		self.waypoint_editor = WaypointEditor(os.path.abspath("."), self.vbox_maps, self)
		self.waypoint_editor.drawMap()
	
	# Sets up the model
	def init_model(self):
		# id, lat, lon, alt, lwc + color
		self.model = gtk.ListStore(str, str, str, str, str, str)
		self.treeview_waypoints.set_model(self.model)
		self.treeview_waypoints.set_search_column(0)
		
	# Sets up the columns
	def init_columns(self):
		# First Column (ID)
		col = gtk.TreeViewColumn("id", gtk.CellRendererText(), text=0, foreground=5)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
		col.set_fixed_width(25)
		col.set_resizable(True)
		col.set_sort_column_id(0) # Make it sortable by clicking on the header
		self.treeview_waypoints.append_column(col)
		
		# Second Column (Latitude)
		col = gtk.TreeViewColumn("Latitude", gtk.CellRendererText(), text=1, foreground=5)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) 
		col.set_fixed_width(50)
		col.set_resizable(True)
		self.treeview_waypoints.append_column(col)
		
		# Third Column (Longtitude)
		col = gtk.TreeViewColumn("Longtitude", gtk.CellRendererText(), text=2, foreground=5)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) 
		col.set_fixed_width(50)
		col.set_resizable(True)
		self.treeview_waypoints.append_column(col)

		# Fourth Column (Latitude)
		col = gtk.TreeViewColumn("Altitude", gtk.CellRendererText(), text=3, foreground=5)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) 
		col.set_fixed_width(50)
		col.set_resizable(True)
		self.treeview_waypoints.append_column(col)

		# Fifth Column (LWC)
		col = gtk.TreeViewColumn("LWC", gtk.CellRendererText(), text=4, foreground=5)
		col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED) 
		col.set_fixed_width(50)
		col.set_expand(True)
		self.treeview_waypoints.append_column(col)		
			
	def get_route(self):
		return self.route
	
	def set_route(self, route):
		self.route = route
		self.model.clear()
		
		for wp in self.route:
			self.model.append([wp._id, wp.latitude, wp.longitude, wp.altitude, wp.lwc, "blue"])
	
	def waypoint_add_candidate(self, lat, lon):
		new_wp_id = len(self.route)
		waypoint_properties = WaypointProperties(new_wp_id, lat, lon, callback=self.waypoint_add)
		waypoint_properties.run()
		
	def waypoint_update_candidate(self, _id, lat, lon):
		wp = self.route[_id]
		waypoint_properties = WaypointProperties(wp._id, lat, lon, wp.altitude, wp.lwc, callback=self.waypoint_update)
		waypoint_properties.run()
		
	def waypoint_remove(self, _id):
		for wp in self.route:
			if wp._id == _id:
				self.route.remove(wp)
				break
				
		self.set_route(self.route)
		self.waypoint_editor.drawMap() 
		
	def waypoint_add(self, wp):
		self.route.add(wp)
		self.set_route(self.route)
		self.waypoint_editor.drawMap() 

	def waypoint_update(self, wp):
		waypoint = self.route[wp._id]
		waypoint.latitude = wp.latitude
		waypoint.longitude = wp.longitude
		waypoint.altitude = wp.altitude
		waypoint.lwc = wp.lwc
		self.set_route(self.route)
		self.waypoint_editor.drawMap() 
	
	def run(self):
		self.mapWindow.show_all()

	#-------------------------------------------------------------------------
	# EVENT HANDLERS
	def event_menu_quit(self, obj, data=None): 
		self.mapWindow.destroy()

	def event_menu_about(self, obj, data=None):
		text = "Albatross Groundstation\nVersion " + str(self.version)+ "\n\n(c) Bohdan Mushkevych 2010.\nLicensed under GPLv2."
		dialog = gtk.MessageDialog(self.mapWindow, buttons=gtk.BUTTONS_CLOSE, message_format=text)
		dialog.run()
		dialog.destroy()
		
	def event_button_add(self, obj, data=None):
		self.waypoint_add_candidate(0, 0)
	
	def event_button_remove(self, obj, data=None):
		treeselection = self.treeview_waypoints.get_selection()
		model, iter = treeselection.get_selected()
		model.remove(iter)
	
	def event_button_load(self, obj, data=None):
		dialog = gtk.FileChooserDialog(title="Load Route", action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                  buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		dialog.set_current_folder(self.route_path)

		response = dialog.run()
		if response == gtk.RESPONSE_OK:
			self.route_filename = dialog.get_filename()
			self.route_path = dialog.get_current_folder_uri()
			
			self.load_route_from_file(self.route_filename)
		elif response == gtk.RESPONSE_CANCEL:
			self.logger.debug("Closed, no file selected for loading")
		dialog.destroy()

	
	def event_button_save(self, obj, data=None):
		dialog = gtk.FileChooserDialog(title="Save current Route", action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                  buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
		dialog.set_default_response(gtk.RESPONSE_OK)
		dialog.set_current_folder(self.route_path)
		dialog.set_filename(self.route_filename)
		
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
			self.route_filename = dialog.get_filename()
			self.route_path = dialog.get_current_folder_uri() 
			
			self.save_route_to_file(self.route_filename)
		elif response == gtk.RESPONSE_CANCEL:
			self.logger.debug("Closed, no file selected for saving")
		dialog.destroy()


	def event_button_transmit(self, obj, data=None):
		d = datagram.Datagram()
		d.set_destination(0xFFFF)
		d.set_sender(datagram.GROUNDSTATION_D_BITMASK)
		d.set_command(datagram.LWC_ADD_WAYPOINT)  # adds a waypoint to the route

		for wp in self.route:
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE, wp.longitude)
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE, wp.latitude)
			d.add_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE, wp.altitude)
			d.add_int(datagram.AUTOPILOT_D_WAYPOINT_ID, wp._id)
			d.add_int(datagram.AUTOPILOT_D_WAYPOINT_LWC, wp.lwc)
			d.pack_and_send()
			time.sleep(0.02) # 50 Hz
			
	def event_button_check_route(self, obj, data=None):
		# preventing start of concurrent thread
		self.button_check.set_sensitive(False)
		
		# start own response listener
		self.datagram = datagram.Datagram()
		self.parsegram = parsegram.Parsegram()
		self.datagram.set_start_addr(0) # Make the buffer large enough to hold any possible input
		self.datagram.set_end_addr(0xFFFF)
		self.datagram.start_threaded_recv()
		
		self.rx_thread = threading.Thread(target=self._receiverThreadRun)
		self.rx_thread.setDaemon(False)
		self.event.clear()
		self.received_route = None
		self.thread_is_running = True
		self.rx_thread.start()
		
		time.sleep(0.5) # allow all threads to start up

		# request autopilot to provide its route
		d = datagram.Datagram()
		d.set_destination(0xFFFF)
		d.set_sender(datagram.TEST_D_BITMASK)
		d.set_command(datagram.LWC_REQUEST)         # requests a Route
		d.add_byte(datagram.AUTOPILOT_D_REQUEST_ROUTE, True)
		d.pack_and_send()
		time.sleep(0.02) # 50 Hz
		
		# wait for _receiverThreadRun to receive Route
		self.event.wait(10)
		self.button_check.set_sensitive(True)
		if self.event.is_set() == True:
			self.check_route(self.received_route)

	def save_route_to_file(self, filename):
		''' format was applied from http://www.darpa.mil/grandchallenge05/RDDF_Document.pdf'''
		with open(filename, "w") as file:
			file.write("# latitude & longitude in degrees, altitude in meters \n")
			file.write("# id,lat,long,altitude,lwc \n")
			for wp in self.route:
				line = "" + str(wp._id)
				line += "," + str(wp.latitude)
				line += "," + str(wp.longitude)
				line += "," + str(wp.altitude)
				line += "," + str(wp.lwc)
				line += "\n"
				file.write (line)
				file.flush()

	def load_route_from_file(self, filename):
		''' format was applied from http://www.darpa.mil/grandchallenge05/RDDF_Document.pdf'''
		with open(filename, "r") as file:
			for line in file:
				if line == "":
					self.logger.info("Reading Route: EOL reached")
					return

				if line[0] == "#":
					continue
				
				splits = line.split(",")
				if len(splits) != 5:
					self.logger.info("ERROR: string %s does not suite the required format")
					return
					
				_id = int(splits[0])
				latitude = float(splits[1])
				longitude = float(splits[2])
				altitude = float(splits[3])
				lwc = int(splits [4])
				wp = Waypoint(_id, latitude, longitude, altitude, lwc)
				self.route.add(wp)

			self.set_route(self.route)
			self.waypoint_editor.drawMap()
			
	def _receiverThreadRun(self):
		''' Method listens for Route to be transfered from Autopilot'''
		self.received_route = Route()
		start_time = time.time()
				
		while self.thread_is_running:
			if self.datagram.received_and_begin_processing():
				try:
					cmd = self.datagram.process(0xFFFF)             # Listen for everything
					self.parsegram = self.datagram.getParsegram()   # get parsed data
				except datagram.IpcException:
					self.logger.info("Invalid datagram received.")

				if cmd == datagram.LWC_RESPONSE:
					# autopilot sends response
					length = self.parsegram.get_byte(datagram.AUTOPILOT_D_RESPONSE_LINKS)
					element = self.parsegram.get_byte(datagram.AUTOPILOT_D_RESPONSE_ELEMENT)
					self.received_route.add(self.parse_waypoint())
					self.logger.info("Response has arrived. Element %d of %d; current Route length %d" % (element, length, len(self.received_route)))
		
					if len(self.received_route) == length:
						self.thread_is_running = False

				else:
					#self.logger.info("Ignoring incoming datagram with command = %d" % cmd)
					pass
			
				self.datagram.end_processing()
				time.sleep(0.01) # 100 Hz
			
			if time.time() - start_time > 10:
				self.logger.error("Autopilot has not transmitted Route in 10 sec")
				self.thread_is_running = False

		# shut down response listeners and notify the parent thread
		self.datagram.stop_threaded_recv()
		self.event.set()		
		
	def parse_waypoint(self):
		''' method is called when we expect Waypoint in the transmission
		method goes thru the parsegram and forms Waypoint base on arrived data'''
		longitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LONGITUDE)
		latitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_LATITUDE)
		altitude = self.parsegram.get_float(datagram.AUTOPILOT_D_WAYPOINT_ALTITUDE)
		lwc = self.parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_LWC)
		_id = self.parsegram.get_int(datagram.AUTOPILOT_D_WAYPOINT_ID)
		waypoint = Waypoint(_id=_id, latitude=latitude, longitude=longitude, altitude=altitude, lwc=lwc)
		
		self.logger.debug("arrived waypoint: %s" % str(waypoint))
		return waypoint
	
	def check_route(self, received_route):
		
		# case 1 - MapWindow contains no Route. Transmitted Route is visualized 
		if len(self.route) == 0:
			self.route = received_route
			self.set_route(self.route)
			self.waypoint_editor.drawMap()
		else:
			# case 2 - Compare current route and Transmitted one
			self.model.clear()
			for wp in self.route:
				color = "red"
				if wp in received_route:
					color = "green"
				self.model.append([wp._id, wp.latitude, wp.longitude, wp.altitude, wp.lwc, color])
		

if __name__ == '__main__':
	mw = MapWindow()
	mw.run()

