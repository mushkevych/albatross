#!/usr/bin/env python

""" Plotting support for the Groundstation using Matplotlib.
Edit /usr/shar/matplotlib/.matplotlibrc to change some options.
To install Matplotlib, go to http://matplotlib.sf.net

Hugo Vincent, 19 June 2005
"""

import pygtk
pygtk.require('2.0')
import gtk, gtk.gdk

import matplotlib
matplotlib.use('GTKAgg') # Anti-grain geometry (antialiased) image engine
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends.backend_gtkagg import NavigationToolbar2GTKAgg as NavigationToolbar
from matplotlib.figure import Figure

class PlottingWindow(gtk.Window):
	def __init__(self, title):
		"""Creates a new PlottingWindow instance, sets the window title
		to 'title'. The window will not be shown until data is set with
		PlottingWindow.show(values...)."""
		gtk.Window.__init__(self)
		self.set_default_size(640,480)
		self.connect('destroy', lambda x: self.destroy())

		self.set_title('Plotting: %s' % title)
		vbox = gtk.VBox()
		self.add(vbox)

		fig = Figure(dpi=100)
		self.axis = fig.add_subplot(111)
		self.axis.grid()
		self.plot = None # Create a null plot for now
		
		self.canvas = FigureCanvas(fig)
		vbox.pack_start(self.canvas)
		toolbar = NavigationToolbar(self.canvas, self)
		vbox.pack_start(toolbar, False, False)
		
	def show(self, values):
		"""Sets plot values and shows the plot window."""
		if self.plot == None: # Is this the first plot?
			self.plot, = self.axis.plot(values) # Create the plot
		else:
			self.plot.set_ydata(values) # Update the values
		self.canvas.draw()
		self.show_all()

	def set_xlabel(self, label): self.axis.set_xlabel(label)
	def set_ylabel(self, label): self.axis.set_ylabel(label)
	def set_plot_title(self, title):  self.axis.set_title(title)

# Test Mode
if __name__ == '__main__':
	pw = PlottingWindow('Test')
	pw.set_plot_title('Testing Plot')
	pw.set_xlabel('Time, s')
	pw.set_ylabel('Speed, m/s')
	pw.show([1,3,4,2,4,-1,1,0,-1,5,-5,0,-5]) 
	gtk.main()
