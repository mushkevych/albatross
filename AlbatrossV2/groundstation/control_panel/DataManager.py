#!/usr/bin/python

""" Data storage and data flow management for the Groundstation.
Responsible for recieving IPC datagrams (with threaded reciever in
datagram module), storing them into the database, and notifying the GUI
to update with the new information.

Hugo Vincent, 23 June 2005
"""

from DataReceiver import DataReceiver
import threading, time, parsegram

class DataManager:
	PARSEGRAM_HISTORY_SIZE = 100

	def __init__(self, databaseFile, ipcProtocolFile):
		self.receiver = DataReceiver(ipcProtocolFile)
		self.receiver.registerListener(self.dataReceived)
		self.lock = threading.Lock()
		self.newDataAvailable = False
		self.guiThreadRunning = False
		self.callbacks = []
		
		# parsegram section
		self.parsegramHistory = list()
		self.latestData = parsegram.Parsegram()

	def dataReceived(self, data):
		self.newDataAvailable = True
		self.latestData = data
		self.appendParsegramHistory(data)

	def getParsegramHistory(self):
		""" method returns list of PARSEGRAM_HISTORY_SIZE last parsegrams """
		return self.parsegramHistory

	def appendParsegramHistory(self, parsegram):
		""" method treats parsegram history list as FIFO, with max size of PARSEGRAM_HISTORY_SIZE
		as new parsegram arrives, method appends it to parsegram history list
		and remove (if necessary) oldest records """
		
		self.parsegramHistory.append(parsegram)
		if len(self.parsegramHistory) > self.PARSEGRAM_HISTORY_SIZE :
			self.parsegramHistory.pop(0)

	def _guiUpdateEvent(self):
		while self.guiThreadRunning:
			with self.lock:
				if self.newDataAvailable:
					# Run callbacks to notify interested objects
					for func in self.callbacks:
						func(self.latestData)
					self.newDataAvailable = False
			time.sleep(self.guiUpdateInterval) # Sleep this thread, so the GUI will update at most every guiUpdateInterval seconds.

	def registerGuiUpdateListener(self, callback):
		self.callbacks.append(callback)

	def unregisterGuiUpdateListener(self, callback):
		self.callbacks.remove(callback)

	def setGuiUpdateInterval(self, freq):
		self.guiUpdateInterval = 1.0/freq

	def start(self):
		if len(self.callbacks) > 1:
			self.receiver.start()
			self.guiThread = threading.Thread(target=self._guiUpdateEvent)
			self.guiThread.setDaemon(True)
			self.guiThreadRunning = True
			self.guiThread.start()
		else:
			raise Exception("register at least one GuiUpdateListener() before calling start.")

	def stop(self):
		self.receiver.stop()
		self.guiThreadRunning = False
