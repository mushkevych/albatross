# -*- coding: utf-8 -*-

# Module to edit properties of selected Waypoint from UAV Route
# (c) Bohdan Mushkevych 2010

# PyGTK imports
import pygtk
pygtk.require('2.0')
import gtk, gtk.glade, gobject
import logging

import datagram
from waypoint import Waypoint

class WaypointProperties:
	lwc_list_file = "data/LWClist_Onboard.py"

	def __init__(self, _id = 0, latitude = 0, longitude = 0, altitude = 0, lwc = 0, callback = None):
		self.wp = Waypoint(_id, latitude, longitude, altitude, lwc)
		self.callback = callback 
		
		# Make the GUI from the Glade file and autoconnect the signals
		wtree = gtk.glade.XML("glade/WaypointProperties.glade")
		wtree.signal_autoconnect(self)
		
		# Set all the widgets as attributes of self (so e.g. self.statusbar works)
		for w in wtree.get_widget_prefix(''):
			name = w.get_name()
			# make sure we don't clobber existing attributes
			if not hasattr(self, name):
				setattr(self, name, w)
		
		self.init_logger()
		self.init_model()
		self.init_lwc()
		self.show_properties()
		
	def init_logger(self):
		# create loger and set it up
		self.logger = logging.getLogger("waypoint_properties")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
	
	def init_model(self):
		self.lwc_model = gtk.ListStore(str, str, str)
		self.wp_lwc.set_model(self.lwc_model)
	
		cell = gtk.CellRendererText()
		self.wp_lwc.pack_start(cell, True)
		self.wp_lwc.add_attribute(cell, 'text', 0)  
		
		cell = gtk.CellRendererText()
		self.wp_lwc.pack_start(cell, True)
		self.wp_lwc.add_attribute(cell, 'text', 1)  

		cell = gtk.CellRendererText()
		self.wp_lwc.pack_start(cell, True)
		self.wp_lwc.add_attribute(cell, 'text', 2)  

		self.lwc_model.append(["0", "NONE", "dummy command"])

	def init_lwc(self):
		# Get the data by invoking the Python interpreter
		env = {}
		execfile(self.lwc_list_file, env)
		lwcmds = env['lightWeightCommands_Onboard']

		for cmd in lwcmds:
			if hasattr(datagram, cmd[0]):
				lwc_id = getattr(datagram, cmd[0])
				lwc_name = cmd[0]
				lwc_descr = cmd[1]
				
				self.lwc_model.append([lwc_id, lwc_name, lwc_descr])
			else:
				self.logger.error("Warning: %s is not a valid LWC.\n\tPlease regen data/LWCList_Onboard and the bindings." % cmd)

		self.wp_lwc.set_active(0)

	def get_active_command(self):
		active = self.wp_lwc.get_active()
		if active < 0:
			return None
#		self.logger.debug("chosen LWC command is %s" % self.lwc_model[active][0])
		return self.lwc_model[active][0]
	
	def set_active_command(self, lwc_id):
		i = 0
		for entry in self.lwc_model:
			if entry[0] == lwc_id:
				self.wp_lwc.set_active(i)
#		self.logger.error("Can not identify LWC command %s" % lwc_id)
		
	def show_properties(self):
		self.wp_id.set_text(str(self.wp._id))
		self.wp_latitude.set_text(str(self.wp.latitude))
		self.wp_longitude.set_text(str(self.wp.longitude))
		self.wp_altitude.set_text(str(self.wp.altitude))
		self.set_active_command(self.wp.lwc)
		
	def event_button_ok(self, obj, data=None):
		self.wp._id = int(self.wp_id.get_text())
		self.wp.latitude = float(self.wp_latitude.get_text())
		self.wp.longitude = float(self.wp_longitude.get_text())
		self.wp.altitude = float(self.wp_altitude.get_text())
		
		active_lwc = self.get_active_command()
		if active_lwc == None: active_lwc = "0"
		self.wp.lwc = int(active_lwc)
		
		self.callback(self.wp)
		self.close_window(obj, data)
		
	def run(self):
		self.waypoint_properties.show_all()

	def close_window(self, obj, data=None):
		self.waypoint_properties.destroy()


if __name__ == '__main__':
	wp_prop = WaypointProperties()
	wp_prop.run()
	wp_prop.show_properties()

	gtk.main()
	gtk.main_quit()

