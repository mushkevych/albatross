"""Pyrex wrapper for parsegram.h functions from onboard/lib
Provides the Parsegram class that encapsulates the C API.

Example: SETING & GETTING
  import parsegram
  d = parsegram.Parsegram()
  key = 0x1
  value = 123.456
  d.put_float(key, value)
  d.get_float(key)

Bohdan Mushkevych, 2009."""

#------------------------------------------------------------------------------
# Includes

include "common_cdef.pxd"

#------------------------------------------------------------------------------
# The structures and functions from the Datetime API
cdef extern from "datetime.h":
  # "datetime" type definition
  ctypedef class datetime.datetime [object PyDateTime_DateTime]:
    pass

# The structures and functions from the Parsegram API
cdef extern from "../../onboard/protocol/parsegram.h":
  # Structure definitions
  ctypedef struct parsegram_t:
    # (Note that we only need to define the members we are using here,
    #  not the whole structure)
    pass

  # Functions in parsegram.h
  # **************************** Parsegram Init functions *****************************
  void parsegram_init(parsegram_t* p)
  void parsegram_clear(parsegram_t* p)
  void parsegram_delete(parsegram_t* p)

  # **************************** Parsegram Access functions *****************************
  void parsegram_put(parsegram_t* p, uint8_t key, uint8_t *value)
  void parsegram_remove(parsegram_t* p, uint8_t key)
  uint8_t* parsegram_get(parsegram_t* p, uint8_t key)

  # **************************** Parsegram Conversions functions *****************************
  void parsegram_put_float(parsegram_t* p, uint8_t key, float value)
  float parsegram_get_as_float(parsegram_t* p, uint8_t key)
  void parsegram_put_byte(parsegram_t* p, uint8_t key, uint8_t value)
  uint8_t parsegram_get_as_byte(parsegram_t* p, uint8_t key)
  void parsegram_put_short(parsegram_t* p, uint8_t key, uint16_t value)
  uint16_t parsegram_get_as_short(parsegram_t* p, uint8_t key)
  void parsegram_put_integer(parsegram_t* p, uint8_t key, uint32_t value)
  uint32_t parsegram_get_as_integer(parsegram_t* p, uint8_t key)

  uint8_t parsegram_get_size(parsegram_t *p)
  uint8_t* parsegram_get_keys(parsegram_t *p)


#------------------------------------------------------------------------------
# The class Parsegram (Pythonized API)
cdef class Parsegram:
  cdef parsegram_t *pg # the C parsegram structure
  cdef datetime dati   # the Python datetime object

