"""Pyrex wrapper for datagram.h functions from onboard/lib
Provides the Datagram class that encapsulates the C API.
Uses the default IP, port and buffer size.

Example: SENDING
  import datagram
  d = datagram.Datagram()
  d.set_destination(0xFFFF)
  d.set_command(datagram.LWC_DATA)
  d.add_float(datagram.STATE_D_SPEED_LOCAL_X, 1.234)
  d.pack_and_send()
  d.display()

Example: RECIEVING (THREADED)
  import datagram, parsegram, time
  d = datagram.Datagram()
  d.set_start_addr(datagram.STATE_D_ADDR_START)
  d.set_end_addr(datagram.STATE_D_ADDR_END)
  d.start_threaded_recv()
  while True:
    if d.received_and_begin_processing():
      cmd = d.process(0xFFFF) # Listen for anything
      if cmd == 1:
      pg = r.getParsegram()
      for address in state_addresses:
        f= pg.get_float(address)
        print 'address = %', address, ' float= %', f
    d.end_processing()
    time.sleep(0.01) # 100 Hz

Hugo Vincent, 22 June 2005.
Bohdan Mushkevych, 2009"""

#------------------------------------------------------------------------------
# Include

include "common_cdef.pxd"

cimport parsegram
from parsegram cimport Parsegram

#------------------------------------------------------------------------------
# The structures and functions from the Datagram API
cdef extern from "../../onboard/protocol/parsegram.h":
  # Structure definitions
  ctypedef struct parsegram_t:
    # (Note that we only need to define the members we are using here,
    #  not the whole structure)
    pass
    
cdef extern from "../../onboard/protocol/datagram.h":
  # Structure definitions
  ctypedef struct datagrizzle_t:
    uint8_t recieverStopped
    # (Note that we only need to define the members we are using here,
    #  not the whole structure)

  # Functions in datagram.h
  # **************************** Datagram Header setters and getters *****************************
  uint16_t datagram_get_destination(datagrizzle_t *d)
  void datagram_set_destination(datagrizzle_t *d, uint16_t dest)
  uint16_t datagram_get_sender(datagrizzle_t *d)
  void datagram_set_sender(datagrizzle_t *d, uint16_t send)
  uint16_t datagram_get_command(datagrizzle_t *d)
  void datagram_set_command(datagrizzle_t *d, uint16_t cmd)
  void datagram_set_port(datagrizzle_t *d, int port)
  #uint16_t datagram_get_chsum(datagrizzle_t *d)
  #void datagram_set_chsum(datagrizzle_t *d, uint16_t ch)
  #uint8_t datagram_get_hchsum(datagrizzle_t *d)
  #void datagram_set_hchsum(datagrizzle_t *d, uint8_t hch)
  #uint8_t datagram_get_payload_size(datagrizzle_t *d)
  #void datagram_set_payload_size(datagrizzle_t *d, uint8_t size)
  #void datagram_set_callback(datagrizzle_t *d,void(*fptr)(int, void*), void *user_data)
  #void datagram_set_receive_bitmask(datagrizzle_t *d, uint16_t bitMask)

  # **************************** Datagram Header checksum functions *****************************
  #uint8_t datagram_check_header(datagrizzle_t *d)
  #uint8_t datagram_build_header_checksum(datagrizzle_t *d)
  # **************************** Datagram Payload checksum functions *****************************
  #uint8_t datagram_check_fletcher(datagrizzle_t *d)
  #uint16_t datagram_build_fletcher_checksum(datagrizzle_t *d)

  # **************************** Datagram processing functions *****************************
  int16_t datagram_process(datagrizzle_t *d, uint16_t bitMask)
  void datagram_process_payload(parsegram_t *p, datagrizzle_t *d, uint16_t startAddress, uint16_t endAddress)

  # **************************** Datagram printing functions *****************************
  #void payload_print(payload_t *p)
  void datagram_print(datagrizzle_t *d)

  # **************************** Datagram initialization functions *****************************
  void datagram_init(datagrizzle_t *d, uint16_t maxSize)
  void datagram_clear(datagrizzle_t *d)

  # **************************** Datagram ADD_TYPE functions *****************************
  uint8_t datagram_add_float(datagrizzle_t *d, uint16_t address, float f)
  uint8_t datagram_add_int(datagrizzle_t *d, uint16_t address, uint32_t anInt)
  uint8_t datagram_add_byte(datagrizzle_t *d, uint16_t address, uint8_t b)
  uint8_t datagram_add_short(datagrizzle_t *d, uint16_t address, uint16_t s)
  uint8_t datagram_add_string(datagrizzle_t *d, uint16_t address, char *s)

  # **************************** Datagram Sending functions *****************************
  uint8_t datagram_pack(datagrizzle_t *d)
  void datagram_send(datagrizzle_t *d)

  # **************************** Datagram Recieving functions *****************************
  void *datagram_threaded_reciever(void *tsd)
  uint8_t datagram_recieved_and_begin_processing(datagrizzle_t *d)
  void datagram_end_processing(datagrizzle_t *d)
  #void datagram_set_recieve_frequency(datagrizzle_t *d, uint8_t hz)
  #void datagram_pause_recieve(datagrizzle_t *d)
  void datagram_send_log_message(char *msg, uint16_t sender, int port)

#------------------------------------------------------------------------------
# The class Datagram (Pythonized API)
cdef class Datagram:
  cdef datagrizzle_t *dg # the C datagram structure
  cdef int start_addr
  cdef int end_addr
  cdef int sleepTime
  cdef Parsegram parsegram_object


