# -*- coding: utf-8 -*-
import datagram, time, math

t = 0.0
d = datagram.Datagram()
d.set_destination(0xFFFF)
d.set_sender(datagram.TEST_D_BITMASK)
while True:
	t += 1.0 / 24.0
#	print ' float= %', t

	d.set_command(datagram.LWC_DATA)  # datagram.LWC_DATA = 1

	address = datagram.STATE_D_ROLL_ANGLE		# Roll
	value = 0.3 * math.sin(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_PITCH_ANGLE		# Pitch
	value = 0.15 * math.cos(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_YAW_ANGLE		# Yaw
	value = 0.15 * math.cos(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_ACCELERATION_X		# Acceleration Front
	value = 0.15 * math.cos(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_ACCELERATION_Y		# Acceleration Right
	value = 0.15 * math.cos(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 
	
	address = datagram.STATE_D_ACCELERATION_Z		# Acceleration Bottom
	value = 0.15 * math.cos(t) * 180.0 / math.pi
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_HEADING		# Heading
	value = math.fmod(t * 15.0, 360.0)
	if value < 0.0:
			value += 360.0
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_SPEED_LOCAL_X
	value = t * 4.0
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_SPEED_GROUND
	value = 0.0
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_ALTITUDE
	value = t * 25.0
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_LATITUDE
	value = 37.621134 + 0.005 * t
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_LONGITUDE
	value = -122.374530
	d.add_float(address, value)         # address, value 

	address = datagram.STATE_D_GPS_MODE
	value = 2 - math.sin(t/2 + 0.5)
	d.add_byte(address, value)         # address, value 

	address = datagram.STATE_D_SATS_IN_VIEW
	value = 5.0 + math.sin(t/2 + 0.5)
	d.add_byte(address, value)         # address, value 

	d.pack_and_send()
	d.display()
	time.sleep(0.25)
    
