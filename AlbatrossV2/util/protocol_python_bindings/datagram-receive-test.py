import datagram, parsegram, time, string

state_addresses = {
                   datagram.STATE_D_ROLL_ANGLE : 'STATE_D_ROLL_ANGLE',
                   datagram.STATE_D_PITCH_ANGLE : 'STATE_D_PITCH_ANGLE',
                   datagram.STATE_D_YAW_ANGLE : 'STATE_D_YAW_ANGLE',
                   datagram.STATE_D_GYRO_X : 'STATE_D_GYRO_X',
                   datagram.STATE_D_GYRO_Y : 'STATE_D_GYRO_Y',
                   datagram.STATE_D_GYRO_Z : 'STATE_D_GYRO_Z',
                   datagram.STATE_D_DCM00 : 'STATE_D_DCM00',
                   datagram.STATE_D_DCM01 : 'STATE_D_DCM01',
                   datagram.STATE_D_DCM02 : 'STATE_D_DCM02',
                   datagram.STATE_D_DCM10 : 'STATE_D_DCM10',
                   datagram.STATE_D_DCM11 : 'STATE_D_DCM11',
                   datagram.STATE_D_DCM12 : 'STATE_D_DCM12',
                   datagram.STATE_D_DCM20 : 'STATE_D_DCM20',
                   datagram.STATE_D_DCM21 : 'STATE_D_DCM21',
                   datagram.STATE_D_DCM22 : 'STATE_D_DCM22'
 }

r = datagram.Datagram()
r.set_start_addr(0x10)
r.set_end_addr(0x8f)
r.start_threaded_recv()
while True:
  if r.received_and_begin_processing():
    cmd = r.process(0xFFFF)

    if cmd == 1:
      pg = r.getParsegram()
      print 'datetime = %', pg.get_datetime()

      for address in state_addresses.keys():
        f= pg.get_float(int(address))
        print 'address = %', state_addresses[address], ' float= %', f
        
#        print 'address = {add} float= {value}'.format(add=address, value=f)

  r.end_processing()
  time.sleep(0.01)
