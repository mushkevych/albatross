/* Quick-and-dirty benchmark to time floating point operations
 *
 * Hugo Vincent, 15 June 2005.
 */

#include <stdio.h>
#include <time.h>

#if CLOCKS_PER_SEC != 1000000
#warning clock() does not return microseconds.
#endif

#define ITER 1000000

int main(int argc, char **argv)
{
	int start_time, finish_time;
	int i;
	float f1, f2;
	double d1, d2;
	
	//----------- Single Precision Addition ----------------------------------
	printf("Testing SP add... ");
	
	f1 = 1.0f; f2 = 2.0f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		f1 = f1 + f2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	printf("\tf1 = %.10g, f2 = %.10g\n", f1, f2);
	
	//----------- Single Precision Subtraction -------------------------------
	printf("Testing SP subtract... ");
	
	f1 = 1.0f; f2 = 2.0f;
	start_time = clock();	
	for (i = 0; i < (ITER/2); i++)
	{
		f1 = f2 - f1;
		f2 = f1 - f2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\tf1 = %.10g, f2 = %.10g\n", f1, f2);

	//----------- Single Precision Multiplication ----------------------------
	printf("Testing SP multiplication... ");
	
	f1 = 1.0f; f2 = 1.000001f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		f1 = f1 * f2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\tf1 = %.10g, f2 = %.10g\n", f1, f2);

	//----------- Single Precision Division ----------------------------------
	printf("Testing SP division... ");
	
	f1 = 100.0f; f2 = 1.000001f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		f1 = f1 / f2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\tf1 = %.10g, f2 = %.10g\n", f1, f2);
	
	//----------- Double Precision Addition ----------------------------------
	printf("Testing DP add... ");
	
	d1 = 1.0f; d2 = 2.0f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		d1 = d1 + d2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	printf("\td1 = %.10g, d2 = %.10g\n", d1, d2);
	
	//----------- Double Precision Subtraction -------------------------------
	printf("Testing DP subtract... ");
	
	d1 = 1.0f; d2 = 2.0f;
	start_time = clock();	
	for (i = 0; i < (ITER/2); i++)
	{
		d1 = d2 - d1;
		d2 = d1 - d2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\td1 = %.10g, d2 = %.10g\n", d1, d2);

	//----------- Double Precision Multiplication ----------------------------
	printf("Testing DP multiplication... ");
	
	d1 = 1.0f; d2 = 1.000001f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		d1 = d1 * d2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\td1 = %.10g, d2 = %.10g\n", d1, d2);

	//----------- Double Precision Division ----------------------------------
	printf("Testing DP division... ");
	
	d1 = 100.0f; d2 = 1.000001f;
	start_time = clock();	
	for (i = 0; i < ITER; i++)
	{
		d1 = d1 / d2;
	}

	finish_time = clock();
	printf("%g us\n", (double)(finish_time - start_time) / (double)ITER);
	
	printf("\td1 = %.10g, d2 = %.10g\n", d1, d2);


	return 0;
}
