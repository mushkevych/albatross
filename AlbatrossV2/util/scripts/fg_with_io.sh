#!/bin/bash

# script to start the FlightGear with IN/OUT ports open
# OUT protocol is presented by: fg_from_alba_input.xml
# IN protocol is presented by: fg_to_alba_output.xml
#
# referenced files: 
# $/onboard/autopilot/fg_sender.py
# $/onboard/autopilot/fg_listener.py
# 
# util folder to stoe protocol files and the conversion script
# $/
#
# Bohdan Mushkevych 2010

FREQUENCY=10    #how often packages will arrive; events per second
HOST=127.0.0.1  #FlightGear machine 

INPUT_PORT=16662
INPUT_PROTOCOL=fg_from_alba_input
OUTPUT_PORT=16661
OUTPUT_PROTOCOL=fg_to_alba_output

echo --generic=socket,out,$FREQUENCY,$HOST,$OUTPUT_PORT,udp,$OUTPUT_PROTOCOL
echo --generic=socket,in,$FREQUENCY,$HOST,$INPUT_PORT,udp,$INPUT_PROTOCOL 

fgfs --generic=socket,out,$FREQUENCY,$HOST,$OUTPUT_PORT,udp,$OUTPUT_PROTOCOL --generic=socket,in,$FREQUENCY,$HOST,$INPUT_PORT,udp,$INPUT_PROTOCOL 
