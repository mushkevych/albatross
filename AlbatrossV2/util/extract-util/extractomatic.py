#!/usr/bin/python

# Dumps IMU measurements to a CSV file for spreadsheeting
# Hugo Vincent, 7 Jul 2005.

import datagram, time

r = datagram.Datagram()
r.set_start_addr(0x0)
r.set_end_addr(0xFFFF)
r.start_threaded_recv()
while True:
	if r.received_and_begin_processing():
		cmd = r.process(0xFFFF)
		if cmd == 1:
			print r.buffer_get_float(datagram.STATE_D_ACCELERATION_X), ",",
			print r.buffer_get_float(datagram.STATE_D_ACCELERATION_Y), ",",
			print r.buffer_get_float(datagram.STATE_D_ACCELERATION_Z), ",",
			print r.buffer_get_float(datagram.STATE_D_ROLL_ANGLE), ",",
			print r.buffer_get_float(datagram.STATE_D_PITCH_ANGLE), ",",
			print r.buffer_get_float(datagram.STATE_D_YAW_ANGLE)
	r.end_processing()
	time.sleep(0.01)



