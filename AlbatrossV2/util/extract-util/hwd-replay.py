#!/usr/bin/python

from cPickle import Unpickler
import datagram, time, sys

# Get the data
unp = Unpickler(open("data.pickle", "rb"))
t = unp.load() # Ignores time and assumes a 50 Hz sample rate
data = unp.load()

d = datagram.Datagram()
d.set_destination(0xFFFF)
d.set_command(datagram.LWC_DATA)
d.set_sender(datagram.HW_D_BITMASK)

start_sample = int(float(sys.argv[1]) / 0.02)

inc_time = start_sample
for i in data[start_sample:]:
	d.add_float(datagram.STATE_D_ACCELERATION_X, i[0])
	d.add_float(datagram.STATE_D_ACCELERATION_Y, i[1])
	d.add_float(datagram.STATE_D_ACCELERATION_Z, i[2])
	d.add_float(datagram.STATE_D_ROLL_ANGLE, i[3])
	d.add_float(datagram.STATE_D_PITCH_ANGLE, i[4])
	d.add_float(datagram.STATE_D_YAW_ANGLE, i[5])
	
	print inc_time * 0.02
	inc_time += 1
	d.pack_and_send()
	time.sleep(0.02)


