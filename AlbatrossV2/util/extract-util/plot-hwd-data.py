#!/usr/bin/python

from cPickle import Unpickler
from pylab import figure, show

# Get the data
unp = Unpickler(open("data.pickle", 'rb'))
time = unp.load()
data = unp.load()

# Plot it
chart = figure(1).add_subplot(211)
chart.plot(time, data[:,0], time, data[:,1], time, data[:,2])
chart2 = figure(1).add_subplot(212)
chart2.plot(time, data[:,3], time, data[:,4], time, data[:,5])
show()
