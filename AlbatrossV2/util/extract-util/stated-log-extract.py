# Shitty hack for extracting a CSV file from an stateD output dump

import string

infile = open("stated-log", "r")
outfile = open("stated.csv", "w")

for line in infile:
	if line[:11] == "Velocities:":
		velocities = string.split(line[12:])
	elif line[:13] == "Vel from sim:":
		simvel = string.split(line[14:])
	elif line[:10] == "Positions:":
		positions = string.split(line[11:])
	elif line[:10] == "Attitudes:":
		attitudes = string.split(line[11:])
		outline = ""
		for a in attitudes:
			outline += a + ","
		for v in velocities:
			outline += v + ","
		for vs in simvel:
			outline += vs + ","
		#for p in positions:
		#	outline += p + ","
		outfile.write(outline + "\n")
