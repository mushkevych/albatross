#!/usr/bin/python

import sys, Numeric, csv, cPickle

# Read the file, Get the data
csv_reader = csv.reader(open(sys.argv[1], 'r'))
data = [ [float(entry) for entry in row] for row in csv_reader]

# Process the data for plotting
time = Numeric.arange(0, (len(data) - 1) * 0.02, 0.02)
data = Numeric.array(data)

# Save the data to a Pickle file (binary, efficient)
pick = cPickle.Pickler(open("data.pickle", 'wb'), protocol=2)
pick.dump(time)
pick.dump(data)
