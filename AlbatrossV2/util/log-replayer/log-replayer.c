/*
 Daemon to replay all stored of UDP port and write it to log files

 date: September, 2010
 author: Albatross UAV team
 2010 Bohdan Mushkevych
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
//#include "logd.h"
#include "../../onboard/protocol/albatross_std.h"
#include "../../onboard/protocol/udp_multicast.h"
#include "../../onboard/protocol/datagram.h"
#include "../../onboard/protocol/protocol.h"

//Datagram for sending
datagrizzle_t DgSend;

int main(int argc, char *argv[])
{
	int16_t header_len;
	int16_t payload_len;
	FILE *packets;
	long long nowTime = 0, nextTime = 0, deltaTime = 0;

	// Check the args
	if (argc != 2)
	{
		fprintf(stderr, "Usage: log-replayer packet.log\n");
		exit(-1);
	}

	//Open The Files
	packets = fopen(argv[1], "r");
	if (packets == NULL)
	{
		fprintf(stderr, "Error opening %s", argv[1]);
		exit(-1);
	}

	datagram_init(&DgSend, DATAGRAM_MAX_PAYLOAD_SIZE);

	// Read the log
	while (!feof(packets))
	{
		// clear the datagram
		datagram_clear(&DgSend);

		// Read the packet's header (time, length, header itself)
		fread(&nextTime, sizeof(nextTime), 1, packets);
		fread(&header_len, sizeof(header_len), 1, packets);
		fread(DgSend.payload.header, header_len, 1, packets);
		// Read the packet's payload (length, payload itself)
		fread(&payload_len, sizeof(payload_len), 1, packets);
		fread(DgSend.payload.data, payload_len, 1, packets);

		if (!datagram_check_header(&DgSend))
			printf("Header checksum failed!\n");
		if (!datagram_check_fletcher(&DgSend))
			printf("Payload Fletcher failed!\n");

		// Sleep until the next packet
		if (nowTime != 0)
		{ // special case for first packet in the file - no sleep
			deltaTime = (nextTime - nowTime);
			//printf("Delta time = %lld\n", deltaTime);
			if ((deltaTime < 86400000) && (deltaTime >= 0)) // 86400000 milliseconds in a day
			{
				printf("(Sleeping for %lld milliseconds.)\n", deltaTime);
				usleep(deltaTime * 1000); // deltaTime is in milliseconds
			} else
			{
				fprintf(stderr, "Internal error: deltaTime invalid.");
				exit(1);
			}
		}
		nowTime = nextTime;
		datagram_print(&DgSend);

		// Send the packet
		datagram_pack(&DgSend);
		datagram_send(&DgSend);
		printf("Sent\n\n\n");
	}

	return 0;
}

