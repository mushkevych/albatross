'''
Module presents Adapter that listens to FlightGear stream and generates Datagrams to fuel the Albatross code

Created on 2010-07-04
@author: Bohdan Mushkevych
'''

# script listens to UDP packets from FlightGear and decodes them 
# base rules are:shershen
# FlightGear counts "1 byte for bool, 4 bytes for int, and 8 bytes for double"
# http://wiki.flightgear.org/index.php/Generic_Protocol
#
# and we will use struct package with rules from http://docs.python.org/library/struct.html
# Bohdan Mushkevych 2010

import datagram
import threading
import SocketServer
from fg_data import FGData

HOSTNAME = '127.0.0.1'
FG_OUTPUT_PORT_NO = 16661
FG_INPUT_PORT_NO = 16662


class ThreadedUDPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        _socket = self.request[1]
        _socket.sendto("", self.client_address)
        
        pack = FGData()
        unpacked_data = pack.unpack(data)
        
        # parsing the   
        d = datagram.Datagram()
        d.set_destination(0xFFFF)
        d.set_sender(datagram.TEST_D_BITMASK)
        d.set_command(datagram.LWC_DATA)

        d.add_float(datagram.STATE_D_ROLL_ANGLE, unpacked_data.get("Roll_Deg")) #'Roll Angle'
        d.add_float(datagram.STATE_D_PITCH_ANGLE, unpacked_data.get("Pitch_Deg")) #'Pitch Angle
        d.add_float(datagram.STATE_D_YAW_ANGLE, unpacked_data.get("Yaw_Deg")) #'Yaw/Heading Angle'
        d.add_float(datagram.STATE_D_LATITUDE, unpacked_data.get("Latitude_Deg")) #
        d.add_float(datagram.STATE_D_LONGITUDE, unpacked_data.get("Longitude_Deg")) #
        d.add_float(datagram.STATE_D_ALTITUDE, unpacked_data.get("Altitude_MSL_Meters")) #
        d.add_float(datagram.STATE_D_ACCELERATION_X, unpacked_data.get("Accel_Body_Fwd_MpS")) #'Accel. X (forward)'
        d.add_float(datagram.STATE_D_ACCELERATION_Y, unpacked_data.get("Accel_Body_Right_MpS")) #'Accel. Y (right)'
        d.add_float(datagram.STATE_D_ACCELERATION_Z, unpacked_data.get("Accel_Body_Down_MpS")) #'Accel. Z (down)'
        d.add_float(datagram.STATE_D_SPEED_LOCAL_X, unpacked_data.get("Airspeed_KMpH")) #
        d.add_float(datagram.STATE_D_SPEED_GROUND, unpacked_data.get("Ground_Speed_KMpH")) #
        d.add_float(datagram.STATE_D_SPEED_LOCAL_Z, unpacked_data.get("Vertical_Speed_MpS")) #'Velocity Z (down)'
        d.add_float(datagram.STATE_D_EXTERNAL_TEMPERATURE, unpacked_data.get("External_Temperature_C")) #
        d.add_float(datagram.STATE_D_HEADING, unpacked_data.get("True_Heading_Deg")) #
        d.add_float(datagram.STATE_D_VOLTAGE_BATTERY, unpacked_data.get("Electric_System_Voltage")) #
        d.add_float(datagram.STATE_D_BAROMETRIC_PRESSURE, unpacked_data.get("Barometric_Pressure_Pa")) #
        
        d.pack_and_send()
        
        
    def print_unpacked_data(self, unpacked_data):
        for key, value in unpacked_data.iteritems():
            print key + " : " + str(value)
    

class FGAdapter():
    def __init__(self):
        self.server = SocketServer.ThreadingUDPServer((HOSTNAME, FG_OUTPUT_PORT_NO), ThreadedUDPRequestHandler)
        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(target=self.server.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.setDaemon(False)
        server_thread.start()

    def shutdown(self):
        self.server.shutdown()        


if __name__ == "__main__":
    fg_listener = FGAdapter()