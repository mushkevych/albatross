#!/bin/bash
# (c) Bohdan Mushkevych 2010

echo "Script parses FlightGear generic protocol file and:"
echo ".  generates .h file with output protocol and places it to OpenGC DataSources folder"
echo ".  generates .h file with input protocol"
echo ".  copies current version of fg_to_alba_output.xml and fg_from_alba_input.xml to FlightGear Protocol folder"

# constants
ALBA_DATASOURCE_FOLDER=../../groundstation/OpenGC/Source/DataSources
FG_PROTOCOL_FOLDER=/usr/share/games/FlightGear/Protocol

# generating the .h Protocol file
echo "... generating .h files ..."
python fgXMLtoHeader.py fg_to_alba_output.xml FlightGear_Protocol.h
python fgXMLtoHeader.py fg_from_alba_input.xml FlightGear_Input.h

# copying files
echo "... copying ..."

echo -n "destination folder for .h files: $ALBA_DATASOURCE_FOLDER"
if [ ! -d "$ALBA_DATASOURCE_FOLDER" ]
then
  echo "ERROR: $ALBA_DATASOURCE_FOLDER DOES NOT EXIST - MODIFY DIRECTORY PATH"
else
  cp --force FlightGear_Protocol.h $ALBA_DATASOURCE_FOLDER
fi

echo -n "destination folder for FlightGear Protocol: $FG_PROTOCOL_FOLDER"
if [ ! -d "$FG_PROTOCOL_FOLDER" ]
then
  echo "ERROR: $FG_PROTOCOL_FOLDER DOES NOT EXIST - MODIFY DIRECTORY PATH"
else
  cp --force fg_to_alba_output.xml $FG_PROTOCOL_FOLDER
  cp --force fg_from_alba_input.xml $FG_PROTOCOL_FOLDER
fi

