# -*- coding: utf-8 -*-
'''
Module test connection to FlightGear
Sends sin-shaped variables of ailerons, rudder and elevator
In results, all control flatness must be in move on the plane 

Created on 2010-07-04

@author: shershen
'''


import socket
import time
from math import sin

HOSTNAME = '127.0.0.1'
FG_OUTPUT_PORT_NO = 16661
FG_INPUT_PORT_NO = 16662

def send (ailerons, rudder, elevator):
    # FlightGear stub
    # http://docs.python.org/howto/sockets.html
    # Set up a UDP socket, performing communication to FlightGear
    sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sender.connect((HOSTNAME, FG_INPUT_PORT_NO))
    message = "%f\t%f\t%f\n" % (ailerons, rudder, elevator)
    print "message = " + message
    sender.send(message)
    sender.close()
    
if __name__ == '__main__':
    for i in range(360):
        send(sin(i), sin(i / 2), sin(i * 2))
        time.sleep(0.2)