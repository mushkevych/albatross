#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# Turns a CSV spreadsheet into protocol.h for the interprocess communication
# and data management subsystem on-board Albatross. Also generates protocol.pyx
# for the Python bindings to the Datagram IPC code.
#
# Hugo Vincent, 20 May 2005

import sys, string, time
from string import split

#-----------------------------------------------------------------------------
# Utility Functions
#-----------------------------------------------------------------------------
def error(msg):
	print msg
	sys.exit(1)

def C_ify_name(name): # Makes a name C-ified
	ret = string.upper(name)
	ret = string.replace(ret, ' ', '_')
	ret = string.replace(ret, '\"', '') # because spreadsheet quotes everything...
	return ret

def type_size(type): # Returns size of a type
	sizes = {'float':4, 'uint32_t':4, 'uint16_t':2, 'uint8_t':1, 'char':1}
	fields = split(type, "[")
	if len(fields) == 1:
		return sizes[string.lower(type)]
	else:
		return sizes[string.lower(fields[0])] * int(fields[1][:-1])

#-----------------------------------------------------------------------------
#		STEP 0.
#-----------------------------------------------------------------------------
# Check the command line args and open the output files
if len(sys.argv) < 2 or len(sys.argv) > 4:
	error("Usage: ./mkprotocol-header.py [spreadsheet.csv] [output.h] [output.pyx]")
if len(sys.argv) == 2: # Write to stdout
	hfile = sys.stdout
	hfile_name = "stdout"
	gen_pyrex = False
elif len(sys.argv) == 3:
	hfile_name = sys.argv[2]
	hfile = open(hfile_name,"w")
	gen_pyrex = False
elif len(sys.argv) == 4:
	hfile_name = sys.argv[2]
	hfile = open(hfile_name,"w")
	gen_pyrex = True
	pyxfile_name = sys.argv[3]
	pyxfile = open(pyxfile_name,"w")
ss = open(sys.argv[1],"r") # Open the spreadsheet

#-----------------------------------------------------------------------------
#		STEP 1.
#-----------------------------------------------------------------------------
# Parse the variables section of the file

# Check format of column headers
column_titles = ss.readline().strip()
if column_titles != "Daemon,\"Variable Name\",Type,Description" and column_titles != "\"Daemon\",\"Variable Name\",\"Type\",\"Description\"":
	print column_titles
	error("File format wrong: column headers for section 1 incorrect.")

# Parse, Assign Addresses (i.e. Preprocess)
vars_preproc, line, fields = [], ss.readline().strip(), []
while line:
	if line == ",,,": # Blank line, end of section
		break

	# Tokenize the line (the 3 makes it ignore commas in the description field)
	fields = split(line, ",", 3) 
	if fields[0][0] == "\"" and fields[0][-1] == "\"":
		fields[0] = fields[0][1:-1] # Strip quotes in the daemon field (if they're there)
	if fields[1][0] == "\"" and fields[1][-1] == "\"":
		fields[1] = fields[1][1:-1] # Strip quotes in the variable name (if they're there)
	if fields[2][0] == "\"" and fields[2][-1] == "\"":
		fields[2] = fields[2][1:-1] # Strip quotes in the type field (if they're there)
	if fields[3][0] == "\"" and fields[3][-1] == "\"":
		fields[3] = fields[3][1:-1] # Strip quotes in the description field (if they're there)

	# Check for sytax errors
	if "" in fields: # Blank field is an error
		error("Error: blank field.")

	# Store information and move on to the next line
	vars_preproc.append(fields)
	line = ss.readline().strip()

#-----------------------------------------------------------------------------
#		STEP 2.
#-----------------------------------------------------------------------------
# Parse user-defined values part of the file

# Check format of column headers
column_titles = ss.readline().strip() 
if column_titles != "Name,Value,,Description" and column_titles != "\"Name\",\"Value\",,\"Description\"":
	error("File format wrong: column headers for section 2 incorrect.")

# Parse/Preprocess
userdefs_preproc, line, fields = [], ss.readline().strip(), []
while line:
	if line == ",,,": # Blank line, end of section
		break

	# Tokenize the line
	fields = split(line, ",",3)
	if fields[0][0] == "\"" and fields[0][-1] == "\"":
		fields[0] = fields[0][1:-1] # Strip quotes in the name field (if they're there)
#	if fields[1][0] == "\"" and fields[1][-1] == "\"":
#		fields[1] = fields[1][1:-1] # Strip quotes in the value field (if they're there)
	if fields[3][0] == "\"" and fields[3][-1] == "\"":
		fields[3] = fields[3][1:-1] # Strip quotes in the description field (if they're there)

	# Check for syntax errors
	if "" in [fields[0:1], fields[3]]: # Blank field is an error except in the blank column
		error("Error: blank field.")

	# Store information and move on to the next line
	userdefs_preproc.append(fields)
	line = ss.readline().strip()

#-----------------------------------------------------------------------------
#		STEP 3.
#-----------------------------------------------------------------------------
# Process the variables part of the file (the user defs part doesn't need processing)

name_converter = lambda x: C_ify_name(x + "_D_")

addr, daemons = 0, []
for var_set in vars_preproc:
	# C-ify the daemon name
	var_set[0] = name_converter(var_set[0])
	
	# Build a list of daemons
	if var_set[0] not in daemons: 
		daemons.append(var_set[0])
	
	# Assign an address to each variable
	var_set.append(addr)
	addr += type_size(var_set[2]) # Increment address by size of the current variable
	
# Find start and end addresses 
# (assumes all variables for each daemon are grouped together in the spreadsheet)
last_var = [vars_preproc[0][0], 0]
daemon_addrs = []
for var_set in vars_preproc:
	if var_set[0] != last_var[0]: # Just got to the next daemon
		daemon_addrs.append([last_var[0], last_var[1], var_set[4]])
		last_var = [var_set[0], var_set[4]]
daemon_addrs.append([last_var[0], last_var[1], addr]) # last one is a special case

#-----------------------------------------------------------------------------
#		STEP 4.
#-----------------------------------------------------------------------------
# Write everything to the output file (C header)
hfile.write( # File Header
"""/*------UDP interprocess communication: Protocol Definitions------------------
 * File:            %s
 * Generated from:  %s
 * Date:            %s
 *
 *		THIS FILE IS AUTOGENERATED, DO NOT EDIT THIS FILE.
 */
""" % (hfile_name, sys.argv[1], time.strftime("%d-%m-%Y %H:%M:%S")))

# Write out the daemon bitmasks 
# FIXME John, aren't the masks better off hard-coded so they can't change?
hfile.write("\n//----------- Daemon Bit Masks -----------------------------------------------\n")
bitmask = 0x1
for daemon in daemons:
	hfile.write("#define %s\t0x%x\n" % (daemon + "BITMASK", bitmask))
	bitmask *= 2

# Write out the daemon start and end addresses
hfile.write("\n//----------- Daemon Start/End Addresses  ------------------------------------\n")
for daddr in daemon_addrs:
	hfile.write("#define %s\t0x%x\n" % (daddr[0] + "ADDR_START", daddr[1]))
	hfile.write("#define %s\t0x%x\n" % (daddr[0] + "ADDR_END", daddr[2] - 1))

# Write out the buffer sizes
hfile.write("\n//----------- Local Variable Buffer Sizes -------------------------------------\n")
for daemon in daemons:
	hfile.write("#define %s\t(%s - %s + 1)\n" % (daemon + "BUFSIZE", daemon + "ADDR_END", daemon + "ADDR_START"))

# Write the variables and addresses
hfile.write("\n//-----------Variable Addresses-----------------------------------------------\n")
var_name = ""
for var_set in vars_preproc: 
	hfile.write("// (Type: %s) %s\n" % (var_set[2], var_set[3]))
	var_name = var_set[0] + C_ify_name(var_set[1])
	hfile.write("#define %s\t0x%x\n" % (var_name, var_set[4]))

# Write the user-defined area to the file
hfile.write("\n//----------- User-defined Values --------------------------------------------\n")
for i in userdefs_preproc:
	hfile.write("// Descr: %s\n" % i[3])
	hfile.write("#define %s\t%s" % (C_ify_name(i[0]), i[1]) + "\n")

hfile.write("\n") # for good measure :)

#-----------------------------------------------------------------------------
#		STEP 5.
#-----------------------------------------------------------------------------
# Write everything to the output file (Pyrex include)
if gen_pyrex:
	pyxfile.write( # File Header
"""#------UDP interprocess communication: Protocol Definitions------------------
# File:            %s
# Generated from:  %s
# Date:            %s
#
#		THIS FILE IS AUTOGENERATED, DO NOT EDIT THIS FILE.

""" % (pyxfile_name, sys.argv[1], time.strftime("%d-%m-%Y %H:%M:%S")))

	# Write out the daemon bitmasks 
	# FIXME John, aren't the masks better off hard-coded so they can't change?
	pyxfile.write("\n#------------ Daemon Bit Masks -----------------------------------------------\n")
	bitmask = 0x1
	for daemon in daemons:
		pyxfile.write("%s = 0x%x\n" % (daemon + "BITMASK", bitmask))
		bitmask *= 2

	# Write out the daemon start and end addresses
	pyxfile.write("\n#------------ Daemon Start/End Addresses and Sizes ---------------------------\n")
	for daddr in daemon_addrs:
		pyxfile.write("%s = 0x%x\n" % (daddr[0] + "ADDR_START", daddr[1]))
		pyxfile.write("%s = 0x%x\n" % (daddr[0] + "ADDR_END", daddr[2] - 1))
		pyxfile.write("%s = 0x%x\n" % (daddr[0] + "BUFSIZE", daddr[2] - daddr[1]))

	# Write the variables and addresses
	pyxfile.write("\n#------------Variable Addresses-----------------------------------------------\n")
	var_name = ""
	for var_set in vars_preproc: 
		pyxfile.write("# (Type: %s) %s\n" % (var_set[2], var_set[3]))
		var_name = var_set[0] + C_ify_name(var_set[1])
		pyxfile.write("%s = 0x%x\n" % (var_name, var_set[4]))

	# Write the user-defined area to the file
	pyxfile.write("\n#------------ User-defined Values --------------------------------------------\n")
	for i in userdefs_preproc:
		pyxfile.write("# Descr: %s\n" % i[3])
		pyxfile.write("%s = %s" % (C_ify_name(i[0]), i[1]) + "\n")

	pyxfile.write("\n") # for good measure :)
