#!/usr/bin/python
# -*- coding: utf-8 -*-

import socket, sys, os, struct, threading, re, atexit
from string import split
from binascii import hexlify

allow_threads = True # This is used to kill threads when the users wants to exit

#------------------------------------------------------------------------------
# The UDP Experimentation Shell
# 2005-05-14
# by Hugo Vincent
class UDPshell:
	banner = "UDP-Multicast Experimentation Shell"
	
	def environment(self):
		# Change this list to add commands, alias built in 
		# functions, modify the help strings, or change the 
		# name of a command.
		# Note: the environment is extended by functions defined in the init script.

		def to_hex(data): return hexlify(data) # this is a dirty hack to modify the docstring.
		to_hex.__doc__ = "to_hex(data) - converts data (a string) to hex."

		return {# UDP Shell functions
				'help': self.help,
				'send': send,
				'recv': recv,
				'listen': listen,
				'add_filter': add_filter,
				'exit': self.exit,
				# (Global) Variables to export
				'listeners': [],
				'filters': [],
				# Modules and some common functions
				'sys': sys,
				'struct': struct,
				'to_hex': to_hex,
				'pack': struct.pack,
				'unpack': struct.unpack}
	
	def __init__(self, init_script=None):		
		# Set up the GNU Readline library for saving history.
		import readline 
		histfile =  os.path.join(os.environ["HOME"], ".udpshell_history")
		try:
			readline.read_history_file(histfile)
		except IOError:
			pass # Non-fatal error - ignore it.
		atexit.register(readline.write_history_file, histfile)
		
		# Define the input prompt.
		sys.ps1, sys.ps2 = "UDP> ", "... "
		
		# Run the init script
		self.env = self.environment() # FIXME can we be more generic here??
		if init_script != None:
			execfile(init_script, self.env)

		# (Re-)Export globals FIXME there must be a better, generic way to do this?
		global listeners, filters
		listeners = self.env['listeners']
		filters = self.env['filters']

		# Run the interactive shell.
		from code import interact
		atexit.register(self.__del__)
		interact(self.banner + "\nType help() for help, exit() to quit.", raw_input, self.env)

	def help(self,command=None):
		"help(command) - displays help for command."
		# if a command has been specified, just print that help
		if command != None:
			print command.__doc__
		# otherwise, print a summary of all functions
		else:
			print self.banner
			print "Uses the Python syntax for the interactive shell.\nAvailable Commands:"
			for cmd in self.env: 
				if callable(self.env[cmd]): # only if it's a function
					if self.env[cmd].__doc__:
						# print first line only as a summary
						print '\t' + split(self.env[cmd].__doc__, '\n')[0] 
					else:
						print '\t' + cmd + "(...) - no help available."

	def exit(self, code=0):
		"exit([exit_code]) - exits the shell, optionally returning exit_code."
		sys.exit(code)

	# Try and clean up by destroying threads.
	def __del__(self):
		global allow_threads, listeners
		if len(listeners) > 0:
			
			allow_threads = False 

			for l in listeners:
				l.join(2.0)
				if l.is_alive():
					print "thread % has not been killed. Please kill Python manually.", l.name

#------------------------------------------------------------------------------
# Commands that can be used in the shell:
def send(data, addr='239.192.1.100', port=50000):
	"""send(data[, addr[, port]]) - multicasts a UDP datagram.
	Default port is 50000.
	Default address is 239.192.1.100 (organization-local multicast)"""
	# Create the socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	# Make the socket multicast-aware, and set TTL.
	s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 20) # Change TTL (=20) to suit
	# Send the data
	s.sendto(data, (addr, port))

def recv(addr="239.192.1.100", port=50000, buf_size=1024):
	"""recv([addr[, port[,buf_size]]]) - blocking datagram receive.
	Receive buffer size of buf_size is used (default is 1024).
	Default port is 50000, address is 239.192.1.100
	
	References:
	http://www.python.org/moin/UdpCommunication
	http://sourceforge.net/projects/pyzeroconf"""
	
	# Create the socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# Set some options to make it multicast-friendly
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	try:
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
	except AttributeError:
		pass # Some systems don't support, nor need SO_REUSEPORT
	s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 20)
	s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
	
	#intf = socket.gethostbyname(socket.gethostname())
	#s.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(intf) + socket.inet_aton('0.0.0.0'))
	s.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(addr) + socket.inet_aton('0.0.0.0'))
	
	# Bind to the port
	s.bind(('', port))
	
	# Receive the data, then unregister multicast receivership, then close the port
	data, sender_addr = s.recvfrom(buf_size)
	s.setsockopt(socket.SOL_IP, socket.IP_DROP_MEMBERSHIP, socket.inet_aton(addr) + socket.inet_aton('0.0.0.0'))
	s.close()
	return (sender_addr, data)
	
def add_filter(expr):
	"""add_filter(expr) - creates a datagram display filter from expr.
	Adds the resultant filter to the global filter list; when a datagram is
	received by the ListenEngine, it is passed through the list of filters.
	If a datagram matches a filter, its contents (and metadata) are displayed 
	to the console. Filters do not affect recv().
	
	Standard filters are just regular expressions. For help on writing 
	regular expressions, see the Python Library Reference manual or
	many other sources.
	
	For complex filters, add a function directly to filters (can be done in
	the init script) that displays the data as required."""
	global filters
	# Create the regular expression
	try:
		regexp = re.compile(expr, re.S | re.M)
	except re.error, e:
		print "Error, invalid regular expression:", e
		return

	# Create the filter (if the regexp matches, print the data)
	def filt(data):
		if regexp.match(data) != None:
			print data
	
	# Add the filter to the global filter list
	filters.append(filt)
	print "Registered filter:", expr

def listen(addr="239.192.1.100", port=50000, buf_size=1024, filt_list=None):
	"""listen([addr, port, buf_size, filter_list]) - new non-blocking listener.

	Optional arguments: 
	addr, port - the UDP multicast address and port to use.
	buf_size- size of the receive buffer.
	filt_list- list of filters to apply to received datagrams (default is
	global filters list).
	
	Also see help(filter) for information about creating filters."""
	global listeners, filters
	if filt_list == None: # If no filter list is supplied, use global list
		filt_list = filters 
	l = ListenEngine(addr, port, buf_size, filt_list)
	listeners.append(l)

#------------------------------------------------------------------------------
# Datagram Listening Engine thread:
class ListenEngine(threading.Thread):
	def __init__(self, addr, port, buf_size, filt_list):
		threading.Thread.__init__(self) # Run parent constructor
		self.addr = addr
		self.port = port
		self.buf_size=1024
		self.filters = filt_list
		self.start()

	def run(self):
		global allow_threads
		while allow_threads:
			data = recv(self.addr, self.port, self.buf_size)
			if len(filters) == 0:
				print data
			else:
				for f in self.filters:
					f(data[1])

## FIXME actually filter it
#print "Received:", data, "from:", addr, "port:", port


#import SocketServer
#
## Test SocketServer Handler.
#class MyHandler(SocketServer.BaseRequestHandler):
#	def handle(self):
#		print "Received data:", self.request[0] 
#		#print "\tfrom ", self.request[1].getpeername()
#
#s = SocketServer.ForkingUDPServer(('localhost', 12345), MyHandler)
#s.serve_forever()

#------------------------------------------------------------------------------
# The UDPshell.py application glue functions
def main(argv=None):
	# Enable use in a Python shell with main(argv), or from the command line.
	if argv is None:
		argv = sys.argv
	
	# Use GNU getopt to process command line arguments.
	# (getopt is overkill at the moment, but could be useful if I add more options)
	import getopt
	try:
		opts, args = getopt.getopt(argv[1:], "h", ["help"])
	except getopt.error, msg:
		print >>sys.stderr, msg, "- for help use --help."
		return 1
	
	# Process getopt-style options
	for o, a in opts:
		if o in ("-h", "--help"): # Display usage message
			print >>sys.stderr, "./UDPshell.ph [-h, --help] [init_script]"
			return 0
	
	# Accept up to one command line argument which is not an option.
	if len(args) > 1:
		print >>sys.stderr, "accepts up to one argument, for help use --help"
		return 2
	elif len(args) == 1:
		# Argument is an Initialization Script, check its openable, the run the UDPshell.
		try:
			script = open(args[0])
			script.close()
		except IOError:
			print >>sys.stderr, "can't open file", args[0]
			return 2
		UDPshell(args[0])
	else:
		UDPshell()

# If running standalone (i.e. not imported into another program), run the main function.
if __name__ == "__main__":
	sys.exit(main())

