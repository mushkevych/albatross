# This is the UDPshell.py initialization script.
# It is run by the interpreter before starting the shell, to allow extra functions
# to be defined and listeners and filters added to the global filter and listener
# lists. Imports and global variables are also retained here.

# Key:         SYNC... DEST... CMD.... SIZE CHK DATA...................................... CHK...   
test_dgram = "\x50\x0A\x0A\x04\x00\x00\x0B\x54\x00\x00\x02\x00\x12\x34\xBB\xAA\x01\x00\xDE\x2d\x57"

#------------------------------------------------------------------------------
# ALBATROSS DATAGRAM HANDLING FUNCTIONS
def alba_sniff():
	print "Running the Albatross Datagram sniffer script."
	print "Ctrl+C to kill the listener loop and go to interactive mode."

	data = recv()
	while data:
		print "Data received from", data[0]
		alba_decode(data[1])
		data = recv()

def alba_decode(data=test_dgram):
	"alba_decode(data) - Decode/display an Albatross IPC datagram."

	def correct(exp1, exp2, xform=str): # Exp1 is what it should be, exp2 is what it is
		if exp1 == exp2: print "CORRECT"
		else: print "INVALID (=%s)" % xform(exp2)
	
	# Preliminary checks:
	if len(data) < 10:
		print "Received packet is too short! IGNORING: 0x" + to_hex(data)
		return
	
	# Unpack the header
	header = unpack(">HHBB", data[2:8]) # the '>' makes it big endian (network byte order)
	trailer = unpack(">H", data[-2:])
	
	print "***Albatross IPC Datagram Received.***"

	# Decode the header
	print "SYNC bytes:", hex(ord(data[0])), hex(ord(data[1])),
	correct(True, data[0] == "\x50" and data[1] == "\x0A") # SYNC, SYNC1
	print "Destination Bit-mask:", hex(header[0]) #to_binstr(header[0],16)
	print "Command:", hex(header[1])
	print "Size of Payload:", header[2],
	correct(header[2], len(data) - 10) # there are 10 bytes of header/trailer
	print "Header Checksum:", hex(header[3]),
	correct(header[3], xor_checksum(data[0:9]), hex)
	
	# Decode the data
	alba_payload_dec(data[8:-2])

	# Checksum the datagram:
	print "Datagram Checksum:", hex(trailer[0]),
	correct(trailer[0], fletcher16(data[0:-2]), hex)
	print

def alba_payload_dec(data):
	"""alba_payload(data) - displays the payload of an Albatross IPC datagram.
	Uses the Address-Size-Data tuples format: 
	[Addr, 2 bytes][Size, 1 byte][Data...]"""
	print "Payload contents:"
	try:
		index = 0
		while index+3 < len(data):
			addr, size = unpack(">HB", data[index:index+3]) # start address and size
			index += 3
			for i in range(0, size):
				print "\tAddr: %s = %s" % (hex(addr), to_hex(data[index + i]))
				addr += 1
			index  += size
	except IndexError, e: # I am lazy
		print "\tWarning: datagram payload is corrupt (truncated?)"
				
def alba_create(dest, cmd, payload):
	"alba_create(dest, cmd, payload) - creates an Albatross IPC datagram."
	size = len(payload)
	data = "\x50\x0A" # SYNC bytes
	#FIXME: The sender for the python program is coded as 1000 0000 0000 0000 (0x8000)
	data += pack(">HHHB", dest, 0x8000, cmd, size) # Fill in DEST, CMD & Size Fields
	data += pack(">B", xor_checksum(data[0:9])) # Header Checksum
	data += payload
	data += pack(">H", fletcher16(data)) # Overall Checksum
	return data

def alba_payload_pack(tuples):
	"""alba_payload_pack(tuples) - packs (addr, size, value) tuples -> payload.
	max(addr) is 0xFFFF (16-bit), size is "B" (byte), "H" (short), or "I" (int)."""
	data = ""
	for i in tuples:
		packed_data = pack(">" + i[1], i[2])
		data += pack(">HB", i[0], len(packed_data))
		data += packed_data
	return data
	
	# Note: I don't try and do anything tricky here. Sequential addressses are only
	# recognized if their tuples are sequential. (And it's *still* messy... ARGGG!)
	#last_tuple = tuples[0]
	#packed_data = pack(">" + last_tuple[1], last_tuple[2])
	#last_length = len(packed_data)
	#data = pack(">HB", last_tuple[0], last_length)
	#data += packed_data
	#for i in tuples[1:]:
	#	packed_data = pack(">" + i[1], i[2])
	#	if last_length + last_tuple[0] == i[0]:
	#		data += packed_data
	#		# Need to fix the already-written size argument from here...
	#		continue
	#	data += pack(">HB", i[0], len(packed_data))
	#	data += packed_data
	#	last_length = len(packed_data)
	#	last_tuple = i
	#return data

#------------------------------------------------------------------------------
# UTIL FUNCTIONS
def to_binstr(num,pad=None):
	"""to_binstr(num[, pad]) - converts an (unsigned) int into a bit string.
	Optional argument 'pad' pads result to at least 'pad' bits."""
	bitstr = ""
	while num > 0:
		j = num % 2
		bitstr = str(j) + bitstr
		num /= 2
	if pad and len(bitstr) < pad:
		bitstr = "0" * (pad - len(bitstr)) + bitstr
	return bitstr

#------------------------------------------------------------------------------
# CHECKSUMS
# FIXME investigate how to make these use 32-bit math for faster throughput
def xor_checksum(data):
	"xor_checksum(data): computes the 8-bit XOR checksum of data."
	check = 0
	for byte in data:
		check ^= ord(byte)
		check = check % 255
	return check

def fletcher16(data):
	"""fletcher16(data) - computes the Fletcher-16 checksum for data.
	See http://johnstowers.no-ip.com/wiki/index.php/Protocol for info."""
	s1, s2 = 1, 0
	for ch in data:
		s1 += ord(ch)
		s1 = s1 % 255 # these explicit modulos are only needed because I can't do 8-bit overflowing math in Python
		s2 += s1
		s2 = s2 % 255
	return (s2 * 256) + s1 # Reverse the byte order

