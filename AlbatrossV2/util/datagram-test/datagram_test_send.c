#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "datagram_test.h"
#include "../../onboard/protocol/albatross_std.h"
#include "../../onboard/protocol/udp_multicast.h"
#include "../../onboard/protocol/datagram.h"
#include "../../onboard/protocol/protocol.h"
#define DATAGRAM_PRINT 1
#define BUFFER_PRINT 0
int main(int argc, char *argv[])
{
	datagrizzle_t snd;
	uint16_t dest;

	if(argc == 2)
	{
		//Check the command line stuff
		if (argv[1][0] == '?')
		{
			fprintf(stderr, "Usage: datagram_send DEST_BITMASK\n");
			fprintf(stderr, "e.g. ./datagram_send 4\n");
			fprintf(stderr, "If ommitted then the destination is TEST_DESTINATION\n");
			return 1;
		}
		else
			sscanf(argv[1],"%d",&dest);
	}
	else
		dest = TEST_DESTINATION;

	printf("Destination: 0x%X\n",dest);

	datagram_clear(&snd);
	datagram_init(&snd,DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_sender(&snd,TEST_SENDER);
	datagram_set_destination(&snd,dest);
	datagram_set_command(&snd,TEST_COMMAND);
	printf("Added Destination and Command\n");
	datagram_add_byte(&snd,TEST_BYTE_ADDY,TEST_BYTE);
	printf("Added Byte\n");
	datagram_add_short(&snd,TEST_SHORT_ADDY,TEST_SHORT);
	printf("Added Short\n");
	datagram_add_int(&snd,TEST_INT_ADDY,TEST_INT);
	printf("Added Int\n");
	datagram_add_float(&snd,TEST_FLOAT_ADDY,TEST_FLOAT);
	printf("Added Float\n");
	datagram_add_string(&snd,TEST_STRING_ADDY,TEST_STRING);
	printf("Added String\n");
	datagram_pack(&snd);
	printf("Packed\n");
	datagram_send(&snd);

#if DATAGRAM_PRINT
	datagram_print(&snd);
#endif
#if BUFFER_PRINT
	payload_print(&snd.payload);
#endif
	printf("--DONE--\n");
	return 0;

}
