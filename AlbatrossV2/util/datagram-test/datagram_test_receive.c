#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "datagram_test.h"
#include "../../onboard/protocol/albatross_std.h"
#include "../../onboard/protocol/udp_multicast.h"
#include "../../onboard/protocol/datagram.h"
#include "../../onboard/protocol/parsegram.h"
#include "../../onboard/protocol/protocol.h"

#define PROCESS_PAYLOAD 1
int main(int argc, char *argv[])
{
	int cmd;
	char *from;
	//Allocate the required memory
	from = (char *)malloc(100 * sizeof(char)); //From IP String

	datagrizzle_t dgz;
	parsegram_t parsegram;

	//Threading
	pthread_t rcv;
  uint16_t dest;

	if(argc == 2)
	{
		//Check the command line stuff
		if (argv[1][0] == '?')
		{
			fprintf(stderr, "Usage: datagram_receive DEST_BITMASK\n");
			fprintf(stderr, "e.g. ./datagram_send 4\n");
			fprintf(stderr, "If ommitted then ALL DATAGRAMS ARE RECEIVED\n");
			return 1;
		}
		else
			sscanf(argv[1],"%d",&dest);
	}
	else
		dest = 0xFFFF;

	printf("Receiving From: 0x%X\n",dest);
	
	//vars = (uint8_t *)calloc(255,sizeof(uint8_t)); //The payload data in the datagram_t struct
	
	//Initiation
	parsegram_init(&parsegram);
	datagram_clear(&dgz);
	datagram_init(&dgz,DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_recieve_frequency(&dgz, 2);
	
	//Now attach the thread and start it
	pthread_create(&rcv, NULL,&datagram_threaded_reciever,(void *)&dgz);
	printf("Reciever Thread Started\n");

	while(TRUE)
	{
		if(datagram_recieved_and_begin_processing(&dgz) == TRUE)
		{
			//printf("Received bytes from %s\n",dgz.from);
			cmd = datagram_process(&dgz, dest);
			printf("Command: 0x%X %i\n", datagram_get_command(&dgz), datagram_get_command(&dgz));

			if(cmd != -1)
			{
				printf("Valid Datagram\n");
				datagram_print(&dgz);
#if PROCESS_PAYLOAD

				datagram_process_payload(&parsegram, &dgz, 0, 0xFFFF);
				printf("Sent Byte = 0x%X\n", parsegram_get_as_byte(&parsegram, TEST_BYTE_ADDY));
				printf("Sent Short = 0x%X\n", parsegram_get_as_short(&parsegram, TEST_SHORT_ADDY));
				printf("Sent Int = 0x%X\n", parsegram_get_as_integer(&parsegram, TEST_INT_ADDY));
				printf("Sent Float = %f\n", parsegram_get_as_float(&parsegram, TEST_FLOAT_ADDY));
				printf("Sent String = %s\n",parsegram_get(&parsegram, TEST_STRING_ADDY));

#endif
			}
			else
			{
				printf("Invalid Datagram\n");
				datagram_print(&dgz);
				//printf("Built Chksums: CRC=[0x%X], F=[0x%X]\n",datagram_build_header_checksum(&dgz),datagram_build_fletcher_checksum(&dgz));
			}
  		datagram_end_processing(&dgz);
		}
		datagram_pause_recieve(&dgz);
	}

	printf("--DONE--\n");
	return 0;

}
